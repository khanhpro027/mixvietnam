import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mixvietnam/constants/colors.dart';

class AppLoadingIndicator extends StatelessWidget {
  const AppLoadingIndicator({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Platform.isIOS
          ? const CupertinoActivityIndicator()
          : const CircularProgressIndicator(
              strokeWidth: 2,
              backgroundColor: PRIMARY_COLOR,
              valueColor: AlwaysStoppedAnimation(Colors.redAccent),
            ),
    );
  }
}
