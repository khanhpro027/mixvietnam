import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:mixvietnam/base/app_config.dart';
import 'package:mixvietnam/constants/colors.dart';
import 'package:mixvietnam/constants/strings.dart';
import 'package:mixvietnam/pages/option_screen/widgets/option_auth_widget.dart';
import 'package:mixvietnam/widgets/back_widget.dart';

class ComeBackGroup extends StatelessWidget {
  ComeBackGroup(
      {Key key, this.isShowLogin = false, this.text1, this.text2, this.text3})
      : super(key: key);
  final bool isShowLogin;
  final String text1;
  final String text2;
  final String text3;
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        BackWidget(),

        // Text create account
        RichText(
          text: TextSpan(
            text: text1 ?? suggestRegister1,
            style: AppConfig.style.contentText1.copyWith(
              fontSize: 12,
              fontWeight: FontWeight.w400,
              color: CL_BOLD,
            ),
            children: [
              TextSpan(
                text: text2 ?? suggestText,
                style: AppConfig.style.contentText1.copyWith(
                  fontSize: 12,
                  fontWeight: FontWeight.w500,
                  color: PRIMARY_BUTTON_COLOR,
                ),
                recognizer: TapGestureRecognizer()
                  ..onTap = () => _onTapSuggest(context),
              ),
              TextSpan(
                text: text3 ?? suggestRegister3,
                style: AppConfig.style.contentText1.copyWith(
                  fontSize: 12,
                  fontWeight: FontWeight.w400,
                  color: CL_BOLD,
                ),
              ),
            ],
          ),
        ),
        const SizedBox(height: 12),
      ],
    );
  }

  void _onTapSuggest(BuildContext context) {
    isShowLogin
        ? OptionAuthWidget().onMoveToLoginPage(context)
        : OptionAuthWidget().onMoveToRegisterPage(context);
  }

  String get suggestText =>
      isShowLogin ? login.toLowerCase() : suggestRegister2.toLowerCase();
}
