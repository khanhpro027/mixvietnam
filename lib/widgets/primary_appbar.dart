import 'package:flutter/material.dart';
import 'package:mixvietnam/base/app_config.dart';
import 'package:mixvietnam/constants/image.dart';
import 'package:mixvietnam/widgets/bottom_appbar.dart';
import 'package:mixvietnam/widgets/type_bottom.dart';

class PrimaryAppBar extends StatelessWidget {
  const PrimaryAppBar(
      {this.title, this.isBottom = true, this.isLoading = false});

  final String title;
  final bool isBottom;
  final bool isLoading;

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return AppBar(
      elevation: 0,
      centerTitle: true,
      leading: Image.asset(primaryLogo),
      title: Text(
        title,
        style: AppConfig.style.contentText1.copyWith(
            color: Colors.grey[700], fontWeight: FontWeight.w500, fontSize: 14),
      ),
      actions: [
        Image.asset(edit_home),
        Image.asset(search_home),
      ],
      bottom: (isBottom == true)
          ? PreferredSize(
              child: Column(
                children: [
                  Container(
                    padding: const EdgeInsets.only(top: 12),
                    color: Colors.white,
                    child: BottomAppbar(
                      isLoading: isLoading,
                    ),
                  ),
                  Container(
                    color: Colors.white,
                    child: TypeBottom(
                      isLoading: isLoading,
                    ),
                  ),
                ],
              ),
              preferredSize: Size(size.width, 150))
          : PreferredSize(
              preferredSize: const Size(0, 0),
              child: Container(),
            ),
    );
  }
}
