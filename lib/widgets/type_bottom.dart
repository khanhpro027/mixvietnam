import 'package:flutter/material.dart';
import 'package:mixvietnam/constants/colors.dart';
import 'package:mixvietnam/constants/image.dart';
import 'package:mixvietnam/constants/strings.dart';
import 'package:mixvietnam/widgets/item_bottom.dart';

class TypeBottom extends StatelessWidget {
  final isLoading;
  TypeBottom({this.isLoading = false});
  @override
  Widget build(BuildContext context) {
    return (isLoading == true)
        ? _loadingWidget()
        : Column(
            children: [
              Divider(
                height: 1,
                color: textColor3.withOpacity(0.7),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 9, bottom: 9),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      child: ItemBottom(icon: music_icon, title: music_text),
                    ),
                    Image.asset(vertical_bar),
                    Expanded(
                      child: ItemBottom(icon: picture, title: image_text),
                    ),
                  ],
                ),
              ),
              Divider(
                height: 1,
                color: textColor3.withOpacity(0.7),
              ),
            ],
          );
  }

  Widget _loadingWidget() {
    return Column(
      children: [
        Divider(
          height: 1,
          color: textColor3.withOpacity(0.7),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 9, bottom: 9),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                width: 57,
                height: 19,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(4),
                    color: Colors.grey[100]),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 50, right: 50),
                child: Image.asset(vertical_bar),
              ),
              Container(
                width: 57,
                height: 19,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(4),
                    color: Colors.grey[100]),
              ),
            ],
          ),
        ),
        Divider(
          height: 1,
          color: textColor3.withOpacity(0.7),
        ),
      ],
    );
  }
}
