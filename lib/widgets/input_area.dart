import 'package:flutter/material.dart';
import 'package:mixvietnam/base/app_config.dart';
import 'package:mixvietnam/base/app_helpers.dart';
import 'package:mixvietnam/constants/colors.dart';
import 'package:mixvietnam/constants/strings.dart';
import 'package:mixvietnam/models/area_model.dart';
import 'package:mixvietnam/widgets/text_field_widget.dart';
import 'package:mixvietnam/widgets/simple_button_widget.dart';

class InputArea extends StatefulWidget {
  const InputArea({
    this.values = false,
    this.title1 = '',
    this.title2 = '',
    this.child,
    this.isShowPrivay = true,
    this.titleButton,
    this.onClickNextButton,
  });

  final bool values;
  final String title1;
  final String title2;
  final bool isShowPrivay;
  final String titleButton;
  final Widget child;
  final ValueChanged<AreaModel> onClickNextButton;

  @override
  State<InputArea> createState() => _InputAreaState();
}

class _InputAreaState extends State<InputArea> {
  FocusScopeNode focusNode;
  final TextEditingController controller1 = TextEditingController();
  final TextEditingController controller2 = TextEditingController();
  bool _values = false;
  @override
  void initState() {
    _onChangeController1();
    _onChangeController2();
    super.initState();
  }

  @override
  void dispose() {
    controller1.dispose();
    controller2.dispose();
    super.dispose();
  }

  void _onChangeController1() {
    controller1.addListener(() {
      setState(() {
        if (widget.title2.isEmpty) {
          if (controller1.text.isNotEmpty) {
            _values = true;
          } else {
            _values = false;
          }
        }
      });
    });
  }

  void _onChangeController2() {
    controller2.addListener(() {
      setState(() {
        if (widget.title2.isNotEmpty) {
          if (controller1.text.isNotEmpty && controller2.text.isNotEmpty) {
            _values = true;
          } else {
            _values = false;
          }
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    final Size screenSize = MediaQuery.of(context).size;
    focusNode = FocusScope.of(context);

    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15.0),
      ),
      child: Padding(
        padding: const EdgeInsets.only(left: 12, right: 12),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            if (widget.child != null) ...[
              widget.child
            ] else ...[
              // TextField 1
              Padding(
                padding: widget.title2.isNotEmpty
                    ? const EdgeInsets.only(top: 10, bottom: 16)
                    : const EdgeInsets.only(top: 10),
                child: TextFieldWidget(
                  title: widget.title1,
                  controller: controller1,
                ),
              ),

              // TextField 2
              if (widget.title2.isNotEmpty)
                Padding(
                  padding: const EdgeInsets.only(bottom: 12),
                  child: TextFieldWidget(
                    title: widget.title2,
                    controller: controller2,
                  ),
                ),
            ],

            // Button next
            SimpleButtonWidget(
              text: widget.titleButton,
              margin: const EdgeInsets.only(top: 5, bottom: 10),
              width: screenSize.width,
              background: (widget.values || _values)
                  ? PRIMARY_BUTTON_COLOR
                  : colorDisable,
              radiusBorder: 10,
              onPressed: (widget.values || _values) ? _onClickNextButton : null,
            ),

            // App privacy
            if (widget.isShowPrivay) ...[
              RichText(
                text: TextSpan(
                    text: note,
                    style: AppConfig.style.contentText1
                        .copyWith(fontSize: 12, fontWeight: FontWeight.w300),
                    children: [
                      TextSpan(
                          text: privacy,
                          style: AppConfig.style.contentText1.copyWith(
                              fontSize: 12,
                              color: CL_BOLD,
                              fontWeight: FontWeight.w500)),
                    ]),
              ),
              Text(
                term,
                style: AppConfig.style.contentText1.copyWith(
                    fontSize: 12, color: CL_BOLD, fontWeight: FontWeight.w500),
              ),
            ],
            const SizedBox(height: 20),
          ],
        ),
      ),
    );
  }

  void _onClickNextButton() {
    // Hide key board
    AppHelpers.hideKeyboard(focusNode);

    // Call onClickNextButton
    widget?.onClickNextButton?.call(
      AreaModel(
        textField1: controller1?.text ?? '',
        textField2: controller2?.text ?? '',
      ),
    );
  }
}
