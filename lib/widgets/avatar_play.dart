import 'package:flutter/material.dart';
import 'package:mixvietnam/constants/image.dart';

class AvatarPlay extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.center,
      children: [
        Container(
          margin: const EdgeInsets.only(right: 10, left: 16),
          width: 110,
          height: 110,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(4),
            image: const DecorationImage(
                image: AssetImage(fake_avatar3), fit: BoxFit.cover),
          ),
        ),
        Container(
          width: 36,
          height: 36,
          decoration: BoxDecoration(
              color: Colors.white54,
              image: const DecorationImage(
                image: AssetImage(playIcon),
              ),
              borderRadius: BorderRadius.circular(20)),
          alignment: Alignment.center,
        )
      ],
    );
  }
}
