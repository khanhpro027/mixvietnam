import 'package:flutter/material.dart';

class ImageAssetWidget extends StatelessWidget {
  const ImageAssetWidget({
    Key key,
    @required this.assetPath,
    this.padding = EdgeInsets.zero,
  }) : super(key: key);

  final String assetPath;
  final EdgeInsetsGeometry padding;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: padding,
      child: Image.asset(assetPath, fit: BoxFit.fill),
    );
  }
}
