import 'package:flutter/material.dart';
import 'package:mixvietnam/base/app_config.dart';
import 'package:mixvietnam/constants/colors.dart';

class ItemBottom extends StatelessWidget {
  ItemBottom({this.icon, this.title});

  final String icon;
  final String title;
  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Image.asset(icon),
        Text(
          title,
          style: AppConfig.style.contentText1.copyWith(
              fontSize: 14, fontWeight: FontWeight.w500, color: textColor3),
        ),
      ],
    );
  }
}
