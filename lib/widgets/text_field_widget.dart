import 'package:flutter/material.dart';
import 'package:mixvietnam/base/app_config.dart';
import 'package:mixvietnam/constants/colors.dart';

class TextFieldWidget extends StatelessWidget {
  const TextFieldWidget({
    this.title = '',
    this.controller,
    this.isPassword = false,
    this.isEnable = false,
    this.errorText = '',
  });

  final String title;
  final String errorText;
  final TextEditingController controller;
  final bool isPassword;
  final bool isEnable;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          height: 56,
          padding: const EdgeInsets.all(8),
          decoration: BoxDecoration(
            border: Border.all(
              width: 1,
              color: getColorBorder,
            ),
            borderRadius: BorderRadius.circular(10),
          ),
          child: TextFormField(
            controller: controller,
            obscureText: isPassword,
            decoration: InputDecoration(
              border: const OutlineInputBorder(borderSide: BorderSide.none),
              enabledBorder:
                  const OutlineInputBorder(borderSide: BorderSide.none),
              labelText: title,
              labelStyle: AppConfig.style.bodyText1.copyWith(
                color: textColor3,
                fontSize: 14,
                fontWeight: FontWeight.w400,
              ),
              floatingLabelStyle: AppConfig.style.bodyText1.copyWith(
                color: textColor3,
                fontSize: 16,
                fontWeight: FontWeight.w400,
              ),
              contentPadding: const EdgeInsets.symmetric(vertical: 5),
            ),
          ),
        ),

        // Error text
        if (errorText?.isNotEmpty ?? false)
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 8.0),
            child: Row(
              children: <Widget>[
                Icon(Icons.error, size: 20.0, color: errorTextFieldColor),
                Padding(
                  padding: const EdgeInsets.only(left: 5.0),
                  child: Text(
                    errorText,
                    style:
                        TextStyle(fontSize: 16.0, color: errorTextFieldColor),
                  ),
                )
              ],
            ),
          ),
      ],
    );
  }

  Color get getColorBorder {
    if (errorText?.isNotEmpty ?? false) {
      return errorTextFieldColor;
    }

    // When not error
    if (isEnable == true) {
      return CL_BOLD;
    } else {
      return CL_DISABLE;
    }
  }
}
