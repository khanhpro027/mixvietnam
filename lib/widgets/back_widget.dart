import 'package:flutter/cupertino.dart';
import 'package:flutter_svg/svg.dart';
import 'package:mixvietnam/base/app_config.dart';
import 'package:mixvietnam/constants/colors.dart';
import 'package:mixvietnam/constants/image.dart';
import 'package:mixvietnam/constants/strings.dart';

class BackWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return CupertinoButton(
      onPressed: () => Navigator.pop(context),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SvgPicture.asset(back),
          Text(
            backText,
            style: AppConfig.style.contentText1.copyWith(
                fontSize: 14, fontWeight: FontWeight.w500, color: CL_BOLD),
          ),
        ],
      ),
    );
  }
}
