import 'package:flutter/material.dart';
import 'package:mixvietnam/constants/image.dart';
import 'package:mixvietnam/constants/strings.dart';

class BottomAppbar extends StatelessWidget {
  final bool isLoading;
  BottomAppbar({this.isLoading = false});
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return (isLoading == true)
        ? _loadingWidget()
        : Padding(
            padding: const EdgeInsets.only(left: 16, bottom: 12),
            child: Row(
              children: [
                Image.asset(fake_avatar),
                Container(
                  margin: const EdgeInsets.only(left: 10),
                  width: size.width - 90,
                  //height: 30,
                  child: const TextField(
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      hintText: yourThiking,
                    ),
                  ),
                ),
              ],
            ),
          );
  }

  Widget _loadingWidget() {
    return Padding(
      padding: const EdgeInsets.only(left: 16, bottom: 12),
      child: Row(
        children: [
          Image.asset(fake_avatar),
          Container(
            margin: const EdgeInsets.only(left: 10),
            width: 146,
            height: 17,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(4),
                color: Colors.grey[100]),
            //height: 30,
          ),
        ],
      ),
    );
  }
}
