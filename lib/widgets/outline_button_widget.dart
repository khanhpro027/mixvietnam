import 'package:flutter/material.dart';

class OutlineButtonWidget extends StatelessWidget {
  const OutlineButtonWidget({
    Key key,
    @required this.text,
    this.width,
    this.height,
    this.textColor = Colors.white,
    this.radius = 10,
    this.onPressed,
  }) : super(key: key);

  final String text;
  final double width;
  final double height;
  final Color textColor;

  final double radius;
  final VoidCallback onPressed;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: height,
      child: TextButton(
        onPressed: () => onPressed != null ? onPressed.call() : null,
        child: Text(text, style: TextStyle(color: textColor)),
        style: ElevatedButton.styleFrom(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(radius)),
          ),
        ),
      ),
    );
  }
}
