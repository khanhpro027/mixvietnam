import 'package:flutter/material.dart';
import 'package:mixvietnam/constants/colors.dart';

class SimpleButtonWidget extends StatelessWidget {
  const SimpleButtonWidget({
    Key key,
    @required this.text,
    this.width,
    this.height,
    this.textColor = Colors.white,
    this.background = Colors.white,
    this.margin = EdgeInsets.zero,
    this.radius = 10,
    this.radiusBorder = 0,
    this.onPressed,
    this.border,
    this.isDisable = false,
  }) : super(key: key);

  final String text;
  final double width;
  final double height;
  final double radiusBorder;
  final Color textColor;
  final Color background;
  final EdgeInsets margin;
  final double radius;
  final VoidCallback onPressed;
  final Border border;
  final bool isDisable;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: height,
      decoration: BoxDecoration(
        color: isDisable ? colorDisable : background,
        borderRadius: BorderRadius.circular(radiusBorder),
        border: border,
      ),
      margin: margin,
      child: TextButton(
        onPressed: () => onPressed != null
            ? isDisable
                ? null
                : onPressed.call()
            : null,
        child: FittedBox(child: Text(text, style: TextStyle(color: textColor))),
        style: ElevatedButton.styleFrom(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(radius)),
          ),
        ),
      ),
    );
  }
}
