import 'package:flutter/material.dart';

class ImageNetworkWidget extends StatelessWidget {
  const ImageNetworkWidget({
    Key key,
    @required this.url,
    this.padding = EdgeInsets.zero,
  }) : super(key: key);

  final String url;
  final EdgeInsetsGeometry padding;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: padding,
      child: Image.network(url, fit: BoxFit.fill),
    );
  }
}
