class Personal {
  const Personal({
    this.id,
    this.userName,
    this.email,
    this.privacy,
    this.name,
    this.intro,
    this.dob,
    this.gender,
    this.phone,
    this.regType,
    this.regIdentity,
    this.avatar,
    this.cover,
    this.follower,
    this.following,
    this.postCount,
    this.bio,
    this.listened,
    this.token,
    this.needSetMusic,
  });

  final int id;
  final String userName;
  final String email;
  final int privacy;
  final String name;
  final String intro;
  final String dob;
  final int gender;
  final String phone;
  final int regType;
  final String regIdentity;
  final String avatar;
  final String cover;
  final String postCount;
  final int listened;
  final String follower;
  final String following;
  final String bio;
  final String token;
  final bool needSetMusic;

  // Get , set
  String get getPhone => phone ?? '';

  factory Personal.fromJson(Map<String, dynamic> json) {
    if (json == null) {
      return null;
    }

    final _mapProfile = json['profile'];

    return Personal(
      id: _mapProfile['id'] ?? 0,
      userName: _mapProfile['userName'] ?? '',
      email: _mapProfile['email'] ?? '',
      privacy: _mapProfile['privacy'],
      name: _mapProfile['name'],
      intro: _mapProfile['intro'],
      dob: _mapProfile['dob'],
      gender: _mapProfile['gender'],
      phone: _mapProfile['phone_number'] ?? '',
      regType: _mapProfile['reg_type'],
      regIdentity: _mapProfile['reg_identity'] ?? '',
      avatar: _mapProfile['avatar'] ?? '',
      cover: _mapProfile['cover'] ?? '',
      token: json['token'] ?? '',
      needSetMusic: json['need_set_music'] ?? '',
    );
  }

  Personal copyWith({
    final String name,
    final String email,
    final String phone,
    final String userName,
    final String token,
    final String follower,
    final String following,
    final String bio,
    final int listened,
  }) {
    return Personal(
      id: id,
      name: name ?? this.name,
      userName: userName ?? this.userName,
      email: email ?? this.email,
      phone: phone ?? this.phone,
      follower: follower ?? this.follower,
      following: following ?? this.following,
      bio: bio ?? this.bio,
      listened: listened ?? this.listened,
      token: token ?? this.token,
    );
  }

  Map<String, dynamic> toJson() => {
        'profile': {
          'id': id,
          'userName': userName,
          'email': email,
          'privacy': privacy,
          'name': name,
          'intro': intro,
          'dob': dob,
          'gender': gender,
          'phone_number': phone,
          'reg_type': regType,
          'reg_identity': regIdentity,
          'avatar': avatar,
          'cover': cover,
        },
        'token': token,
        'need_set_music': needSetMusic,
      };
}
