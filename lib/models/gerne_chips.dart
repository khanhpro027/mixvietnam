class GerneChips {
  GerneChips({this.id, this.isChoose = false, this.text});

  final String id;
  final String text;
  bool isChoose;

  factory GerneChips.fromText(String text) {
    return GerneChips(text: text, isChoose: false);
  }
}
