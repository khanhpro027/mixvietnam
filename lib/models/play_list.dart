import 'package:mixvietnam/models/app_model.dart';
import 'package:mixvietnam/models/gerne_chips.dart';

class PlayList extends AppModel {
  PlayList({this.authorName, this.avatar, this.songName, this.tab});
  final String authorName;
  final String songName;
  final String avatar;
  final GerneChips tab;

  @override
  List<Object> get props => throw UnimplementedError();

  @override
  String get id => throw UnimplementedError();
}
