class Singer {
  Singer({this.follower, this.isFlow, this.name, this.avatar});

  final String name;
  final String follower;
  final String avatar;
  bool isFlow;
}
