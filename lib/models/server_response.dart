import 'package:mixvietnam/base/app_urls.dart';
import 'package:mixvietnam/constants/lengths.dart';
import 'package:mixvietnam/constants/strings.dart';

class ServerResponse {
  const ServerResponse({this.code, this.msg, this.data, this.appError});

  final int code;
  final String msg;
  final dynamic data;
  final AppError appError;

  factory ServerResponse.fromJson(Map<String, dynamic> json) => ServerResponse(
        code: json['code'] ?? STATUS_ERROR,
        msg: json['msg'] ?? STR_NETWORK_ERROR,
        data: json['data'] ?? '',
      );

  Map<String, dynamic> toJson() => {
        'code': code,
        'msg': msg,
        'data': data,
      };
}

// class Data {
//   Data({
//     this.found,
//   });

//   bool found;

//   factory Data.fromJson(Map<String, dynamic> json) => Data(
//         found: json['found'],
//       );

//   Map<String, dynamic> toJson() => {
//         'found': found,
//       };
// }
