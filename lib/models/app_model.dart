import 'package:equatable/equatable.dart';

/// Normal app model
abstract class AppModel extends Equatable {
  @override
  const AppModel();

  @override
  factory AppModel.fromJson() => null;

  /// Return the ID of model instance
  String get id;

  /// Return true if instance exists
  bool get exists => id != null && id.isNotEmpty;
}
