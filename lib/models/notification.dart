class NotificationModel {
  NotificationModel(
      {this.avatar, this.content, this.isRead, this.time, this.title});

  final String avatar;
  final String title;
  final String content;
  final String time;
  bool isRead;
}
