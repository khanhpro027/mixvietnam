
import 'package:mixvietnam/models/app_model.dart';

class MusicPostModel extends AppModel {
  const MusicPostModel({
    this.musicId,
    this.title,
    this.author,
    this.postTime,
    this.postion,
    this.countPlayList,
    this.postUserName,
    this.content,
    this.time,
    this.tag,
    this.durarion,
    this.like,
    this.avatar,
    this.listented,
  });

  factory MusicPostModel.fromJson(dynamic json) {
    if (json == null) {
      return null;
    }

    return MusicPostModel(
      musicId: json['id'],
      title: json['content_text'],
      postTime: json['created_at'],

      // userAnswers: lookup<List>(json, ['user_answers'])
      //         ?.map<TblAnswerHistory>((o) => TblAnswerHistory.fromJson(o))
      //         ?.toList() ??
      //     <TblAnswerHistory>[],
    );
  }

  final int musicId;
  final String title;
  final String author;
  final String postTime;
  final int countPlayList;
  final String postion;
  final String durarion;
  final int like;
  final int listented;
  final String postUserName;
  final String time;
  final String content;
  final String tag;
  final String avatar;

  @override
  String get id => musicId.toString();

  @override
  List<Object> get props => [
        musicId,
        title,
        postTime,
      ];
}
