class RegisterPrepareModel {
  RegisterPrepareModel({
    this.verifyCode,
    this.encryptedKey,
  });

  String verifyCode;
  String encryptedKey;

  factory RegisterPrepareModel.fromJson(Map<String, dynamic> json) =>
      RegisterPrepareModel(
        verifyCode: json['verify_code'],
        encryptedKey: json['encrypted_key'],
      );

  Map<String, dynamic> toJson() => {
        'verify_code': verifyCode,
        'encrypted_key': encryptedKey,
      };
}
