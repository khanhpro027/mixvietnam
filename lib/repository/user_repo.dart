import 'dart:async';

import 'package:mixvietnam/base/app_api_urls.dart';
import 'package:mixvietnam/base/app_helpers.dart';
import 'package:mixvietnam/constants/common.dart';
import 'package:mixvietnam/constants/lengths.dart';
import 'package:mixvietnam/constants/strings.dart';
import 'package:mixvietnam/models/personal.dart';

class UserRepo {
  static Future<bool> updateMyProfile({Personal user}) async {
    final serverResponse = await AppApiUrls.urlUpdateMyProfile.withHeader(
      headers: {
        'accept': 'application/json',
        'Content-Type': HTTP_CONTENT_TYPE_FORM_DATA,
        'Authorization': 'Token ${user.token}',
      },
    ).withData(
      {
        'name': user.name,
        'username': user.userName,
      },
    ).post();

    if (serverResponse.code == STATUS_SUCCESS) {
      return FLAG_ON;
    } else {
      // Show error message
      AppHelpers.showToast(text: serverResponse.msg);
      return IS_NULL;
    }
  }
}
