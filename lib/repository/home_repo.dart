import 'dart:async';

import 'package:mixvietnam/base/app_api_urls.dart';
import 'package:mixvietnam/base/app_helpers.dart';
import 'package:mixvietnam/base/app_urls.dart';
import 'package:mixvietnam/constants/lengths.dart';
import 'package:mixvietnam/models/music_post_model.dart';
import 'package:mixvietnam/constants/globals.dart' as globals;


class HomeRepo {
  static FutureOr<dynamic> getMusicPosts(String token) async {
    final result = await AppApiUrls.urlFollowerList.withData({
      'token': token,
    }).fetch();

    if (result is AppError) {
      return result;
    }

    return MusicPostModel.fromJson(result);
  }

  static FutureOr<List<MusicPostModel>> getRecentlyPostList({int page}) async {
    final serverResponse = await AppApiUrls.urlGetRecentlyPostList.withHeader(
      headers: {
        'Authorization': 'Token ${globals.user.token}',
      },
    ).withData(
      {
        'paging': {'page': page, 'page_size': 20}
      },
    ).post();

    if (serverResponse.code == STATUS_SUCCESS) {
      final _json = serverResponse.data;

      // Parse list genre
      final _listGenre = lookup<List>(_json, ['records'])
              ?.map<MusicPostModel>((o) => MusicPostModel.fromJson(o))
              ?.toList() ??
          <MusicPostModel>[];

      return _listGenre;
    } else {
      return [];
    }
  }
}
