import 'dart:async';

import 'package:flutter/material.dart';
import 'package:mixvietnam/base/app_api_urls.dart';
import 'package:mixvietnam/base/app_cache.dart';
import 'package:mixvietnam/base/app_helpers.dart';
import 'package:mixvietnam/base/app_urls.dart';
import 'package:mixvietnam/constants/bool.dart';
import 'package:mixvietnam/constants/common.dart';
import 'package:mixvietnam/constants/lengths.dart';
import 'package:mixvietnam/constants/strings.dart';
import 'package:mixvietnam/models/personal.dart';
import 'package:mixvietnam/models/register_prepare_model.dart';

class AuthRepo {
  static FutureOr<bool> authCheck({
    BuildContext context,
    String email = '',
    String username = '',
  }) async {
    final serverResponse = await AppApiUrls.urlAuthCheck.withData({
      'email': email,
      'username': username,
    }).post();

    // Turn off loading
    AppHelpers.onLoading(context, flag: false);

    // Check result code
    if (serverResponse.code == STATUS_SUCCESS) {
      final found = serverResponse.data['found'] as bool;

      if (found) {
        // Show error message
        AppHelpers.showToast(text: MSG_REGISTER_EXIST_ACCOUNT);
      }

      return found ? CHECK_IS_FALSE : CHECK_IS_TRUE;
    } else {
      // Show error message
      AppHelpers.showToast(text: serverResponse.msg);
      return CHECK_IS_FALSE;
    }
  }

  static Future<RegisterPrepareModel> registerPrepare(
      {String email, String pass}) async {
    final serverResponse = await AppApiUrls.urlRegisterPrepare.withData({
      'email': email,
      'password': pass,
    }).post();

    if (serverResponse.code == STATUS_SUCCESS) {
      return RegisterPrepareModel.fromJson(serverResponse.data);
    } else {
      // Show error message
      AppHelpers.showToast(text: serverResponse.msg);
      return IS_NULL;
    }
  }

  static Future<String> registerConfirm({
    Personal user,
    RegisterPrepareModel registerPrepare,
    String otp,
  }) async {
    final serverResponse = await AppApiUrls.urlConfirmRegister.withData({
      'email': user.email,
      'verify_code': registerPrepare.verifyCode,
      'encrypted_key': registerPrepare.encryptedKey,
      'otp': otp,
    }).post();

    if (serverResponse.code == STATUS_SUCCESS) {
      return serverResponse.data['token'];
    } else {
      // Show error message
      AppHelpers.showToast(text: serverResponse.msg);
      return IS_NULL;
    }
  }

  static FutureOr<void> login({
    String user,
    String pass,
    ValueChanged<Personal> onLoginSuccess,
    VoidCallback onLoginError,
  }) async {
    final serverResponse = await AppApiUrls.urlAuthLogin.withData({
      'identity': user,
      'password': pass,
    }).post();

    if (serverResponse.code == STATUS_SUCCESS) {
      final personal = Personal.fromJson(serverResponse.data);

      // Cache user
      AppCache().user = personal;

      // Call event
      onLoginSuccess.call(personal);
    } else {
      // Show error message
      AppHelpers.showToast(text: serverResponse.msg);

      // Call event
      onLoginError.call();
    }
  }

  static FutureOr<dynamic> logout(String userToken) async {
    final result = await AppApiUrls.urlLogout.withData({
      'token': userToken,
    }).fetch();

    if (result is AppError) {
      return result;
    }

    return result;
  }
}
