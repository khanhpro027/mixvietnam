import 'dart:async';

import 'package:flutter/material.dart';
import 'package:mixvietnam/base/app_api_urls.dart';
import 'package:mixvietnam/base/app_helpers.dart';
import 'package:mixvietnam/constants/common.dart';
import 'package:mixvietnam/constants/lengths.dart';
import 'package:mixvietnam/constants/globals.dart' as globals;
import 'package:mixvietnam/models/gerne_chips.dart';

class GemeRepo {
  static FutureOr<List<GerneChips>> getListGenre() async {
    final serverResponse = await AppApiUrls.urlGetMusicTypeList.withHeader(
      headers: {
        'Authorization': 'Token ${globals.user.token}',
      },
    ).fetch();

    if (serverResponse.code == STATUS_SUCCESS) {
      final _json = serverResponse.data;

      // Parse list genre
      final _listGenre = lookup<List>(_json, ['types'])
              ?.map<GerneChips>((o) => GerneChips.fromText(o.toString()))
              ?.toList() ??
          <GerneChips>[];

      return _listGenre;
    } else {
      // Show error message
      AppHelpers.showToast(text: serverResponse.msg);
      return [
        GerneChips.fromText('Pop'),
        GerneChips.fromText('Dance'),
        GerneChips.fromText('Jazz'),
        GerneChips.fromText('Disco'),
        GerneChips.fromText('Dub'),
        GerneChips.fromText('EDM'),
        GerneChips.fromText('Funk'),
        GerneChips.fromText('Hip Hop')
      ];
    }
  }

  static FutureOr<void> saveListGenre({
    List<String> genres,
    VoidCallback onSuccess,
    VoidCallback onError,
  }) async {
    final serverResponse = await AppApiUrls.urlSetFavouriteMusics.withHeader(
      headers: {
        'accept': 'application/json',
        'Content-Type': HTTP_CONTENT_TYPE_JSON,
        'Authorization': 'Token ${globals.user.token}',
      },
    ).withData({'types': genres}).post();

    if (serverResponse.code == STATUS_SUCCESS) {
      // Call event onSuccess
      onSuccess?.call();
    } else {
      // Show error message
      AppHelpers.showToast(text: serverResponse.msg);
      onError?.call();
    }
  }
}
