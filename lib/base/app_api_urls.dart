import 'package:mixvietnam/base/app_urls.dart';

class AppApiUrls {
  static const staging = 'https://test.mixvietnam.com';
  static const produce = 'https://mixvietnam.com';

  static const prefix = staging;
  static const user = 'user';
  static const social = 'social/v1';

  /// ============ AUTH ============ ///

  // User Check
  static const urlAuthCheck = AppUrl.api('$prefix/$user/v1/auth/check/');

  // User Login
  static const urlAuthLogin = AppUrl.api('$prefix/$user/v1/auth/login/');

  // User Logout
  static const urlLogout = AppUrl.api('$prefix/$user/v1/auth/logout/');

  // User Login with Social
  static const urlAuthLoginSocial =
      AppUrl.api('$prefix/$user/v1/auth/login/social/');

  // Change password confirm
  static const urlChangePasswordConfirm =
      AppUrl.api('$prefix/$user/v1/auth/password/change/confirm/');

  // Change password prepare
  static const urlChangePasswordPrepare =
      AppUrl.api('$prefix/$user/v1/auth/password/change/prepare/');

  // Confirm register
  static const urlConfirmRegister =
      AppUrl.api('$prefix/$user/v1/auth/register-confirm/');

  // Register Prepare
  static const urlRegisterPrepare =
      AppUrl.api('$prefix/$user/v1/auth/register-prepare/');

  /// ============ USER ============ ///

  // Update my profile
  static const urlUpdateMyProfile =
      AppUrl.api('$prefix/$user/v1/profile/update/');

  /// ============ PROFILE ============ ///

  // Set favourite musics
  static const urlSetFavouriteMusics =
      AppUrl.api('$prefix/$user/v1/profile/music/set/');

  // Get my profile
  static const urlGetMyProfile = AppUrl.api('$prefix/$user/v1/profile/read/');

  // Get music type list
  static const urlGetMusicTypeList =
      AppUrl.api('$prefix/$user/v1/setting/music/types/');

  /// ============ HOME ============ ///

  // Follow
  static const urlFollow = AppUrl.api('$prefix/$social/user/follow/perform/');

  // Follower List
  static const urlFollowerList =
      AppUrl.api('$prefix/$social/user/follower/list/');

  // Following List
  static const urlFollowingList =
      AppUrl.api('$prefix/$social/user/following/list/');

  // Listen list
  static const urlListenList = AppUrl.api('$prefix/$social/user/listen/list/');

  // Listen perform
  static const urlUserListenPerform =
      AppUrl.api('$prefix/$social/user/listen/perform/');

  // Follow community
  static const urlFollowCommunity =
      AppUrl.api('$prefix/$social/user/community/follow/');

  // Following Community List
  static const urlFollowingCommunityList =
      AppUrl.api('$prefix/$social/user/community/list/');

  // Get user profile
  static const urlGetUserProfile = AppUrl.api('$prefix/$social/user/profile/');

  // Reaction list
  static const urlGetReactionList =
      AppUrl.api('$prefix/$social/user/reaction/list/');

  // Get user insight
  static const urlGetUserInsight = AppUrl.api('$prefix/$social/user/insight/');

  // Create community
  static const urlCreateCommunity =
      AppUrl.api('$prefix/$social/community/create/');

  // Follower list
  static const urlGetFollowerList =
      AppUrl.api('$prefix/$social/community/follower/list/');

  // Get community insight
  static const urlGetcommunityInsight =
      AppUrl.api('$prefix/$social/community/insight/');

  // Get community
  static const urlGetCommunity = AppUrl.api('$prefix/$social/community/read/');

  // Update community
  static const urlUpdateCommunity =
      AppUrl.api('$prefix/$social/community/update/');

  // Create playlist
  static const urlCreatePlaylist =
      AppUrl.api('$prefix/$social/playlist/create/');

  // Delete playlist
  static const urlDeletePlaylist =
      AppUrl.api('$prefix/$social/playlist/delete/');

  // Playlist Item List
  static const urlPlaylistItemList =
      AppUrl.api('$prefix/$social/playlist/item/list/');

  // Playlist Item List
  static const urlGetListPlaylist =
      AppUrl.api('$prefix/$social/playlist/list/');

  // Playlist listen perform
  static const urlPlaylistListenPerform =
      AppUrl.api('$prefix/$social/playlist/listen/perform/');

  // Get playlist
  static const urlGetPlaylist = AppUrl.api('$prefix/$social/playlist/read/');

  // Update playlist
  static const urlUpdatePlaylist =
      AppUrl.api('$prefix/$social/playlist/update/');

  // Media Upload Presigned URL
  static const urlMediaUploadPresignedURL =
      AppUrl.api('$prefix/$social/media/presigned_url/');

  // Create post
  static const urlCreatePost = AppUrl.api('$prefix/$social/post/create/');

  // Delete post
  static const urlDeletePost = AppUrl.api('$prefix/$social/post/delete/');

  // Update post
  static const urlUpdatePost = AppUrl.api('$prefix/$social/post/update/');

  // Get list post
  static const urlGetListPost = AppUrl.api('$prefix/$social/post/list/');

  // Get post
  static const urlGetPost = AppUrl.api('$prefix/$social/post/read/');

  // Share list post
  static const urlShareListPost =
      AppUrl.api('$prefix/$social/post/share/list/');

  // Post react list
  static const urlPostReactList =
      AppUrl.api('$prefix/$social/post/react/list/');

  // Post react perform
  static const urlPostReactPerform =
      AppUrl.api('$prefix/$social/post/react/perform/');

  // Create comment
  static const urlCreateComment =
      AppUrl.api('$prefix/$social/post/comment/create/');

  // Update comment
  static const urlUpdateComment =
      AppUrl.api('$prefix/$social/post/comment/update/');

  // Get comment list
  static const urlGetCommentLists =
      AppUrl.api('$prefix/$social/post/comment/list/');

  // Get react comment list
  static const urlGetReactCommentLists =
      AppUrl.api('$prefix/$social/post/comment/react/list/');

  // Get react comment perform
  static const urlGetReactCommentPerform =
      AppUrl.api('$prefix/$social/post/comment/react/perform/');

  // Add post to playlist
  static const urlAddToPlaylist =
      AppUrl.api('$prefix/$social/post/add_to_playlist/');

  // Hashtag post list
  static const urlGetHashtagPostList =
      AppUrl.api('$prefix/$social/post/hashtag/');

  // Recently post list
  static const urlGetRecentlyPostList =
      AppUrl.api('$prefix/$social/post/recently/');

  // Search post list by type
  static const urlSearchPostListByType =
      AppUrl.api('$prefix/$social/post/search/');

  // Search post list with all type
  static const urlSearchPostListWithAllType =
      AppUrl.api('$prefix/$social/post/search/all/');

  // Search post list
  static const urlRecentlySearchPostList =
      AppUrl.api('$prefix/$social/post/search/recently/');

  // Create notification
  static const urlCreateNotification =
      AppUrl.api('$prefix/$social/notification/list/');

  // Mark notification as read
  static const urlMarkNotification =
      AppUrl.api('$prefix/$social/notification/mark_as_read/');

  // List activity
  static const urlListActivity = AppUrl.api('$prefix/$social/activity/list/');

  // Orther insight
  static const urlInsight = AppUrl.api('$prefix/$social/insight/');

  // Privacy list
  static const urlPrivacyList = AppUrl.api('$prefix/$social/privacy/list/');
}
