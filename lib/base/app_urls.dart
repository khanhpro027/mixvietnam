import 'dart:async';
import 'dart:io';

import 'package:mixvietnam/base/app_config.dart';
import 'package:mixvietnam/base/app_helpers.dart';
import 'package:mixvietnam/constants/common.dart';
import 'package:mixvietnam/constants/lengths.dart';
import 'package:mixvietnam/constants/strings.dart';
import 'package:mixvietnam/models/server_response.dart';

enum AppError { Format, NetWork, Timeout, SocketException }

class AppUrl {
  const AppUrl._({this.url, this.data, this.headers});

  final String url;
  final Map<String, String> headers;
  final Map<String, dynamic> data;

  const AppUrl.api(String url) : this._(url: url);

  /// Clone current instance with new options
  AppUrl copyWith({
    String url,
    Map<String, String> headers,
    Map<String, dynamic> data,
  }) =>
      AppUrl._(
        url: url ?? this.url,
        headers: headers ?? this.headers,
        data: data ?? this.data,
      );

  /// Clone with header
  AppUrl withHeader({Map<String, String> headers}) =>
      copyWith(headers: headers);

  /// Clone with data
  AppUrl withData(Map<String, dynamic> data) => copyWith(data: data);

  /// Fetch data from URL
  Future<ServerResponse> fetch() async {
    try {
      return _normalizeResult(
        await AppConfig.apiClient?.fetch(url, headers: headers, data: data),
      );
    } on dynamic catch (e, t) {
      return _normalizeError(e, t, url: url, data: data);
    }
  }

  /// Post data to URL
  Future<ServerResponse> post({String checksum}) async {
    try {
      return _normalizeResult(
        await AppConfig.apiClient?.post(
          url,
          headers: headers,
          data: data,
        ),
      );
    } on dynamic catch (e, t) {
      return _normalizeError(e, t, url: url, data: data);
    }
  }

  // Re-form the response data
  ServerResponse _normalizeResult(dynamic result) {
    if (result is Map && result.containsKey('code')) {
      return ServerResponse.fromJson(result);
    }

    return const ServerResponse(
      code: STATUS_ERROR,
      appError: AppError.NetWork,
      msg: STR_NETWORK_ERROR,
    );
  }

  // Re-form the response error
  ServerResponse _normalizeError(
    dynamic error,
    StackTrace trace, {
    String url,
    dynamic data,
  }) {
    printDebug('Exception : $error');

    switch (error) {
      case TimeoutException:
        return ServerResponse(
          code: STATUS_ERROR,
          appError: AppError.Timeout,
          msg: error.toString(),
        );
        break;

      case FormatException:
        return ServerResponse(
          code: STATUS_ERROR,
          appError: AppError.Format,
          msg: error.toString(),
        );
        break;

      case SocketException:
        return ServerResponse(
          code: STATUS_ERROR,
          appError: AppError.SocketException,
          msg: error.toString(),
        );
        break;

      default:
        return const ServerResponse(
          code: STATUS_ERROR,
          appError: AppError.NetWork,
          msg: STR_NETWORK_ERROR,
        );
    }
  }
}

//// Helper class that generates full URLs based on environment
class UrlUtils {
  static AppEnv _env = AppEnv.Production;

  /// Getter for current environment
  static AppEnv get currentEnv {
    if (!IS_DEBUG_MODE) {
      return AppEnv.Production;
    }

    return _env;
  }

  /// Setter for current environment
  static set currentEnv(AppEnv mode) {
    _env = mode;
  }

  /// Get API endpoint's base URL
  static String get baseUrl {
    return '';
  }

  // Append data to the request URL and normalize the final url
  static Uri appendData(Uri uri, [Map<String, dynamic> data]) {
    if (data != null && data.isNotEmpty) {
      dynamic _makeStringQuery(dynamic value) {
        if (value is Iterable) {
          return value.map((child) => _makeStringQuery(child));
        }

        return value?.toString();
      }

      final Map<String, dynamic> tmp = {};

      // Add values from origin uri and data
      tmp.addAll(uri.queryParameters);
      tmp.addAll(data ?? {});

      // Convert array key into key[], value to String or Iterable<String>
      final Map<String, dynamic> query = tmp.map<String, dynamic>(
          (key, value) => MapEntry(
              value is Iterable ? '$key[]' : key, _makeStringQuery(value)));

      // Remove redundant parameters before sending the request
      query.removeWhere((key, value) => value == null || value.isEmpty);

      // Update the uri with new parameters
      return uri
          .replace(queryParameters: query)
          .removeFragment()
          .normalizePath();
    }

    return uri.removeFragment().normalizePath();
  }

  static String getUrl(
    String path, {
    Map<String, dynamic> params,
    bool timestamp = true,
  }) {
    String url;

    if (path == null || path.isEmpty || path == '/') {
      url = baseUrl;
    } else if (isUrl(path)) {
      url = path;
    } else {
      url = baseUrl + path.replaceAll(RegExp('^/+'), '');
    }

    final Map<String, dynamic> data = {};
    final uri = Uri.tryParse(url);

    if (params != null) {
      data.addAll(params);
    }

    if (timestamp) {
      data.putIfAbsent(
          '_t', () => DateTime.now().millisecondsSinceEpoch.toString());
    }

    return appendData(uri, data).toString();
  }

  static bool isUrl(String url) {
    return 'http://'.matchAsPrefix(url) != null ||
        'https://'.matchAsPrefix(url) != null;
  }
}
