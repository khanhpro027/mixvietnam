import 'dart:async';
import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:mixvietnam/base/app_config.dart';
import 'package:mixvietnam/constants/common.dart';

class AppApiClient extends http.BaseClient {
  AppApiClient({this.timeout = API_TIMEOUT});

  final Duration timeout;

  @override
  Future<http.StreamedResponse> send(http.BaseRequest request) async {
    http.StreamedResponse response;
    // HttpMetric metric;

    try {
      // metric = FirebasePerformance.instance
      //     .newHttpMetric(request.url.toString(), HttpMethod.Get);
      // await metric?.start();
    } finally {}

    try {
      _appendUa(request.headers);
      response = await http.Client().send(request).timeout(timeout);

      // if (metric != null) {
      //   metric
      //     ..responsePayloadSize = response.contentLength
      //     ..responseContentType = response.headers['Content-Type']
      //     ..requestPayloadSize = response.request.contentLength
      //     ..httpResponseCode = response.statusCode;
      // }
    } on Exception {
      rethrow;
    } finally {
      // await metric?.stop();
      _logApiResponse(response);
    }

    return response;
  }

  @override
  Future<http.Response> get(Uri url, {Map<String, String> headers}) =>
      _proxy('get', url, headers: headers);

  @override
  Future<http.Response> post(Uri url,
          {Map<String, String> headers, dynamic body, Encoding encoding}) =>
      _proxy('post', url, headers: headers, body: body, encoding: encoding);

  @override
  Future<http.Response> patch(Uri url,
          {Map<String, String> headers, dynamic body, Encoding encoding}) =>
      _proxy('patch', url, headers: headers, body: body, encoding: encoding);

  @override
  Future<http.Response> put(Uri url,
          {Map<String, String> headers, dynamic body, Encoding encoding}) =>
      _proxy('put', url, headers: headers, body: body, encoding: encoding);

  @override
  Future<http.Response> head(Uri url, {Map<String, String> headers}) =>
      _proxy('head', url, headers: headers);

  @override
  Future<http.Response> delete(Uri url,
          {Map<String, String> headers, dynamic body, Encoding encoding}) =>
      _proxy('delete', url, headers: headers);

  FutureOr<http.Response> _proxy(
    String method,
    Uri uri, {
    Map<String, String> headers,
    dynamic body,
    Encoding encoding,
  }) async {
    http.Response response;

    try {
      final client = http.Client();
      _appendUa(headers);

      switch (method) {
        case 'post':
          response = await client
              .post(uri, headers: headers, body: body, encoding: encoding)
              .timeout(timeout);
          break;
        case 'patch':
          response = await client
              .patch(uri, headers: headers, body: body, encoding: encoding)
              .timeout(timeout);
          break;
        case 'put':
          response = await client
              .put(uri, headers: headers, body: body, encoding: encoding)
              .timeout(timeout);
          break;
        case 'head':
          response = await client.head(uri, headers: headers).timeout(timeout);
          break;
        case 'delete':
          response =
              await client.delete(uri, headers: headers).timeout(timeout);
          break;
        default:
          response = await client.get(uri, headers: headers).timeout(timeout);
          break;
      }
    } on Exception {
      rethrow;
    } finally {
      _logApiResponse(response);
    }

    return response;
  }

  // Append custom data
  void _appendUa(Map<String, String> headers) {
    final escapeExp = RegExp(r'[^\u0001-\u007F]');

    // Add user-agent
    headers?.addAll({
      'User-Agent': AppConfig.userAgent.replaceAll(escapeExp, '_'),
    });

    // App Info
    headers?.addAll({
      'X-App-Bundle': AppConfig.packageInfo.packageName,
      'X-App-Version': AppConfig.version,
    });
  }

  // Print response info (Debug only)
  void _logApiResponse(http.BaseResponse response) {
    if (response is http.Response) {
      final http.Request request = response.request;
      final String url = request.url.toString();
      final String method = request.method.toUpperCase();

      AppConfig.debug(
        '-------------------------------------\n'
        'Response for: $method $url\n',
      );

      if (API_LOG_RESPONSE) {
        final String body = response.body;
        AppConfig.debug('Sent Data: ${request.body}');
        AppConfig.debug('Response body: $body');
      }
    }
  }
}
