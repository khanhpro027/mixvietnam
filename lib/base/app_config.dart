import 'dart:async';
import 'dart:io';

import 'package:device_info/device_info.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:mixvietnam/base/app_cache.dart';
import 'package:mixvietnam/base/app_json_client.dart';
import 'package:mixvietnam/constants/common.dart';
import 'package:logging/logging.dart';
import 'package:mixvietnam/constants/strings.dart';
import 'package:mixvietnam/constants/styles.dart';
import 'package:package_info/package_info.dart';
import 'package:mixvietnam/constants/globals.dart' as globals;

/// App environment
enum AppEnv {
  Staging,
  Production,
}

/// App configurations
class AppConfig {
  static String _ua;
  static String _version;

  // Number formatter
  static final NumberFormat formatter = NumberFormat(UI_NUMBER_FM);
  static final DateFormat date = DateFormat(UI_DATE_FM);
  static final DateFormat dateMMdd = DateFormat(UI_DATE_MMDD);
  static final DateFormat datetime = DateFormat(UI_DATETIME_FM);
  static final DateFormat datetimeHHmm = DateFormat(UI_DATETIME_HHMM_FM);
  static final DateFormat datetimeGeneral = DateFormat(UI_DATETIME);
  static final DateFormat datetimeFM = DateFormat(UI_DATE_MMDD_FM);
  static final DateFormat dateMMMd = DateFormat(UI_DATE_MMMD);
  static final DateFormat dateMMMdyyyy = DateFormat(UI_DATE_MMMDYYYY);

  // Final instances
  static final AppCache storage = AppCache();
  static final AppJsonClient apiClient = AppJsonClient();
  static final Style style = Style();

  // Runtime device info
  static PackageInfo packageInfo;
  static IosDeviceInfo ios;
  static AndroidDeviceInfo android;

  // Save previous UI adjustment flag
  static bool _previousUiAdjustment;

  /// Init build context
  static BuildContext appContext;

  static Completer _appContextCompleter = Completer<BuildContext>();

  static void initContext(BuildContext context) {
    if (context != null && appContext != context) {
      appContext = context;

      if (_appContextCompleter == null || _appContextCompleter.isCompleted) {
        _appContextCompleter = Completer<BuildContext>();
      }

      _appContextCompleter.complete(appContext);
    }
  }

  static Future<BuildContext> ensureContextExists() =>
      _appContextCompleter.future;

  //----------------------------------------------------------------------------
  // Constructor
  //----------------------------------------------------------------------------

  // Init the app settings
  static Future<void> init([BuildContext context]) async {
    // Set default behavior for Equatable
    EquatableConfig.stringify = true;

    // Load all settings from storage
    await storage.init();

    // Init firebase app
    await initFirebase();

    // Get device info
    await _fetchDeviceInfo();

    // Package info
    packageInfo = await PackageInfo.fromPlatform();

    // Build context
    initContext(context);

    // Build user agent string
    debug(userAgent);

    // Debug all app configurations
    _debugConfig();

    // Init user
    globals.user = storage.user;
  }

  /// Get user agent
  static String get userAgent {
    String _compileUa() {
      final String versionString =
          '${packageInfo.packageName}/${packageInfo.version}+${packageInfo.buildNumber}';
      String deviceString = 'Unknown device';

      if (ios != null) {
        deviceString = '${ios.model}/${ios.utsname.machine}; '
            '${ios.systemName}/${ios.systemVersion}; '
            '${ios.utsname.sysname}/${ios.utsname.release}/'
            '${ios.isPhysicalDevice ? ios.name : 'simulator'}';
      } else if (android != null) {
        deviceString = '${android.manufacturer} ${android.model}; '
            'Android/${android.version.release}; '
            '${android.hardware}/${android.device}/'
            '${android.isPhysicalDevice ? android.display : 'simulator'}';
      }

      return '$versionString ($deviceString)'.trim();
    }

    return _ua ??= _compileUa();
  }

  /// Get app version
  static String get version =>
      _version ??= '${packageInfo.version}+${packageInfo.buildNumber}';

  static Future<void> initFirebase() async {
    // To initialize FlutterFire
    // await Firebase.initializeApp();
  }

  //----------------------------------------------------------------------------
  // App settings
  //----------------------------------------------------------------------------

  /// Try go to top screen
  static void tryGoToAppRoot() {
    ensureContextExists().then((context) {
      try {} finally {}
    });
  }

  /// Try logout from everywhere
  static void tryLogout() {
    ensureContextExists().then((context) {
      // logout
    });
  }

  //----------------------------------------------------------------------------
  // System UI settings
  //----------------------------------------------------------------------------

  static void setUiBrightness({Brightness brightness}) {
    if (Platform.isIOS) {
      if (brightness == null && _previousUiAdjustment != null) {
        SystemChrome.restoreSystemUIOverlays();
        WidgetsBinding.instance.renderView.automaticSystemUiAdjustment =
            _previousUiAdjustment ?? true;
      } else if (brightness != null) {
        _previousUiAdjustment =
            WidgetsBinding.instance.renderView.automaticSystemUiAdjustment;

        WidgetsBinding.instance.renderView.automaticSystemUiAdjustment = false;
        SystemChrome.setSystemUIOverlayStyle(brightness == Brightness.light
            ? SystemUiOverlayStyle.dark
            : SystemUiOverlayStyle.light);
      }
    }
  }

  //----------------------------------------------------------------------------
  // Protected helper methods
  //----------------------------------------------------------------------------

  static Future<void> _fetchDeviceInfo() async {
    final _deviceInfoPlugin = DeviceInfoPlugin();

    ios = Platform.isIOS ? await _deviceInfoPlugin.iosInfo : null;
    android = Platform.isAndroid ? await _deviceInfoPlugin.androidInfo : null;
  }

  //----------------------------------------------------------------------------
  // App debug
  //----------------------------------------------------------------------------

  static void debug(dynamic object, {Level level = Level.ALL}) {
    if (IS_DEBUG_MODE && ENABLE_DEBUG_OUTPUT) {
      debugPrint(object.toString());
    }
  }

  static void verbose(dynamic object, {Level level = Level.ALL}) {
    debugPrint(object.toString());
  }

  static void _debugConfig() {
    if (IS_DEBUG_MODE) {
      debugPrint('----------------------IS_DEBUG_MODE----------------------');
    }
  }

  static bool get isShowRatingApp {
    final DateTime before = DateTime.fromMillisecondsSinceEpoch(1640979000000);
    final DateTime now = DateTime.now();
    final dayDifference = now.difference(before).inDays;
    if (dayDifference >= 365) {
      return true;
    }
    return false;
  }
}
