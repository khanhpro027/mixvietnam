import 'dart:async';
import 'dart:convert';

import 'package:mixvietnam/base/app_api_client.dart';
import 'package:mixvietnam/base/app_urls.dart';
import 'package:http/http.dart' as http;
import 'package:mixvietnam/constants/common.dart';

class AppJsonClient {
  final _client = AppApiClient();
  final _headers = {
    'accept': HTTP_CONTENT_TYPE_JSON,
    'Content-Type': HTTP_CONTENT_TYPE_JSON,
  };

  /// Call API with GET method.
  FutureOr<dynamic> fetch(
    String url, {
    Map<String, String> headers,
    Map<String, dynamic> data,
  }) async {
    if (headers != null) _headers.addAll(headers);

    final http.Response response = await _client.get(
      UrlUtils.appendData(Uri.tryParse(url), data),
      headers: _headers,
    );

    return json.decode(utf8.decode(response.bodyBytes));
  }

  /// Call API with POST method.
  FutureOr<dynamic> post(
    String url, {
    Map<String, String> headers,
    Map<String, dynamic> data,
  }) async {
    if (headers != null) _headers.addAll(headers);

    final http.Response response = await _client.post(
      UrlUtils.appendData(Uri.tryParse(url)),
      headers: _headers,
      body: json.encode(data ?? {}),
    );

    return json.decode(utf8.decode(response.bodyBytes));
  }
}
