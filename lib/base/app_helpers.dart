import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:mixvietnam/constants/colors.dart';
import 'package:mixvietnam/widgets/app_loading_indicator.dart';
import 'package:url_launcher/url_launcher.dart';

R lookup<R>(Map map, Iterable keys) {
  dynamic current = map;

  for (final key in keys) {
    if (current is Map) {
      current = current[key];
    } else if (key is int && current is Iterable && current.length > key) {
      current = current.elementAt(key);
    } else {
      return null;
    }
  }

  try {
    if (current is! R) {
      switch (R) {
        case DateTime:
          current = DateTime.tryParse(current?.toString() ?? '')?.toLocal();
          break;
        case num:
          current = num.tryParse(current?.toString() ?? '');
          break;
        case String:
          current = current?.toString();
          break;
      }
    }

    return current as R;
    // ignore: nullable_type_in_catch_clause
  } on dynamic catch (e) {
    debugPrint('message: ${e.toString()}');
  }

  // return null as default
  return null;
}

void printDebug(String text) {
  debugPrint(text);
}

class AppHelpers {
  static String safeString(String s) => s;

  static int parseInt(dynamic o) {
    if (o == null) return 0;
    return o is int ? o : (int.tryParse(o) ?? 0);
  }

  static String parseString(dynamic o) {
    return o is String ? o : o.toString();
  }

  static String parseArticleIdFromUrl(String url) {
    final _url = Uri.parse(url);
    if (_url.path.isNotEmpty && _url.path.toString().contains('.htm')) {
      return _url.path.split('.').first.split('-').last;
    }
    return null;
  }

  static void onOpenLink({@required String url}) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  static void launchMail(String toMailId, String subject, String body) async {
    final url = 'mailto:$toMailId?subject=$subject&body=$body';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  static void callPhone({String phone = '+84918033133'}) async {
    // await FlutterPhoneDirectCaller.callNumber(phone);
  }

  static void showToast({String text = 'Messege', bool isError = true}) {
    Fluttertoast.showToast(
      msg: '$text',
      toastLength: Toast.LENGTH_LONG,
      gravity: ToastGravity.BOTTOM,
      timeInSecForIosWeb: 1,
      backgroundColor: isError ? PRIMARY_COLOR : SUCCESS_COLOR,
      textColor: Colors.white,
      fontSize: 16.0,
    );
  }

  static void hideKeyboard(FocusScopeNode focusNode) {
    if (focusNode.hasFocus) {
      focusNode.unfocus();
    }
  }

  static void onLoading(BuildContext context, {bool flag = false}) {
    if (flag) {
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return const AppLoadingIndicator();
        },
      );
    } else {
      if (Navigator.of(context).canPop()) {
        Navigator.pop(context);
      }
    }
  }
}
