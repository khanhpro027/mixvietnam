import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:mixvietnam/constants/strings.dart';
import 'package:mixvietnam/models/personal.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AppCache {
  static SharedPreferences _sharedPreferences;

  Future<void> init() async {
    _sharedPreferences ??= await SharedPreferences.getInstance();
  }

  bool isValid(List<String> cacheValueEntry) {
    try {
      if (cacheValueEntry.isNotEmpty && cacheValueEntry.length == 2) {
        final expiryTimestamp = int.parse(cacheValueEntry[0]);
        final isCheck =
            expiryTimestamp >= DateTime.now().millisecondsSinceEpoch;
        return isCheck;
      }
    } catch (e) {
      debugPrint(e.toString());
    }
    return false;
  }

  void setKey(
      {@required String key, @required String value, int expireOn = -1}) {
    final List<String> cacheValueEntry = [expireOn.toString(), value];

    _sharedPreferences?.setStringList(key, cacheValueEntry);
  }

  String getKey({@required String key}) {
    final List<String> cacheValueEntry = _sharedPreferences?.getStringList(key);
    if (isValid(cacheValueEntry)) {
      return cacheValueEntry[1];
    }

    return null;
  }

  void removeKey({@required String key}) {
    _sharedPreferences?.remove(key);
  }

  void purgeCache() {
    final Set<String> keys = _sharedPreferences?.getKeys();
    for (String key in keys) {
      if (getKey(key: key) == null) {
        _sharedPreferences?.remove(key);
      }
    }
  }

  void clearCache() {
    _sharedPreferences?.clear();
  }

  // Function get key

  // App theme setting
  String get themeSetting =>
      _sharedPreferences?.getString(THEME_SETTING) ?? LIGHT_THEME;
  set themeSetting(String value) =>
      _sharedPreferences?.setString(THEME_SETTING, value);

  // Text size setting
  int get textSize => _sharedPreferences?.getInt(TEXT_ZOOM) ?? 100;
  set textSize(int value) => _sharedPreferences?.setInt(TEXT_ZOOM, value);

  // Flag show introduction_screen
  bool get isShowWelcomeScreen =>
      _sharedPreferences?.getBool(FLAG_WELCOME_SCREEN) ?? true;
  set isShowWelcomeScreen(bool value) =>
      _sharedPreferences?.setBool(FLAG_WELCOME_SCREEN, value);

  // Flag login
  bool get logged => _sharedPreferences?.getBool(FLAG_LOGIN) ?? false;
  set logged(bool value) => _sharedPreferences?.setBool(FLAG_LOGIN, value);

  // Info user
  Personal get user {
    final jsonString = _sharedPreferences?.getString(CACHE_USER);

    // Return if null
    if (jsonString?.isEmpty ?? true) {
      return const Personal();
    }

    return Personal.fromJson(json.decode(jsonString)) ?? const Personal();
  }

  set user(Personal personal) {
    final _mapPersonal = personal.toJson();
    _sharedPreferences?.setString(CACHE_USER, json.encode(_mapPersonal));
  }

  // Remember login
  bool get rememberLogin =>
      _sharedPreferences?.getBool(FLAG_REMEMBER_LOGIN) ?? false;
  set rememberLogin(bool value) =>
      _sharedPreferences?.setBool(FLAG_REMEMBER_LOGIN, value);

  // User name
  String get userName => _sharedPreferences?.getString(CACHE_USERNAME) ?? '';
  set userName(String value) =>
      _sharedPreferences?.setString(CACHE_USERNAME, value);

  // Pass
  String get userPass => _sharedPreferences?.getString(CACHE_PASS) ?? '';
  set userPass(String value) =>
      _sharedPreferences?.setString(CACHE_PASS, value);
}

class PersitentStore {
  static Future<bool> save({String key, Map<String, dynamic> jsonModel}) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final String jsonString = json.encode(jsonModel);
    return prefs.setString(key, jsonString);
  }

  static Future<Map<String, dynamic>> load(String key) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    if (!prefs.containsKey(key)) {
      return null;
    }
    final String jsonString = prefs.getString(key);
    return json.decode(jsonString);
  }

  static Future<bool> remove(String key) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    if (!prefs.containsKey(key)) {
      return false;
    }
    return prefs.remove(key);
  }
}
