import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mixvietnam/base/app_cache.dart';
import 'package:mixvietnam/constants/colors.dart';
import 'package:mixvietnam/constants/strings.dart';

class AppTheme {
  final _darkTheme = ThemeData.dark().copyWith(
    primaryColor: CL_WHITE,
    //
    appBarTheme: const AppBarTheme(
      color: CL_PRIMARY_DARK,
      iconTheme: IconThemeData(color: CL_WHITE),
      systemOverlayStyle: SystemUiOverlayStyle.light,
    ),
    //
    tabBarTheme: const TabBarTheme(
      labelColor: CL_WHITE,
      unselectedLabelColor: tabBarTextLabelColor,
    ),
    //
    bottomAppBarTheme:
        const BottomAppBarTheme(color: CL_PRIMARY_DARK, elevation: 10),
    scaffoldBackgroundColor: CL_SECONDARY_DARK,
    brightness: Brightness.dark,
    indicatorColor: CL_PRIMARY_DARK,
    iconTheme: const IconThemeData(color: CL_WHITE),
    sliderTheme: SliderThemeData(
      activeTrackColor: CL_ACCENT_DARK,
      trackHeight: 3.0,
      tickMarkShape: const RoundSliderTickMarkShape(tickMarkRadius: 4),
      activeTickMarkColor: CL_ACCENT_DARK,
      inactiveTickMarkColor: CL_ACCENT_DARK.withOpacity(0.99),
      thumbColor: CL_ACCENT_DARK,
      thumbShape: const RoundSliderThumbShape(enabledThumbRadius: 8.0),
      overlayShape: const RoundSliderOverlayShape(overlayRadius: 14.0),
    ),
    colorScheme: ColorScheme.fromSwatch().copyWith(secondary: CL_WHITE),
  );

  final _lightTheme = ThemeData.light().copyWith(
    primaryColor: PRIMARY_COLOR,
    //
    appBarTheme: const AppBarTheme(
      color: CL_WHITE,
      iconTheme: IconThemeData(color: tabBarTextLabelColor),
      systemOverlayStyle: SystemUiOverlayStyle.light,
    ),
    //
    tabBarTheme: const TabBarTheme(
      labelColor: PRIMARY_COLOR,
      unselectedLabelColor: tabBarTextLabelColor,
    ),
    //
    bottomAppBarTheme: const BottomAppBarTheme(color: CL_WHITE, elevation: 10),
    brightness: Brightness.light,
    indicatorColor: PRIMARY_COLOR,
    // iconTheme: IconThemeData(color: Colors.black87),
    sliderTheme: SliderThemeData(
      activeTrackColor: CL_ACCENT_LIGHT,
      trackHeight: 3.0,
      tickMarkShape: const RoundSliderTickMarkShape(tickMarkRadius: 4),
      activeTickMarkColor: CL_ACCENT_LIGHT,
      inactiveTickMarkColor: CL_ACCENT_LIGHT.withOpacity(0.99),
      thumbColor: CL_ACCENT_LIGHT,
      thumbShape: const RoundSliderThumbShape(enabledThumbRadius: 8.0),
      overlayShape: const RoundSliderOverlayShape(overlayRadius: 18.0),
    ),
    colorScheme: ColorScheme.fromSwatch().copyWith(secondary: PRIMARY_COLOR),
  );

  ThemeData get currentTheme {
    final themeCurrent = AppCache().themeSetting;

    switch (themeCurrent) {
      case DART_THEME:
        return _darkTheme;
      default:
        return _lightTheme;
    }
  }
}
