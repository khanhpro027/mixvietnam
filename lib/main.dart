import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mixvietnam/app_bloc_delegate.dart';
import 'package:mixvietnam/base/app_config.dart';
import 'package:mixvietnam/base/app_theme.dart';
import 'package:mixvietnam/constants/strings.dart';
import 'package:mixvietnam/pages/splash_screen/splash_screen.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]).then(
    (_) {
      // Add new Bloc delegate for debugging bloc behaviors
      Bloc.observer = AppBlocDelegate();

      // Init the app settings and run the main app
      AppConfig.init().then((_) {
        // Disable resampling touch events
        GestureBinding.instance?.resamplingEnabled = false;

        // Start the app
        runApp(MyApp());
      });
    },
  );
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // Get theme
    final ThemeData _themeData = AppTheme().currentTheme;

    // Init text style for all screen
    AppConfig.style.init(context);

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: strAppName,
      theme: _themeData,
      home: SplashScreen(),
    );
  }
}
