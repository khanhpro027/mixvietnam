class AuthValidators {
  String storeEmail = null;
  String storePass = null;
  String errorName = null;
  String errorEmailLogin = null;
  String errorPassLogin = null;
  String errorEmailRegister = null;
  String errorPass = null;
  String errorRepeatPass = null;

  bool isValidRegister(
    String name,
    String email,
    String pass,
    String repeatPass,
  ) {
    // Valid name
    if (name == null || name.isEmpty) {
      errorName = 'Vui lòng nhập tên ( nickname )';
      return false;
    }
    errorName = null;

    // Valid email
    if (email == null || email.isEmpty) {
      errorEmailRegister = 'Vui lòng nhập email';
      return false;
    }

    if (!isValidEmail(email)) {
      errorEmailRegister = 'Email không hợp lệ';
      return false;
    }
    errorEmailRegister = null;

    // Valid pass
    if (pass.isEmpty || pass == null) {
      errorPass = 'Vui lòng nhập mật khẩu';
      return false;
    }
    if (!isValidPass(pass)) {
      errorPass = 'Mật khẩu phải trên 5 ký tự';
      return false;
    }
    errorPass = null;

    // Valid repeat pass
    if (repeatPass == null || repeatPass.isEmpty) {
      errorRepeatPass = 'Vui lòng nhập mật khẩu';
      return false;
    }
    if (!isValidPass(repeatPass)) {
      errorRepeatPass = 'Mật khẩu phải trên 5 ký tự';
      return false;
    }
    if (repeatPass.toString() != pass.toString()) {
      errorRepeatPass = 'Mật khẩu nhập lại không khớp ';
      return false;
    }
    errorRepeatPass = null;

    return true;
  }

  bool isValidLogin(String email, String pass) {
    // Save for email + pass
    storeEmail = email;
    storePass = pass;

    // Valid email
    if (!isValidEmail(email)) {
      errorEmailLogin = 'Email không hợp lệ';
      return false;
    }
    errorEmailLogin = null;

    // Valid pass
    if (!isValidPass(pass)) {
      errorPassLogin = 'Mật khẩu phải trên 5 ký tự';
      return false;
    }

    errorPassLogin = null;

    return true;
  }

  static bool isValidEmail(String email) {
    return email.contains('@');
  }

  static bool isValidPass(String pass) {
    return pass != null && pass.length >= 5;
  }
}
