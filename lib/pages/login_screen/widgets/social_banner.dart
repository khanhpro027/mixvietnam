import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:mixvietnam/base/app_config.dart';
import 'package:mixvietnam/constants/colors.dart';
import 'package:mixvietnam/constants/image.dart';

class SocialBanner extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 16, right: 28),
      child: Container(
        child: Stack(
          children: [
            SvgPicture.asset(iconHome),
            Padding(
              padding: const EdgeInsets.only(top: 150),
              child: Text(
                'Đăng nhập',
                style: AppConfig.style.contentText1.copyWith(
                    fontSize: 20, fontWeight: FontWeight.w500, color: CL_BOLD),
              ),
            ),
            Align(
              child: SvgPicture.asset(girl),
              alignment: Alignment.topRight,
            ),
          ],
        ),
      ),
    );
  }
}
