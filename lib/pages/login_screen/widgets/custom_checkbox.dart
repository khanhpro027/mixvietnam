import 'package:flutter/material.dart';
import 'package:mixvietnam/base/app_config.dart';
import 'package:mixvietnam/constants/colors.dart';
import 'package:mixvietnam/constants/strings.dart';

class CustomCheckbox extends StatefulWidget {
  const CustomCheckbox({this.value = false, this.onValueChange});
  final bool value;
  final ValueChanged<bool> onValueChange;

  @override
  State<CustomCheckbox> createState() => _CustomCheckboxState();
}

class _CustomCheckboxState extends State<CustomCheckbox> {
  bool _valueOfCheckbox;

  @override
  void initState() {
    _valueOfCheckbox = widget.value;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: _onTap,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          SizedBox(
            height: 24.0,
            width: 24.0,
            child: IgnorePointer(
              child: Checkbox(
                value: _valueOfCheckbox,
                onChanged: (_) {},
              ),
            ),
          ),
          const SizedBox(width: 10.0),
          Text(
            STR_REMEMBER,
            style: AppConfig.style.contentText1.copyWith(
              fontWeight: FontWeight.w400,
              fontSize: 14,
              color: textColor3,
            ),
          )
        ],
      ),
    );
  }

  void _onTap() {
    setState(() {
      _valueOfCheckbox = !_valueOfCheckbox;
    });

    // Call back value
    widget.onValueChange.call(_valueOfCheckbox);
  }
}
