import 'dart:io';

import 'package:flutter/material.dart';
import 'package:mixvietnam/base/app_config.dart';
import 'package:mixvietnam/constants/colors.dart';
import 'package:mixvietnam/constants/image.dart';
import 'package:mixvietnam/constants/strings.dart';
import 'package:mixvietnam/pages/login_screen/login_with_account.dart';
import 'package:mixvietnam/pages/login_screen/widgets/social_button.dart';

class SocialArea extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Card(
      child: Padding(
        padding: const EdgeInsets.only(top: 10, left: 12, right: 12),
        child: Column(
          children: [
            // Button login with Google
            SocialButton(
              icon: google,
              title: 'ĐĂNG NHẬP BẰNG GOOGLE',
              style: AppConfig.style.contentText1.copyWith(
                  color: CL_BOLD, fontSize: 14, fontWeight: FontWeight.w500),
              borderColor: textColor3,
            ),

            // Button login with Facebook
            Padding(
              padding: const EdgeInsets.only(top: 16, bottom: 16),
              child: SocialButton(
                icon: facebook,
                title: 'ĐĂNG NHẬP BẰNG FACEBOOK',
                style: AppConfig.style.contentText1.copyWith(
                    color: Colors.white,
                    fontSize: 14,
                    fontWeight: FontWeight.w500),
                borderColor: colorFacebook,
                backgroundColor: colorFacebook,
              ),
            ),

            // Button login with Apple
            if (Platform.isIOS)
              SocialButton(
                icon: apple,
                title: 'ĐĂNG NHẬP BẰNG APPLE',
                style: AppConfig.style.contentText1.copyWith(
                  color: Colors.white,
                  fontSize: 14,
                  fontWeight: FontWeight.w500,
                ),
                borderColor: CL_BOLD,
                backgroundColor: CL_BOLD,
              ),
            Padding(
              padding: const EdgeInsets.only(top: 10, bottom: 10),
              child: Text(or,
                  style: AppConfig.style.contentText1.copyWith(
                      fontSize: 12,
                      fontWeight: FontWeight.w500,
                      color: textColor3.withOpacity(0.7))),
            ),

            // Button login with account
            SocialButton(
              title: loginWithAccount.toUpperCase(),
              style: AppConfig.style.contentText1.copyWith(
                color: PRIMARY_BUTTON_COLOR,
                fontSize: 14,
                fontWeight: FontWeight.w500,
              ),
              borderColor: PRIMARY_BUTTON_COLOR,
              onTap: () {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) => LoginWithAccount(),
                    settings: const RouteSettings(
                        name:
                            '/login_screen/login_with_social/login_with_account'),
                  ),
                );
              },
            ),

            // Bottom
            Padding(
              padding: const EdgeInsets.only(top: 10),
              child: RichText(
                text: TextSpan(
                    text: note,
                    style: AppConfig.style.contentText1
                        .copyWith(fontSize: 12, fontWeight: FontWeight.w300),
                    children: [
                      TextSpan(
                          text: privacy,
                          style: AppConfig.style.contentText1.copyWith(
                              fontSize: 12,
                              color: CL_BOLD,
                              fontWeight: FontWeight.w500)),
                    ]),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 10),
              child: Text(
                term,
                style: AppConfig.style.contentText1.copyWith(
                  fontSize: 12,
                  color: CL_BOLD,
                  fontWeight: FontWeight.w500,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
