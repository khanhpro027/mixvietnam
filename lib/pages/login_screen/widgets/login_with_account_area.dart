import 'package:flutter/material.dart';
import 'package:mixvietnam/base/app_cache.dart';
import 'package:mixvietnam/base/app_config.dart';
import 'package:mixvietnam/constants/colors.dart';
import 'package:mixvietnam/constants/strings.dart';
import 'package:mixvietnam/pages/login_screen/forget_password.dart';
import 'package:mixvietnam/pages/login_screen/widgets/custom_checkbox.dart';
import 'package:mixvietnam/util/auth_validators.dart';
import 'package:mixvietnam/util/debounce.dart';
import 'package:mixvietnam/widgets/text_field_widget.dart';
import 'package:mixvietnam/widgets/simple_button_widget.dart';

class LoginWithAccountArea extends StatefulWidget {
  LoginWithAccountArea({
    this.values = false,
    this.emailController,
    this.passController,
    this.onClickLogin,
  });

  final bool values;
  final TextEditingController emailController;
  final TextEditingController passController;
  final VoidCallback onClickLogin;

  @override
  State<LoginWithAccountArea> createState() => _LoginWithAccountAreaState();
}

class _LoginWithAccountAreaState extends State<LoginWithAccountArea> {
  final _formKey = GlobalKey<FormState>();
  final AuthValidators _authValidators = AuthValidators();
  final _debounce = Debounce(milliseconds: 500);

  bool _rememberLogin = AppCache().rememberLogin;
  bool isEnableEmail = false;
  bool isEnablePassWord = false;
  bool _isShowButton = false;

  @override
  void initState() {
    widget.emailController.text = _rememberLogin ? AppCache().userName : '';
    widget.passController.text = _rememberLogin ? AppCache().userPass : '';
    isEnableEmail = widget.emailController.text.isNotEmpty;
    isEnablePassWord = widget.passController.text.isNotEmpty;
    _isShowButton = isEnableEmail && isEnablePassWord;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final Size screenSize = MediaQuery.of(context).size;

    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15.0),
      ),
      child: Padding(
        padding: const EdgeInsets.only(left: 12, right: 12),
        child: Form(
          key: _formKey,
          onChanged: () => _debounce.run(() => _onFormChanged()),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              // Input email
              Padding(
                padding: const EdgeInsets.only(top: 10, bottom: 16),
                child: TextFieldWidget(
                  isEnable: isEnableEmail,
                  controller: widget.emailController,
                  title: labelEmail,
                  errorText: _authValidators.errorEmailLogin,
                ),
              ),

              // Input pass
              Padding(
                padding: const EdgeInsets.only(bottom: 12),
                child: TextFieldWidget(
                  isEnable: isEnablePassWord,
                  isPassword: true,
                  controller: widget.passController,
                  title: labelPassword,
                  errorText: _authValidators.errorPassLogin,
                ),
              ),

              // Remember user
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  // Remember login checkbox
                  CustomCheckbox(
                    value: _rememberLogin,
                    onValueChange: _onChangeValueCheckbox,
                  ),

                  // Fotget pass
                  InkWell(
                    child: Text(
                      forgetPass,
                      style: AppConfig.style.contentText1.copyWith(
                          color: PRIMARY_BUTTON_COLOR,
                          fontSize: 14,
                          fontWeight: FontWeight.w500),
                    ),
                    onTap: () {
                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (context) => ForgetPassword(),
                          settings: const RouteSettings(
                              name:
                                  '/login_screen/login_with_social/login_with_account/forget_password'),
                        ),
                      );
                    },
                  ),
                ],
              ),

              // Button call login
              SimpleButtonWidget(
                isDisable: !_isShowButton,
                text: login,
                margin: const EdgeInsets.only(top: 20, bottom: 10),
                width: screenSize.width,
                background: PRIMARY_BUTTON_COLOR,
                radiusBorder: 10,
                onPressed: _onClickLogin,
              ),

              // Accept policy
              RichText(
                text: TextSpan(
                    text: note,
                    style: AppConfig.style.contentText1
                        .copyWith(fontSize: 12, fontWeight: FontWeight.w300),
                    children: [
                      TextSpan(
                        text: privacy,
                        style: AppConfig.style.contentText1.copyWith(
                            fontSize: 12,
                            color: CL_BOLD,
                            fontWeight: FontWeight.w500),
                      ),
                    ]),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 12),
                child: Text(
                  term,
                  style: AppConfig.style.contentText1.copyWith(
                      fontSize: 12,
                      color: CL_BOLD,
                      fontWeight: FontWeight.w500),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _onFormChanged() {
    if (widget.emailController.text.isEmpty) {
      isEnableEmail = false;
    } else {
      isEnableEmail = true;
    }
    if (widget.passController.text.isEmpty) {
      isEnablePassWord = false;
    } else {
      isEnablePassWord = true;
    }

    // Update once change
    setState(() {
      _isShowButton = isEnableEmail && isEnablePassWord;
    });
  }

  void _onClickLogin() {
    final isValidatorLogin = _authValidators.isValidLogin(
      widget.emailController.text,
      widget.passController.text,
    );

    // If validator is true => call back
    if (isValidatorLogin) {
      widget.onClickLogin.call();
    } else {
      setState(() {});
    }
  }

  void _onChangeValueCheckbox(bool value) {
    _rememberLogin = value;
    AppCache().rememberLogin = value;
  }
}
