import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class SocialButton extends StatelessWidget {
  SocialButton(
      {this.icon = '',
      this.title = '',
      this.onTap,
      this.style,
      this.backgroundColor,
      this.borderColor});

  final String icon;
  final String title;
  final Color backgroundColor;
  final Color borderColor;
  final TextStyle style;
  final Function onTap;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap ?? () {},
      child: Container(
        height: 48,
        decoration: BoxDecoration(
          color: backgroundColor ?? Colors.white,
          border: Border.all(
            width: 1,
            color: borderColor ?? Colors.white,
          ),
          borderRadius: BorderRadius.circular(8),
        ),
        child: Stack(
          children: [
            if (icon.isNotEmpty)
              Padding(
                padding: const EdgeInsets.only(left: 18, top: 15, bottom: 15),
                child: SvgPicture.asset(icon),
              ),
            Center(
              child: Text(
                title,
                style: style ?? const TextStyle(),
              ),
            )
          ],
        ),
      ),
    );
  }
}
