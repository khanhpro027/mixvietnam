import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:mixvietnam/base/app_cache.dart';
import 'package:mixvietnam/base/app_config.dart';
import 'package:mixvietnam/base/app_helpers.dart';
import 'package:mixvietnam/constants/colors.dart';
import 'package:mixvietnam/constants/image.dart';
import 'package:mixvietnam/constants/strings.dart';
import 'package:mixvietnam/models/personal.dart';
import 'package:mixvietnam/pages/home_screen/home_screen.dart';
import 'package:mixvietnam/pages/login_screen/widgets/login_with_account_area.dart';
import 'package:mixvietnam/pages/welcom_screen/welcome_screen.dart';
import 'package:mixvietnam/repository/auth_repo.dart';
import 'package:mixvietnam/widgets/come_back_group.dart';
import 'package:mixvietnam/constants/globals.dart' as globals;

class LoginWithAccount extends StatefulWidget {
  @override
  State<LoginWithAccount> createState() => _LoginWithAccountState();
}

class _LoginWithAccountState extends State<LoginWithAccount> {
  /*
   * Login account
   * Email :  thuhuynh1997@gmail.com
   * PassWord: Thuhuynh123
   */

  final TextEditingController emailController = TextEditingController();
  final TextEditingController passController = TextEditingController();
  FocusScopeNode focusNode = FocusScopeNode();

  @override
  Widget build(BuildContext context) {
    focusNode = FocusScope.of(context);
    printDebug('build : LoginWithAccount');

    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Align(
            child: Padding(
              padding: const EdgeInsets.only(top: 50, left: 16),
              child: SvgPicture.asset(iconHome),
            ),
            alignment: Alignment.topLeft,
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 15),
                child: Text(
                  loginWithAccount,
                  style: AppConfig.style.contentText1.copyWith(
                    fontWeight: FontWeight.w500,
                    fontSize: 20,
                    color: CL_BOLD,
                  ),
                ),
              ),

              // Form login
              LoginWithAccountArea(
                emailController: emailController,
                passController: passController,
                onClickLogin: () => _onCallLogin(),
              ),
            ],
          ),

          // Back + create account
          ComeBackGroup(),
        ],
      ),
    );
  }

  Future<void> _onCallLogin() async {
    // Hide key board
    AppHelpers.hideKeyboard(focusNode);

    // Turn on loading
    AppHelpers.onLoading(context, flag: FLAG_ON);

    // Call login
    await AuthRepo.login(
        user: emailController.text,
        pass: passController.text,
        onLoginError: () => AppHelpers.onLoading(context, flag: FLAG_OFF),
        onLoginSuccess: (Personal personal) {
          // Set gobal User
          globals.user = personal;

          // Set flag has login = true
          AppCache().logged = FLAG_ON;

          // Save email + pass . If flag remember login = true
          final _rememberLogin = AppCache().rememberLogin;
          AppCache().userName = _rememberLogin ? emailController.text : '';
          AppCache().userPass = _rememberLogin ? passController.text : '';

          // Check is show introduction screen
          if (AppCache().isShowWelcomeScreen) {
            // Move to WelcomeScreen
            Navigator.of(context).pushReplacement(
              MaterialPageRoute(
                builder: (context) => WelcomeScreen(),
                settings: const RouteSettings(name: '/welcom_screen'),
              ),
            );
          } else {
            // Move to Home
            Navigator.of(context).pushReplacement(
              MaterialPageRoute(
                builder: (context) => HomeScreen(),
                settings: const RouteSettings(name: '/home_screen'),
              ),
            );
          }
        });
  }
}
