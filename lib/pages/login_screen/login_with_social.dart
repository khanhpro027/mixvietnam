import 'package:flutter/material.dart';
import 'package:mixvietnam/base/app_helpers.dart';
import 'package:mixvietnam/pages/login_screen/widgets/social_area.dart';
import 'package:mixvietnam/pages/login_screen/widgets/social_banner.dart';
import 'package:mixvietnam/widgets/come_back_group.dart';

class LoginWithSocial extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    printDebug('build : LoginWithSocial');

    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          SafeArea(
            child: Stack(
              children: [
                // Group login with social
                Padding(
                  padding: const EdgeInsets.only(top: 200, left: 16, right: 16),
                  child: SocialArea(),
                ),

                // Social banner
                Padding(
                  padding: const EdgeInsets.only(top: 20),
                  child: SocialBanner(),
                ),
              ],
            ),
          ),

          // Back
          ComeBackGroup(),
        ],
      ),
    );
  }
}
