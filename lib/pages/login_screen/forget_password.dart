import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:mixvietnam/base/app_config.dart';
import 'package:mixvietnam/base/app_helpers.dart';
import 'package:mixvietnam/constants/colors.dart';
import 'package:mixvietnam/constants/image.dart';
import 'package:mixvietnam/constants/strings.dart';
import 'package:mixvietnam/widgets/come_back_group.dart';
import 'package:mixvietnam/widgets/input_area.dart';

class ForgetPassword extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    printDebug('build : ForgetPassword');

    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Align(
            child: Padding(
              padding: const EdgeInsets.only(top: 50, left: 16),
              child: SvgPicture.asset(iconHome),
            ),
            alignment: Alignment.topLeft,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 16, right: 16),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(bottom: 3),
                  child: Text(
                    forgetPassScreen,
                    style: AppConfig.style.contentText1.copyWith(
                        fontWeight: FontWeight.w500,
                        fontSize: 20,
                        color: CL_BOLD),
                  ),
                ),
                Text(
                  forgetPassMiniText,
                  style: AppConfig.style.contentText1.copyWith(
                      fontWeight: FontWeight.w500,
                      fontSize: 12,
                      color: textColor.withOpacity(0.6)),
                ),
                const SizedBox(
                  height: 10,
                ),
                const InputArea(
                  title1: labelEmailOnly,
                  titleButton: resetPassButton,
                  isShowPrivay: false,
                ),
              ],
            ),
          ),

          // Bottom : back
          ComeBackGroup(),
        ],
      ),
    );
  }
}
