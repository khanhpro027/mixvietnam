import 'package:flutter/material.dart';
import 'package:mixvietnam/base/app_config.dart';
import 'package:mixvietnam/constants/colors.dart';
import 'package:mixvietnam/constants/strings.dart';
import 'package:mixvietnam/fake_data/fake_notification.dart';
import 'package:mixvietnam/pages/notification_screen/widget/item_notification.dart';
import 'package:mixvietnam/widgets/primary_appbar.dart';

class NotificationScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: backgroundColor,
      appBar: PreferredSize(
        preferredSize: Size(size.width, 50),
        child: const PrimaryAppBar(
          title: notiText,
          isBottom: false,
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 16, left: 16, bottom: 12),
              child: Text(
                newestNoti,
                style: AppConfig.style.contentText1.copyWith(
                    fontWeight: FontWeight.w500, fontSize: 20, color: CL_BOLD),
              ),
            ),
            Column(
              children: FakeNoti.newestNoti
                  .map((data) => ItemNotification(
                        noti: data,
                      ))
                  .toList(),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 16, left: 16, bottom: 12),
              child: Text(
                oldNoti,
                style: AppConfig.style.contentText1.copyWith(
                    fontWeight: FontWeight.w500, fontSize: 20, color: CL_BOLD),
              ),
            ),
            Column(
              children: FakeNoti.newestNoti
                  .map((data) => ItemNotification(
                        noti: data,
                      ))
                  .toList(),
            ),
          ],
        ),
      ),
    );
  }
}
