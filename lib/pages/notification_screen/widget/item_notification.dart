import 'package:flutter/material.dart';
import 'package:mixvietnam/base/app_config.dart';
import 'package:mixvietnam/constants/colors.dart';
import 'package:mixvietnam/constants/image.dart';
import 'package:mixvietnam/models/notification.dart';

class ItemNotification extends StatelessWidget {
  ItemNotification({this.noti});
  final NotificationModel noti;
  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Container(
      color: (noti.isRead) ? Colors.white : buttonPostColor,
      child: Row(
        children: [
          Container(
            margin:
                const EdgeInsets.only(left: 16, top: 8, bottom: 8, right: 8),
            height: 51,
            width: 51,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(26),
                image: DecorationImage(
                    image: AssetImage(noti.avatar), fit: BoxFit.cover)),
          ),
          Container(
            width: size.width - 109,
            child: RichText(
                text: TextSpan(
                    text: noti.title,
                    style: AppConfig.style.contentText1.copyWith(
                        fontWeight: FontWeight.w500,
                        fontSize: 14,
                        color: CL_BOLD),
                    children: [
                  TextSpan(
                      text: noti.content,
                      style: AppConfig.style.contentText1.copyWith(
                          fontWeight: FontWeight.w500,
                          fontSize: 14,
                          color: textColor3)),
                ])),
          ),
          Padding(
            padding: const EdgeInsets.only(right: 16),
            child: Image.asset(iconThreeDot),
          ),
        ],
      ),
    );
  }
}
