import 'package:flutter/material.dart';
import 'package:mixvietnam/base/app_config.dart';
import 'package:mixvietnam/constants/colors.dart';
import 'package:mixvietnam/models/personal.dart';

class PersonalBio extends StatelessWidget {
  PersonalBio({this.personal});
  final Personal personal;
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.only(left: 16, top: 16, bottom: 10),
          child: Text(
            'Mô tả',
            style: AppConfig.style.contentText1.copyWith(
                fontSize: 20, fontWeight: FontWeight.w500, color: CL_BOLD),
          ),
        ),
        Text(
          personal.bio,
          style: AppConfig.style.contentText1.copyWith(
              fontWeight: FontWeight.w400, fontSize: 14, color: CL_BOLD),
        ),
      ],
    );
  }
}
