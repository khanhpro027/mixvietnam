import 'package:flutter/material.dart';
import 'package:mixvietnam/base/app_config.dart';
import 'package:mixvietnam/constants/colors.dart';
import 'package:mixvietnam/constants/image.dart';

import 'package:mixvietnam/models/personal.dart';
import 'package:mixvietnam/pages/personal_page/personal_bio.dart';
import 'package:mixvietnam/pages/personal_page/personal_music.dart';
import 'package:mixvietnam/pages/personal_page/personal_post_all.dart';
import 'package:mixvietnam/pages/personal_page/widget/personal_appbar.dart';

class PersonalPage extends StatefulWidget {
  PersonalPage({this.personal});
  final Personal personal;

  @override
  State<PersonalPage> createState() => _PersonalPageState();
}

class _PersonalPageState extends State<PersonalPage>
    with SingleTickerProviderStateMixin {
  TabController _tabController;

  @override
  void initState() {
    _tabController = TabController(
      initialIndex: 0,
      length: 5,
      vsync: this,
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        centerTitle: true,
        leading: Image.asset(primaryLogo),
        title: Text(
          widget.personal.name,
          style: AppConfig.style.contentText1.copyWith(
              color: Colors.grey[700],
              fontWeight: FontWeight.w500,
              fontSize: 14),
        ),
        actions: [
          Image.asset(edit_home),
          Image.asset(search_home),
        ],
      ),
      body: NestedScrollView(
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
          return [
            SliverAppBar(
              pinned: true,
              titleSpacing: 0,
              floating: true,
              toolbarHeight: size.height * 0.55,
              automaticallyImplyLeading: false,
              title: PersonalAppbar(
                personal: widget.personal,
              ),
              bottom: TabBar(
                controller: _tabController,
                tabs: tabs.map((data) => Tab(text: data)).toList(),
                indicatorColor: PRIMARY_COLOR,
                isScrollable: true,
                labelColor: PRIMARY_COLOR,
                unselectedLabelColor: textColor3,
              ),
            ),
          ];
        },
        body: TabBarView(controller: _tabController, children: [
          PersonalPostAll(),
          PersonalBio(
            personal: widget.personal,
          ),
          PersonalMusic(),
          Container(),
          Container(),
        ]),
      ),
    );
  }

  final tabs = [
    'Tất cả',
    'Giới thiệu',
    'Bài hát',
    'Playlist',
    'Album',
  ];
}
