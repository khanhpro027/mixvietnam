import 'package:flutter/material.dart';
import 'package:mixvietnam/base/app_config.dart';
import 'package:mixvietnam/constants/colors.dart';
import 'package:mixvietnam/constants/strings.dart';
import 'package:mixvietnam/fake_data/fake_post.dart';
import 'package:mixvietnam/pages/home_screen/widgets/item_post.dart';

class PersonalPostAll extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      physics: const BouncingScrollPhysics(),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 16, left: 16, bottom: 10),
            child: Text(
              pinPostUserText,
              style: AppConfig.style.contentText1.copyWith(
                fontWeight: FontWeight.w500,
                fontSize: 20,
                color: CL_BOLD,
              ),
            ),
          ),
          ItemPost(
            post: FakePost.post[0],
          ),
          ItemPost(
            post: FakePost.post[0],
          ),
          ItemPost(
            post: FakePost.post[0],
          ),
        ],
      ),
    );
  }
}
