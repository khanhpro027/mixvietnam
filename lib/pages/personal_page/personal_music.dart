import 'package:flutter/material.dart';
import 'package:mixvietnam/base/app_config.dart';
import 'package:mixvietnam/constants/colors.dart';
import 'package:mixvietnam/constants/strings.dart';
import 'package:mixvietnam/fake_data/fake_post.dart';
import 'package:mixvietnam/pages/personal_page/widget/personal_item_music.dart';

class PersonalMusic extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                musicPosted,
                style: AppConfig.style.contentText1.copyWith(
                    fontWeight: FontWeight.w500, fontSize: 20, color: CL_BOLD),
              ),
              Text(
                newest,
                style: AppConfig.style.contentText1.copyWith(
                    fontWeight: FontWeight.w400,
                    fontSize: 14,
                    color: PRIMARY_COLOR),
              ),
            ],
          ),
          Column(
            children: [
              PersonalItemMusic(
                music: FakePost.music[0],
              ),
              PersonalItemMusic(
                music: FakePost.music[0],
              ),
              PersonalItemMusic(
                music: FakePost.music[0],
              ),
              PersonalItemMusic(
                music: FakePost.music[0],
              ),
              PersonalItemMusic(
                music: FakePost.music[0],
              ),
            ],
          ),
        ],
      ),
    );
  }
}
