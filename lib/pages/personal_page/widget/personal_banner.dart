import 'package:flutter/material.dart';
import 'package:mixvietnam/models/personal.dart';

class PersonalBanner extends StatelessWidget {
  const PersonalBanner({this.personal});
  final Personal personal;
  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Stack(
      alignment: Alignment.bottomCenter,
      children: [
        Column(
          children: [
            Container(
              decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage(personal.cover), fit: BoxFit.cover),
              ),
              width: size.width,
              height: 142,
            ),
            Container(
              width: size.width,
              height: 33,
            ),
          ],
        ),
        Container(
          width: 90,
          height: 90,
          decoration: BoxDecoration(
              border: Border.all(color: Colors.white, width: 3),
              borderRadius: BorderRadius.circular(45),
              image: DecorationImage(
                  image: AssetImage(personal.avatar), fit: BoxFit.cover)),
        ),
      ],
    );
  }
}
