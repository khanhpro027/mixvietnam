import 'package:flutter/material.dart';
import 'package:mixvietnam/base/app_config.dart';
import 'package:mixvietnam/constants/colors.dart';
import 'package:mixvietnam/constants/image.dart';
import 'package:mixvietnam/models/music_post_model.dart';

class PersonalItemMusic extends StatelessWidget {
  PersonalItemMusic({this.music});
  final MusicPostModel music;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 16, right: 16, bottom: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            children: [
              Container(
                margin: const EdgeInsets.only(right: 8),
                width: 51,
                height: 51,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(4),
                  image: DecorationImage(
                      image: AssetImage(music.avatar), fit: BoxFit.fill),
                ),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    music.author,
                    style: AppConfig.style.contentText1.copyWith(
                        fontWeight: FontWeight.w400,
                        fontSize: 12,
                        color: textColor3),
                  ),
                  Text(
                    music.title,
                    style: AppConfig.style.contentText1.copyWith(
                        fontWeight: FontWeight.w500,
                        fontSize: 14,
                        color: CL_BOLD),
                  ),
                  Row(
                    children: [
                      const Icon(Icons.play_arrow_rounded),
                      Text(
                        music.countPlayList.toString() + ' lượt nghe',
                        style: AppConfig.style.contentText1.copyWith(
                            fontWeight: FontWeight.w400,
                            fontSize: 12,
                            color: textColor3),
                      ),
                    ],
                  ),
                ],
              ),
            ],
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Image.asset(love_track),
              Text(
                music.durarion,
                style: AppConfig.style.contentText1.copyWith(
                    fontSize: 12,
                    fontWeight: FontWeight.w400,
                    color: textColor3),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
