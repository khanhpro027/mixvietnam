import 'package:flutter/material.dart';
import 'package:mixvietnam/base/app_config.dart';
import 'package:mixvietnam/constants/colors.dart';
import 'package:mixvietnam/constants/image.dart';
import 'package:mixvietnam/constants/strings.dart';
import 'package:mixvietnam/models/personal.dart';
import 'package:mixvietnam/pages/personal_page/widget/personal_banner.dart';
import 'package:mixvietnam/widgets/simple_button_widget.dart';

class PersonalAppbar extends StatelessWidget {
  PersonalAppbar({this.personal});
  final Personal personal;

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;

    return Column(
      children: [
        PersonalBanner(
          personal: personal,
        ),
        Padding(
          padding: const EdgeInsets.only(top: 5, bottom: 2),
          child: Text(personal.name,
              style: AppConfig.style.contentText1.copyWith(
                  fontWeight: FontWeight.w500, fontSize: 24, color: CL_BOLD)),
        ),
        Text(personal.email,
            style: AppConfig.style.contentText1.copyWith(
                fontSize: 14, fontWeight: FontWeight.w500, color: textColor)),
        Padding(
          padding: const EdgeInsets.only(top: 10),
          child: Text(
            personal.bio,
            style: AppConfig.style.contentText1.copyWith(
                fontWeight: FontWeight.w400, fontSize: 14, color: CL_BOLD),
            textAlign: TextAlign.center,
          ),
        ),
        Padding(
          padding:
              const EdgeInsets.only(left: 16, right: 16, top: 10, bottom: 8),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              _folowAndPostColumn(title: postText, count: personal.postCount),
              _folowAndPostColumn(title: follower, count: personal.follower),
              _folowAndPostColumn(
                  title: userFollowingText, count: personal.following),
            ],
          ),
        ),
        Row(
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 16),
              child: SimpleButtonWidget(
                text: yourSignText,
                textColor: PRIMARY_COLOR,
                width: size.width - 72,
                height: 32,
                radiusBorder: 8,
                background: buttonPostColor,
              ),
            ),
            Container(
              margin: const EdgeInsets.only(left: 8, right: 16),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                border: Border.all(color: CL_DISABLE, width: 1),
              ),
              width: 32,
              height: 32,
              child: Image.asset(dotIcon),
            ),
          ],
        ),
      ],
    );
  }

  Widget _folowAndPostColumn({String title, String count}) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Text(
          title,
          style: AppConfig.style.contentText1.copyWith(
            fontSize: 14,
            fontWeight: FontWeight.w400,
            color: textColor3,
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 4),
          child: Text(
            count,
            style: AppConfig.style.contentText1.copyWith(
                fontWeight: FontWeight.w500, fontSize: 16, color: CL_BOLD),
          ),
        ),
      ],
    );
  }
}
