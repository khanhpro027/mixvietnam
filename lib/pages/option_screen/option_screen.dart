import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:mixvietnam/base/app_config.dart';
import 'package:mixvietnam/base/app_helpers.dart';
import 'package:mixvietnam/constants/colors.dart';
import 'package:mixvietnam/constants/image.dart';
import 'package:mixvietnam/constants/strings.dart';
import 'package:mixvietnam/pages/option_screen/widgets/option_auth_widget.dart';

class OptionScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    printDebug('build : OptionScreen');

    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 16, top: 50, bottom: 40),
              child: SvgPicture.asset(introHome),
            ),

            // Logo
            Center(
              child: SvgPicture.asset(logoHome, fit: BoxFit.contain),
            ),

            // Option ath
            Padding(
              padding: const EdgeInsets.only(right: 16, left: 16),
              child: OptionAuthWidget(),
            ),
          ],
        ),
      ),
      bottomNavigationBar: Container(
        height: 72,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(start,
                    style: AppConfig.style.bodyText1.copyWith(
                        fontSize: 14,
                        fontWeight: FontWeight.w500,
                        color: CL_BOLD)),
                SvgPicture.asset(next),
              ],
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 16, top: 25),
              child: Text(
                copyRight,
                style: AppConfig.style.bodyText1.copyWith(
                    fontWeight: FontWeight.w400,
                    color: textColor,
                    fontFamily: 'SF Pro Display',
                    fontSize: 12),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
