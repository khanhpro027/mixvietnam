import 'package:flutter/material.dart';
import 'package:mixvietnam/constants/colors.dart';
import 'package:mixvietnam/constants/strings.dart';
import 'package:mixvietnam/pages/login_screen/login_with_social.dart';
import 'package:mixvietnam/pages/register_screen/register_screen.dart';
import 'package:mixvietnam/widgets/simple_button_widget.dart';

class OptionAuthWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final Size screenSize = MediaQuery.of(context).size;

    return Card(
      color: Colors.white,
      child: Padding(
        padding:
            const EdgeInsets.only(top: 10, left: 12, right: 12, bottom: 10),
        child: Column(
          children: [
            // Button login
            SimpleButtonWidget(
              text: login,
              width: screenSize.width,
              background: PRIMARY_BUTTON_COLOR,
              radiusBorder: 10,
              onPressed: () => onMoveToLoginPage(context),
            ),
            const SizedBox(height: 15),

            // Button register
            SimpleButtonWidget(
              text: register,
              width: screenSize.width,
              background: Colors.white,
              radiusBorder: 10,
              border: Border.all(width: 1, color: PRIMARY_BUTTON_COLOR),
              textColor: PRIMARY_BUTTON_COLOR,
              onPressed: () => onMoveToRegisterPage(context),
            ),
          ],
        ),
      ),
    );
  }

  void onMoveToLoginPage(BuildContext context) {
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) => LoginWithSocial(),
        settings: const RouteSettings(name: '/login_screen/login_with_social'),
      ),
    );
  }

  void onMoveToRegisterPage(BuildContext context) {
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) => RegisterScreen(),
        settings: const RouteSettings(name: '/login_screen/register_screen'),
      ),
    );
  }
}
