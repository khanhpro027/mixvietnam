import 'package:flutter/material.dart';
import 'package:mixvietnam/base/app_config.dart';
import 'package:mixvietnam/constants/colors.dart';
import 'package:mixvietnam/constants/strings.dart';
import 'package:mixvietnam/pages/tab_user/widget/setting_appbar.dart';

class SettingReportBug extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: PreferredSize(
          child: SettingAppbar(
            title: reportBug,
            action: [
              Container(
                width: 54,
                margin: const EdgeInsets.only(top: 6, bottom: 6, right: 16),
                height: 32,
                alignment: Alignment.center,
                decoration: BoxDecoration(
                    color: buttonPostColor,
                    borderRadius: BorderRadius.circular(8)),
                child: Text(
                  'Gửi',
                  style: AppConfig.style.contentText1.copyWith(
                      color: PRIMARY_COLOR,
                      fontSize: 14,
                      fontWeight: FontWeight.w500),
                ),
              ),
            ],
          ),
          preferredSize: Size(size.width, 50)),
      body: Container(
        width: size.width,
        margin: const EdgeInsets.only(left: 16, right: 16),
        child: const TextField(
          maxLength: 100,
          maxLines: null,
          decoration: InputDecoration(
            contentPadding: EdgeInsets.symmetric(vertical: 224.0),
          ),
          textAlign: TextAlign.justify,
        ),
      ),
    );
  }
}
