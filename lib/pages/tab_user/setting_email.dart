import 'package:flutter/material.dart';
import 'package:mixvietnam/base/app_config.dart';
import 'package:mixvietnam/base/app_helpers.dart';
import 'package:mixvietnam/constants/colors.dart';
import 'package:mixvietnam/constants/strings.dart';
import 'package:mixvietnam/pages/tab_user/widget/setting_appbar.dart';
import 'package:mixvietnam/widgets/text_field_widget.dart';

class SettingEmail extends StatefulWidget {
  @override
  State<SettingEmail> createState() => _SettingEmailState();
}

class _SettingEmailState extends State<SettingEmail> {
  bool _onChange = false;
  final _formKey = GlobalKey<FormState>();
  bool isEnableEmail = false;
  bool isEnableName = false;

  final TextEditingController emailController = TextEditingController();
  final TextEditingController nameController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;

    return Scaffold(
        appBar: PreferredSize(
            child: SettingAppbar(
              title: emailAndUserNameText,
              action: [
                InkWell(
                  onTap: () {
                    printDebug('$_onChange');
                  },
                  child: Container(
                    width: 54,
                    margin: const EdgeInsets.only(top: 6, bottom: 6, right: 16),
                    height: 32,
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                        color: buttonPostColor,
                        borderRadius: BorderRadius.circular(8)),
                    child: Text(
                      'Lưu',
                      style: AppConfig.style.contentText1.copyWith(
                          color: PRIMARY_COLOR,
                          fontSize: 14,
                          fontWeight: FontWeight.w500),
                    ),
                  ),
                ),
              ],
            ),
            preferredSize: Size(size.width, 50)),
        body: Form(
          key: _formKey,
          onChanged: () {
            if (emailController.text.isEmpty) {
              isEnableEmail = false;
            } else {
              isEnableEmail = true;
            }
            if (nameController.text.isEmpty) {
              isEnableName = false;
            } else {
              isEnableName = true;
            }
            setState(() {
              _onChange = _formKey.currentState.validate();
            });
          },
          child: Column(
            children: [
              // TextField 1
              Padding(
                padding: const EdgeInsets.only(
                    top: 10, bottom: 16, left: 16, right: 16),
                child: TextFieldWidget(
                  title: labelEmail,
                  controller: emailController,
                  isEnable: isEnableEmail,
                ),
              ),

              // TextField 2
              Padding(
                padding: const EdgeInsets.only(bottom: 12, left: 16, right: 16),
                child: TextFieldWidget(
                  title: registerName,
                  controller: nameController,
                  isEnable: isEnableName,
                ),
              ),
            ],
          ),
        ));
  }
}
