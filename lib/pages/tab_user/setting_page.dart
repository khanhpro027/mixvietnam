import 'package:flutter/material.dart';
import 'package:mixvietnam/base/app_config.dart';
import 'package:mixvietnam/constants/colors.dart';
import 'package:mixvietnam/constants/strings.dart';

class SettingPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        leading: const InkWell(
          child: Icon(Icons.arrow_back_ios_new_sharp),
        ),
        title: Text(
          infomationText,
          style: AppConfig.style.contentText1.copyWith(
              fontSize: 16, fontWeight: FontWeight.w500, color: textColor3),
        ),
      ),
      body: Column(
        children: [
          _itemSetting(text: aboutYourSelf),
          _itemSetting(text: yourNameText),
          _itemSetting(text: emailAndUserNameText),
          _itemSetting(text: passWordText),
        ],
      ),
    );
  }

  Widget _itemSetting({String text, Function onTap}) {
    return InkWell(
      onTap: onTap ?? () {},
      child: Container(
        color: Colors.white,
        padding: const EdgeInsets.only(top: 8, bottom: 7, left: 16, right: 11),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              text,
              style: AppConfig.style.contentText1.copyWith(
                  fontWeight: FontWeight.w500, fontSize: 14, color: CL_BOLD),
            ),
            const Icon(Icons.navigate_next_outlined)
          ],
        ),
      ),
    );
  }
}
