import 'package:flutter/material.dart';
import 'package:mixvietnam/base/app_cache.dart';
import 'package:mixvietnam/base/app_config.dart';
import 'package:mixvietnam/constants/colors.dart';
import 'package:mixvietnam/constants/image.dart';
import 'package:mixvietnam/constants/strings.dart';
import 'package:mixvietnam/pages/login_screen/login_with_social.dart';
import 'package:mixvietnam/pages/tab_user/widget/setting_widget.dart';

class TabUser extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        leading: const InkWell(
          child: Icon(Icons.arrow_back_ios_new_sharp),
        ),
        title: Text(
          'Cài đặt',
          style: AppConfig.style.contentText1.copyWith(
              fontSize: 16, fontWeight: FontWeight.w500, color: textColor3),
        ),
        actions: [
          SizedBox(
            width: 50,
            height: 50,
            child: Container(
              height: 34,
              width: 34,
              child: Image.asset(
                fake_avatar4,
              ),
            ),
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            // group 1
            Padding(
              padding: const EdgeInsets.only(left: 16, top: 12, bottom: 10),
              child: Text(
                'Tài khoản',
                style: AppConfig.style.contentText1.copyWith(
                    fontSize: 20, fontWeight: FontWeight.w500, color: CL_BOLD),
              ),
            ),
            SettingWidget(
              pathIcon: iconLock,
              text: historyLogin,
            ),
            SettingWidget(
              pathIcon: iconInfomationUser,
              text: infomationText,
            ),

            // group 2
            Padding(
              padding: const EdgeInsets.only(left: 16, top: 12, bottom: 10),
              child: Text(
                'Trợ giúp',
                style: AppConfig.style.contentText1.copyWith(
                    fontSize: 20, fontWeight: FontWeight.w500, color: CL_BOLD),
              ),
            ),
            SettingWidget(
              pathIcon: iconReport,
              text: reportBug,
            ),

            // group 3
            Padding(
              padding: const EdgeInsets.only(left: 16, top: 12, bottom: 10),
              child: Text(
                'MixVietnam',
                style: AppConfig.style.contentText1.copyWith(
                    fontSize: 20, fontWeight: FontWeight.w500, color: CL_BOLD),
              ),
            ),
            SettingWidget(
              pathIcon: iconPrivacy,
              text: privacyUser,
            ),
            SettingWidget(
              pathIcon: iconService,
              text: serviceText,
            ),
            SettingWidget(
              pathIcon: iconCopyRight,
              text: copyRightText,
            ),
            SettingWidget(
              pathIcon: iconCopyRight,
              text: aboutUs,
            ),

            // Logout
            GestureDetector(
              child: Container(
                width: size.width,
                margin: const EdgeInsets.only(left: 16, right: 16, top: 12),
                padding: const EdgeInsets.only(top: 5, bottom: 5),
                decoration: BoxDecoration(
                  border: Border.all(color: CL_DISABLE),
                  borderRadius: BorderRadius.circular(8),
                ),
                child: Text(
                  strLogout.toUpperCase(),
                  style: AppConfig.style.contentText1.copyWith(
                      fontSize: 14,
                      fontWeight: FontWeight.w500,
                      color: CL_DISABLE),
                  textAlign: TextAlign.center,
                ),
              ),
              onTap: () => _onLogout(context),
            ),
          ],
        ),
      ),
    );
  }

  void _onLogout(BuildContext context) {
    // Update flag logged = false
    AppCache().logged = FLAG_OFF;

    // AppCache().isShowWelcomeScreen = FLAG_ON;

    // Remove all route
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(builder: (context) => LoginWithSocial()),
        (Route<dynamic> route) => false);

    //     Navigator.of(context)
    // .pushNamedAndRemoveUntil('/login_screen/login_with_social', (Route<dynamic> route) => false);
  }
}
