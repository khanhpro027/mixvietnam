import 'package:flutter/material.dart';
import 'package:mixvietnam/base/app_config.dart';
import 'package:mixvietnam/constants/colors.dart';
import 'package:mixvietnam/constants/strings.dart';
import 'package:mixvietnam/pages/tab_user/widget/setting_appbar.dart';
import 'package:mixvietnam/pages/tab_user/widget/timeline_item.dart';

class HistoryLogin extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: PreferredSize(
          child: const SettingAppbar(
            title: historyLogin,
          ),
          preferredSize: Size(size.width, 50)),
      body: SingleChildScrollView(
        physics: const BouncingScrollPhysics(),
        child: Padding(
          padding: const EdgeInsets.only(left: 16, right: 16),
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 16, bottom: 10),
                child: Text(
                  '22 tháng 10, 2020',
                  style: AppConfig.style.contentText1.copyWith(
                      color: CL_BOLD,
                      fontSize: 16,
                      fontWeight: FontWeight.w500),
                ),
              ),
              //timeline

              const TimeLineItem(
                time: '04:18 PM',
                content: 'vừa đăng một bài hát mới',
              ),
              const TimeLineItem(
                time: '04:18 PM',
                content: 'vừa tạo một danh sách phát mới',
                isFirst: false,
                isLast: false,
              ),
              const TimeLineItem(
                time: '04:18 PM',
                content: 'vừa bình luận bài viết của',
                tail: ' Joge Mckey',
                isFirst: false,
                isLast: true,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
