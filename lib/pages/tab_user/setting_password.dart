import 'package:flutter/material.dart';
import 'package:mixvietnam/base/app_config.dart';
import 'package:mixvietnam/base/app_helpers.dart';
import 'package:mixvietnam/constants/colors.dart';
import 'package:mixvietnam/constants/strings.dart';
import 'package:mixvietnam/pages/tab_user/widget/setting_appbar.dart';
import 'package:mixvietnam/widgets/text_field_widget.dart';

class SettingPassword extends StatefulWidget {
  @override
  State<SettingPassword> createState() => _SettingPasswordState();
}

class _SettingPasswordState extends State<SettingPassword> {
  bool _onChange = false;
  final _formKey = GlobalKey<FormState>();
  bool isEnableLPass = false;
  bool isEnableNPass = false;
  bool isEnableRPass = false;

  final TextEditingController lPassController = TextEditingController();
  final TextEditingController nPassController = TextEditingController();
  final TextEditingController rPassController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;

    return Scaffold(
        appBar: PreferredSize(
            child: SettingAppbar(
              title: 'Mật khẩu',
              action: [
                InkWell(
                  onTap: () {
                    printDebug('$_onChange');
                  },
                  child: Container(
                    width: 54,
                    margin: const EdgeInsets.only(top: 6, bottom: 6, right: 16),
                    height: 32,
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                        color: buttonPostColor,
                        borderRadius: BorderRadius.circular(8)),
                    child: Text(
                      'Lưu',
                      style: AppConfig.style.contentText1.copyWith(
                          color: PRIMARY_COLOR,
                          fontSize: 14,
                          fontWeight: FontWeight.w500),
                    ),
                  ),
                ),
              ],
            ),
            preferredSize: Size(size.width, 50)),
        body: Form(
          key: _formKey,
          onChanged: () {
            if (lPassController.text.isEmpty) {
              isEnableLPass = false;
            } else {
              isEnableLPass = true;
            }
            if (nPassController.text.isEmpty) {
              isEnableNPass = false;
            } else {
              isEnableNPass = true;
            }
            if (rPassController.text.isEmpty) {
              isEnableRPass = false;
            } else {
              isEnableRPass = true;
            }
            setState(() {
              _onChange = _formKey.currentState.validate();
            });
          },
          child: Column(
            children: [
              // TextField 1
              Padding(
                padding: const EdgeInsets.only(
                    top: 10, bottom: 16, left: 16, right: 16),
                child: TextFieldWidget(
                  isPassword: true,
                  title: lastPassword,
                  isEnable: isEnableLPass,
                  controller: lPassController,
                ),
              ),

              // TextField 2
              Padding(
                padding: const EdgeInsets.only(bottom: 12, left: 16, right: 16),
                child: TextFieldWidget(
                  title: newPassword,
                  isEnable: isEnableNPass,
                  isPassword: true,
                  controller: nPassController,
                ),
              ),

              // TextField 3
              Padding(
                padding: const EdgeInsets.only(bottom: 12, left: 16, right: 16),
                child: TextFieldWidget(
                  title: inputPassword,
                  isPassword: true,
                  isEnable: isEnableRPass,
                  controller: rPassController,
                ),
              ),
            ],
          ),
        ));
  }
}
