import 'package:flutter/material.dart';
import 'package:mixvietnam/base/app_config.dart';
import 'package:mixvietnam/constants/colors.dart';
import 'package:mixvietnam/pages/tab_user/widget/setting_appbar.dart';

class SettingUsername extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: PreferredSize(
          child: SettingAppbar(
            title: 'Tên hiển thị',
            action: [
              Container(
                width: 54,
                margin: const EdgeInsets.only(top: 6, bottom: 6, right: 16),
                height: 32,
                alignment: Alignment.center,
                decoration: BoxDecoration(
                    color: buttonPostColor,
                    borderRadius: BorderRadius.circular(8)),
                child: Text(
                  'Lưu',
                  style: AppConfig.style.contentText1.copyWith(
                      color: PRIMARY_COLOR,
                      fontSize: 14,
                      fontWeight: FontWeight.w500),
                ),
              ),
            ],
          ),
          preferredSize: Size(size.width, 50)),
      body: Container(
        width: size.width,
        margin: const EdgeInsets.only(left: 16, right: 16),
        child: const TextField(
          maxLength: 12,
          maxLines: 1,
          textAlign: TextAlign.justify,
        ),
      ),
    );
  }
}
