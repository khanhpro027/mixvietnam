import 'dart:io';

import 'package:flutter/material.dart';
import 'dart:ui' as ui;
import 'package:flutter/services.dart' show rootBundle;
import 'dart:async';
import 'dart:typed_data';
import 'dart:math' as math;
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';

class CustomImageCanvas extends StatefulWidget {
  CustomImageCanvas({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _CustomImageCanvasState createState() => _CustomImageCanvasState();
}

class _CustomImageCanvasState extends State<CustomImageCanvas> {
  ui.Image image;
  bool isImageloaded = false;
  @override
  void initState() {
    super.initState();
    init();
  }

  Future init() async {
    final ByteData data = await rootBundle.load('assets/image.jpeg');
    image = await loadImage(Uint8List.view(data.buffer));
  }

  Future<ui.Image> loadImage(List<int> img) async {
    final Completer<ui.Image> completer = Completer();
    ui.decodeImageFromList(img, (ui.Image img) {
      setState(() {
        isImageloaded = true;
      });
      return completer.complete(img);
    });
    return completer.future;
  }

  Widget _buildImage() {
    if (isImageloaded) {
      return CustomPaint(
        painter: PngImagePainter(image: image),
      );
    } else {
      return const Center(child: Text('loading'));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
        ),
        body: Container(
          child: _buildImage(),
        ));
  }
}

class PngImagePainter extends CustomPainter {
  PngImagePainter({
    this.image,
  });

  ui.Image image;

  @override
  void paint(Canvas canvas, Size size) {
    _drawCanvas(size, canvas);
    _saveCanvas(size);
  }

  Canvas _drawCanvas(Size size, Canvas canvas) {
    const center = Offset(150, 50);
    final radius = math.min(size.width, size.height) / 8;

    // The circle should be paint before or it will be hidden by the path
    final Paint paintCircle = Paint()..color = Colors.black;
    final Paint paintBorder = Paint()
      ..color = Colors.white
      ..strokeWidth = size.width / 36
      ..style = PaintingStyle.stroke;
    canvas.drawCircle(center, radius, paintCircle);
    canvas.drawCircle(center, radius, paintBorder);

    const drawImageWidth = 0.0;
    final drawImageHeight = -size.height * 0.8;

    final Path path = Path()
      ..addOval(Rect.fromLTWH(drawImageWidth, drawImageHeight,
          image.width.toDouble(), image.height.toDouble()));

    canvas.clipPath(path);

    canvas.drawImage(image, Offset(drawImageWidth, drawImageHeight), Paint());
    return canvas;
  }

  void _saveCanvas(Size size) async {
    final pictureRecorder = ui.PictureRecorder();
    final canvas = Canvas(pictureRecorder);
    final paint = Paint();
    paint.isAntiAlias = true;

    _drawCanvas(size, canvas);

    final pic = pictureRecorder.endRecording();
    final ui.Image img = await pic.toImage(image.width, image.height);
    final byteData = await img.toByteData(format: ui.ImageByteFormat.png);
    final buffer = byteData.buffer.asUint8List();

    // var response = await get(imgUrl);
    final documentDirectory = await getApplicationDocumentsDirectory();
    final File file = File(join(documentDirectory.path,
        '${DateTime.now().toUtc().toIso8601String()}.png'));
    file.writeAsBytesSync(buffer);

    //print(file.path);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}
