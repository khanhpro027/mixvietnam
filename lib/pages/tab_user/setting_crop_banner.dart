import 'dart:io';
import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mixvietnam/base/app_config.dart';
import 'package:mixvietnam/base/app_helpers.dart';
import 'package:mixvietnam/constants/colors.dart';
import 'package:mixvietnam/constants/image.dart';
import 'package:mixvietnam/pages/tab_user/widget/setting_appbar.dart';
import 'package:path_provider/path_provider.dart';
import 'package:photo_view/photo_view.dart';
import 'package:image/image.dart' as img;

class SettingCropBanner extends StatefulWidget {
  @override
  State<SettingCropBanner> createState() => _SettingCropBannerState();
}

const double min = pi * -2;
const double max = pi * 2;

const double minScale = 0.9;
const double defScale = 0.1;
const double maxScale = 1.9;

class _SettingCropBannerState extends State<SettingCropBanner> {
  PhotoViewControllerBase controller;

  PhotoViewScaleStateController scaleStateController;
  int calls = 0;
  final GlobalKey _cropKey = GlobalKey();

  @override
  void initState() {
    controller = PhotoViewController(initialScale: defScale)
      ..outputStateStream.listen(onController);

    scaleStateController = PhotoViewScaleStateController()
      ..outputScaleStateStream.listen(onScaleState);
    super.initState();
  }

  void onController(PhotoViewControllerValue value) {
    setState(() {
      calls += 1;
    });
  }

  void onScaleState(PhotoViewScaleState scaleState) {
    printDebug(scaleState.name);
  }

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;

    return Scaffold(
      appBar: PreferredSize(
          child: SettingAppbar(
            title: 'Thay đổi ảnh đại diện',
            action: [
              InkWell(
                onTap: () => _cropNewImage(avatar_wibu),
                child: Container(
                  width: 54,
                  margin: const EdgeInsets.only(top: 6, bottom: 6, right: 16),
                  height: 32,
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                      color: buttonPostColor,
                      borderRadius: BorderRadius.circular(8)),
                  child: Text(
                    'Lưu',
                    style: AppConfig.style.contentText1.copyWith(
                        color: PRIMARY_COLOR,
                        fontSize: 14,
                        fontWeight: FontWeight.w500),
                  ),
                ),
              ),
            ],
          ),
          preferredSize: Size(size.width, 50)),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            height: size.height - 130,
            child: Stack(
              fit: StackFit.expand,
              children: [
                Container(
                  child: PhotoView(
                    key: _cropKey,
                    controller: controller,
                    scaleStateController: scaleStateController,
                    enableRotation: true,
                    initialScale: minScale,
                    minScale: minScale,
                    maxScale: size.height - 50,
                    imageProvider: const AssetImage(
                      avatar_wibu,
                    ),
                  ),
                ),
                ColorFiltered(
                  colorFilter: ColorFilter.mode(
                    Colors.black.withOpacity(0.5),
                    BlendMode.srcOut,
                  ), // This one will create the magic
                  child: Stack(
                    fit: StackFit.expand,
                    children: [
                      Container(
                        decoration: const BoxDecoration(
                          color: Colors.black,
                          backgroundBlendMode: BlendMode.dstOut,
                        ), // This one will handle background + difference out
                      ),
                      Align(
                        alignment: Alignment.center,
                        child: Container(
                          height: size.width - 16,
                          width: size.width - 16,
                          decoration: const BoxDecoration(
                            color: Colors.red,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            child: Container(
              color: Colors.white,
              width: size.width,
              alignment: Alignment.center,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  CupertinoButton(
                      child: const ImageIcon(AssetImage(iconDecrease)),
                      onPressed: () => _onTapDownScaleImage()),
                  StreamBuilder(
                    stream: controller.outputStateStream,
                    initialData: controller.value,
                    builder: _streamBuild,
                  ),
                  CupertinoButton(
                      child: const ImageIcon(AssetImage(iconIncrease)),
                      onPressed: () => _onTapUpScaleImage()),
                ],
              ),
            ),
          ),
          //ontainer()m
        ],
      ),
    );
  }

  void _onTapDownScaleImage() {
    // _controller.setInvisibly(PhotoViewScaleState.zoomedIn);
    if (controller.scale > 0.9) {
      if (controller.scale - 0.1 > 0.9) {
        controller.scale = controller.scale - 0.1;
        value.scale.clamp(minScale, maxScale);
      } else {
        controller.scale = 0.9;
      }
    }
  }

  void _onTapUpScaleImage() {
    // _controller.setInvisibly(PhotoViewScaleState.zoomedIn);
    if (controller.scale < 1.9) {
      if (controller.scale + 0.1 < 1.9) {
        controller.scale = controller.scale + 0.1;
        value.scale.clamp(minScale, maxScale);
      } else {
        controller.scale = 1.9;
      }
    }
  }

  PhotoViewControllerValue value;

  Widget _streamBuild(BuildContext context, AsyncSnapshot snapshot) {
    if (snapshot.hasError || !snapshot.hasData) {
      return Container();
    }
    value = snapshot.data;
    return SliderTheme(
      data: SliderTheme.of(context).copyWith(
        activeTrackColor: PRIMARY_COLOR,
        thumbColor: PRIMARY_COLOR,
      ),
      child: Slider(
        value: value.scale.clamp(minScale, maxScale),
        min: minScale,
        max: maxScale,
        onChanged: (double newScale) {
          controller.scale = newScale;
        },
      ),
    );
  }

  // random path avoid dupclip
  String _randomNonceString([int length = 32]) {
    final random = Random();

    final charCodes = List<int>.generate(length, (_) {
      int codeUnit;

      switch (random.nextInt(3)) {
        case 0:
          codeUnit = random.nextInt(10) + 48;
          break;
        case 1:
          codeUnit = random.nextInt(26) + 65;
          break;
        case 2:
          codeUnit = random.nextInt(26) + 97;
          break;
      }

      return codeUnit;
    });

    return String.fromCharCodes(charCodes);
  }

  //load asset file, only debug
  Future<File> getImageFileFromAssets(String patImage) async {
    final byteData = await rootBundle.load(patImage);

    final file =
        File('${(await getApplicationDocumentsDirectory()).path}/$patImage');
    printDebug(file.path);
    file.writeAsBytesSync(byteData.buffer
        .asUint8List(byteData.offsetInBytes, byteData.lengthInBytes));

    return file;
  }

  /*
   * creator: Hoang vjp pro
   * date: 26/07/2022
   */
  void _cropNewImage(String pathImage) async {
    // convert path image to bytes
    //final _file = File(pathImage);
    final _file = await getImageFileFromAssets(pathImage);
    final bytes = await _file.readAsBytes();
    final img.Image src = img.decodeImage(bytes);
    // crop image with Offset and width height r = (Ox, Oy, w, h)
    final img.Image destImage = img.copyCrop(
        src,
        controller.position.dx.toInt(),
        controller.position.dy.toInt(),
        controller.scale.toInt(),
        controller.scale.toInt());

    // save new image to directory
    final jpg = img.encodeJpg(destImage);
    final Directory dir = await getTemporaryDirectory();
    final String path = '${dir.path}/${_randomNonceString()}.png';
    final _capturedImage = await File(path).writeAsBytes(jpg);

    printDebug('path new image $_capturedImage');
  }
}
