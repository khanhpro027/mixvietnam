import 'package:flutter/material.dart';
import 'package:mixvietnam/base/app_config.dart';
import 'package:mixvietnam/constants/colors.dart';
import 'package:timeline_tile/timeline_tile.dart';

class TimeLineItem extends StatelessWidget {
  const TimeLineItem(
      {@required this.content,
      @required this.time,
      this.tail,
      this.isFirst = true,
      this.isLast = false});
  final String time;
  final String content;
  final bool isFirst;
  final bool isLast;
  final String tail;
  @override
  Widget build(BuildContext context) {
    return TimelineTile(
      axis: TimelineAxis.vertical,
      alignment: TimelineAlign.start,
      lineXY: 1,
      endChild: RichText(
          text: TextSpan(
              text: 'Bạn ',
              style: AppConfig.style.contentText1.copyWith(
                  fontWeight: FontWeight.w500, fontSize: 14, color: CL_BOLD),
              children: [
            TextSpan(
                text: content,
                style: AppConfig.style.contentText1.copyWith(
                    fontWeight: FontWeight.w400,
                    fontSize: 14,
                    color: textColor3)),
            if (tail != null)
              TextSpan(
                  text: tail,
                  style: AppConfig.style.contentText1.copyWith(
                      fontWeight: FontWeight.w500,
                      fontSize: 14,
                      color: CL_BOLD))
          ])),
      startChild: Container(
        child: Text(
          time,
          style: AppConfig.style.contentText1.copyWith(
              fontSize: 14, fontWeight: FontWeight.w400, color: textColor3),
        ),
      ),
    );
  }
}
