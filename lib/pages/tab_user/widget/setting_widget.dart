import 'package:flutter/material.dart';
import 'package:mixvietnam/base/app_config.dart';
import 'package:mixvietnam/constants/colors.dart';

class SettingWidget extends StatelessWidget {
  SettingWidget({this.pathIcon, this.text});

  final String pathIcon;
  final String text;

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Container(
      alignment: Alignment.center,
      width: size.width,
      height: 44,
      color: Colors.white,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 16, right: 8),
                child: Image.asset(pathIcon),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 0),
                child: Text(
                  text,
                  style: AppConfig.style.contentText1.copyWith(
                      fontWeight: FontWeight.w500,
                      fontSize: 14,
                      color: CL_BOLD),
                ),
              ),
            ],
          ),
          // SizedBox(
          //   height: 33,
          //   width: size.width * 0.8,
          //   child: ListTile(
          //
          //     leading:
          //     title:
          //   ),
          // ),
          const Padding(
            padding: EdgeInsets.only(right: 16),
            child: Icon(Icons.navigate_next_rounded),
          )
        ],
      ),
    );
  }
}
