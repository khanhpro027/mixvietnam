import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mixvietnam/base/app_config.dart';
import 'package:mixvietnam/constants/colors.dart';

class SettingAppbar extends StatelessWidget {
  const SettingAppbar({this.onTap, this.action, this.title});
  final Function onTap;
  final String title;
  final List<Widget> action;
  @override
  Widget build(BuildContext context) {
    return AppBar(
      centerTitle: true,
      leading: CupertinoButton(
          child: const Icon(
            Icons.arrow_back_ios_new_sharp,
            color: textColor3,
          ),
          onPressed: onTap ?? () => Navigator.pop(context)),
      title: Text(
        title ?? '',
        style: AppConfig.style.contentText1.copyWith(
            fontSize: 16, fontWeight: FontWeight.w500, color: textColor3),
      ),
      actions: action ?? [],
    );
  }
}
