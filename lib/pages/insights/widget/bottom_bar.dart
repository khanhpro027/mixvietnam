import 'package:flutter/material.dart';
import 'package:mixvietnam/base/app_config.dart';
import 'package:mixvietnam/constants/colors.dart';
import 'package:mixvietnam/constants/image.dart';

class BottomBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 45, right: 45),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          _iconTrack(icon: playIcon, count: '12'),
          _iconTrack(icon: darkLoveTrack, count: '12'),
          _iconTrack(icon: darkComment, count: '12'),
        ],
      ),
    );
  }

  Widget _iconTrack({String icon, String count}) {
    return Row(
      children: [
        Image.asset(
          icon,
          color: Colors.black,
        ),
        Text(
          count,
          style: AppConfig.style.contentText1.copyWith(
            fontWeight: FontWeight.w500,
            fontSize: 14,
            color: textColor3,
          ),
        ),
      ],
    );
  }
}
