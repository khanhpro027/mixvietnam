import 'package:flutter/material.dart';
import 'package:mixvietnam/base/app_config.dart';
import 'package:mixvietnam/constants/colors.dart';
import 'package:mixvietnam/models/music_post_model.dart';

class SpecialMusic extends StatelessWidget {
  const SpecialMusic({this.musicPost, this.number});
  final MusicPostModel musicPost;
  final int number;
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Row(
          children: [
            Text(
              number.toString(),
              style: AppConfig.style.contentText1.copyWith(
                fontWeight: FontWeight.w400,
                fontSize: 12,
                color: textColor3,
              ),
            ),
            Container(
              margin: const EdgeInsets.only(left: 11, right: 8),
              width: 32,
              height: 32,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(2),
                image: DecorationImage(
                    image: AssetImage(musicPost.avatar), fit: BoxFit.cover),
              ),
            ),
            Text(
              musicPost.title,
              style: AppConfig.style.contentText1.copyWith(
                  fontWeight: FontWeight.w500, fontSize: 12, color: CL_BOLD),
            ),
          ],
        ),
        Text(
          musicPost.listented.toString(),
          style: AppConfig.style.contentText1.copyWith(
              fontWeight: FontWeight.w500, fontSize: 12, color: CL_BOLD),
        ),
      ],
    );
  }
}
