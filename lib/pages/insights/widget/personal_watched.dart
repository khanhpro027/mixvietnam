import 'package:flutter/material.dart';
import 'package:mixvietnam/base/app_config.dart';
import 'package:mixvietnam/constants/colors.dart';
import 'package:mixvietnam/models/personal.dart';

class PersonalWatcher extends StatelessWidget {
  const PersonalWatcher({this.personal, this.number});
  final Personal personal;
  final int number;
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Row(
          children: [
            Text(
              number.toString(),
              style: AppConfig.style.contentText1.copyWith(
                fontWeight: FontWeight.w400,
                fontSize: 12,
                color: textColor3,
              ),
            ),
            Container(
              margin: const EdgeInsets.only(left: 11, right: 8),
              width: 32,
              height: 32,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(50),
                image: DecorationImage(
                    image: AssetImage(personal.avatar), fit: BoxFit.cover),
              ),
            ),
            Text(
              personal.name,
              style: AppConfig.style.contentText1.copyWith(
                  fontWeight: FontWeight.w500, fontSize: 12, color: CL_BOLD),
            ),
          ],
        ),
        Text(
          personal.listened.toString(),
          style: AppConfig.style.contentText1.copyWith(
              fontWeight: FontWeight.w500, fontSize: 12, color: CL_BOLD),
        ),
      ],
    );
  }
}
