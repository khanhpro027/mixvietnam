import 'package:flutter/material.dart';
import 'package:mixvietnam/base/app_config.dart';
import 'package:mixvietnam/constants/colors.dart';

class TimeLineWidgets extends StatelessWidget {
  const TimeLineWidgets({this.label, this.onChoose = false});
  final String label;
  final bool onChoose;
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {},
      child: Container(
        width: 70,
        height: 32,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(30),
          color: (onChoose == true) ? PRIMARY_COLOR : Colors.transparent,
        ),
        padding: const EdgeInsets.only(left: 10, top: 7, bottom: 7, right: 10),
        child: Text(
          label,
          style: AppConfig.style.contentText1.copyWith(
              fontWeight: FontWeight.w500,
              fontSize: 14,
              color: (onChoose == true) ? Colors.white : textColor3),
        ),
      ),
    );
  }
}
