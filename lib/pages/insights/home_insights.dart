import 'package:flutter/material.dart';
import 'package:mixvietnam/base/app_config.dart';
import 'package:mixvietnam/constants/colors.dart';
import 'package:mixvietnam/fake_data/fake_post.dart';
import 'package:mixvietnam/pages/insights/widget/bottom_bar.dart';
import 'package:mixvietnam/pages/insights/widget/special_music.dart';
import 'package:mixvietnam/pages/insights/widget/timeline_widget.dart';

class HomeInsights extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[300],
      appBar: AppBar(
        leading: const Icon(Icons.keyboard_arrow_down_rounded),
        title: Text(
          'Insights',
          style: AppConfig.style.contentText1.copyWith(
            fontWeight: FontWeight.w500,
            fontSize: 16,
            color: CL_BOLD,
          ),
        ),
        centerTitle: true,
        elevation: 1,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            _topWidget(),
            _specialMusic(),
            const SizedBox(
              height: 10,
            ),
            _specialMusic(title: 'Người nghe'),
          ],
        ),
      ),
    );
  }

  Widget _topWidget() {
    return Container(
      margin: const EdgeInsets.only(top: 10, bottom: 10),
      color: Colors.white,
      child: Column(
        children: [
          Padding(
            padding:
                const EdgeInsets.only(left: 16, right: 16, bottom: 10, top: 12),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: const [
                TimeLineWidgets(
                  label: '1 ngày',
                  onChoose: true,
                ),
                TimeLineWidgets(
                  label: '1 tháng',
                  onChoose: false,
                ),
                TimeLineWidgets(
                  label: '1 năm',
                  onChoose: false,
                ),
                TimeLineWidgets(
                  label: 'tất cả',
                  onChoose: false,
                ),
              ],
            ),
          ),
          BottomBar(),
          const SizedBox(
            height: 12,
          ),
        ],
      ),
    );
  }

  Widget _specialMusic({String title}) {
    return Container(
      color: Colors.white,
      padding: const EdgeInsets.only(bottom: 10, left: 16, right: 16),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(bottom: 5, top: 12),
            child: Text(
              title ?? 'Bài hát nỗi bật',
              style: AppConfig.style.contentText1.copyWith(
                  fontWeight: FontWeight.w500, fontSize: 16, color: CL_BOLD),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                'Hôm nay',
                style: AppConfig.style.contentText1.copyWith(
                    fontWeight: FontWeight.w400,
                    fontSize: 12,
                    color: const Color(0xffBBBBBB)),
              ),
              Text(
                'Phát',
                style: AppConfig.style.contentText1.copyWith(
                    fontWeight: FontWeight.w400,
                    fontSize: 12,
                    color: const Color(0xffBBBBBB)),
              ),
            ],
          ),
          Container(
            height: 2,
            padding: const EdgeInsets.only(top: 8, bottom: 8),
            child: Divider(
              height: 1,
              color: Colors.grey[300],
            ),
          ),
          ListView.builder(
              itemCount: 3,
              shrinkWrap: true,
              padding: const EdgeInsets.only(top: 15),
              itemBuilder: (context, index) {
                return Padding(
                  padding: const EdgeInsets.only(top: 12),
                  child: SpecialMusic(
                    musicPost: FakePost.post[0],
                    number: 16,
                  ),
                );
              }),
          Container(
            margin: const EdgeInsets.only(top: 10),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8),
              color: buttonPostColor,
            ),
            alignment: Alignment.center,
            padding: const EdgeInsets.only(top: 8, bottom: 8),
            child: Text(
              'Xem tất cả',
              style: AppConfig.style.contentText1.copyWith(
                  fontWeight: FontWeight.w500,
                  fontSize: 14,
                  color: PRIMARY_COLOR),
            ),
          ),
        ],
      ),
    );
  }
}
