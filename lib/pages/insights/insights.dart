import 'package:flutter/material.dart';
import 'package:mixvietnam/base/app_config.dart';
import 'package:mixvietnam/constants/colors.dart';
import 'package:mixvietnam/constants/strings.dart';
import 'package:mixvietnam/fake_data/fake_post.dart';
import 'package:mixvietnam/pages/insights/widget/special_music.dart';

class InSights extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          leading: const Icon(Icons.keyboard_arrow_down_rounded),
          title: Text(
            todayInsightsText,
            style: AppConfig.style.contentText1.copyWith(
              fontWeight: FontWeight.w500,
              fontSize: 16,
              color: CL_BOLD,
            ),
          ),
          centerTitle: true,
          elevation: 1,
        ),
        body: Container(
          margin: const EdgeInsets.only(top: 10),
          padding: const EdgeInsets.only(top: 12, left: 16, right: 16),
          decoration: BoxDecoration(
              color: Colors.white,
              border: Border.all(color: Colors.grey[300], width: 1)),
          child: Column(
            children: [
              Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        'Hôm nay',
                        style: AppConfig.style.contentText1.copyWith(
                            fontWeight: FontWeight.w400,
                            fontSize: 12,
                            color: const Color(0xffBBBBBB)),
                      ),
                      Text(
                        'Phát',
                        style: AppConfig.style.contentText1.copyWith(
                            fontWeight: FontWeight.w400,
                            fontSize: 12,
                            color: const Color(0xffBBBBBB)),
                      ),
                    ],
                  ),
                  Container(
                    height: 2,
                    padding: const EdgeInsets.only(top: 8, bottom: 8),
                    child: Divider(
                      height: 1,
                      color: Colors.grey[300],
                    ),
                  ),
                  ListView.builder(
                      itemCount: 5,
                      shrinkWrap: true,
                      padding: const EdgeInsets.only(top: 15),
                      itemBuilder: (context, index) {
                        return Padding(
                          padding: const EdgeInsets.only(top: 12),
                          child: SpecialMusic(
                            musicPost: FakePost.post[0],
                            number: 16,
                          ),
                        );
                      }),
                ],
              ),
            ],
          ),
        ));
  }
}
