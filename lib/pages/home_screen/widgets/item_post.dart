import 'package:flutter/material.dart';
import 'package:mixvietnam/base/app_config.dart';
import 'package:mixvietnam/constants/colors.dart';
import 'package:mixvietnam/constants/image.dart';
import 'package:mixvietnam/constants/strings.dart';
import 'package:mixvietnam/models/music_post_model.dart';
import 'package:mixvietnam/pages/home_screen/player/player_music.dart';
import 'package:mixvietnam/pages/home_screen/widgets/post/like_share_post.dart';
import 'package:mixvietnam/pages/home_screen/widgets/post/music_post.dart';

class ItemPost extends StatelessWidget {
  const ItemPost({this.post});
  final MusicPostModel post;

  @override
  Widget build(BuildContext context) {
   // final Size size = MediaQuery.of(context).size;
    return GestureDetector(
      onTap: (){
        Navigator.push(context, MaterialPageRoute(builder: (context) => PlayerMusic(musicPost: post,)));
      },
      child: Container(
        margin: const EdgeInsets.only(bottom: 16),
        color: Colors.white,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    Container(
                      margin: const EdgeInsets.only(left: 16, right: 8,bottom: 12,top: 12),
                      width: 36,
                      height: 36,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(18),
                          image: const DecorationImage(
                              image: AssetImage(fake_avatar2))),
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          post.postUserName,
                          style: AppConfig.style.contentText1.copyWith(
                              fontSize: 14,
                              fontWeight: FontWeight.w500,
                              color: CL_BOLD),
                        ),
                        Text(
                          post.time,
                          style: AppConfig.style.contentText1.copyWith(
                              fontSize: 12,
                              fontWeight: FontWeight.w400,
                              color: textColor3.withOpacity(0.7)),
                        ),
                      ],
                    ),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 16),
                  child: Image.asset(dotIcon),
                ),
              ],
            ),

            // music Area
            PlayMusicPost(
              post: post,
            ),

            GestureDetector(
              onTap: () {},
              child: Container(
                margin: const EdgeInsets.only(
                    left: 16, right: 16, top: 10, bottom: 12),
                height: 32,
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(16),
                  color: buttonPostColor,
                ),
                child: Text(
                  seeAllAlbum,
                  style: AppConfig.style.contentText1.copyWith(
                      fontSize: 14,
                      fontWeight: FontWeight.w500,
                      color: PRIMARY_COLOR),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 16, right: 16),
              child: Text(
                post.content,
                style: AppConfig.style.contentText1
                    .copyWith(fontSize: 14, fontWeight: FontWeight.w400),
                textAlign: TextAlign.justify,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                top: 10,
                left: 16,
                right: 16,
                bottom: 10,
              ),
              child: Text(
                post.tag,
                style: AppConfig.style.contentText1.copyWith(
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                  color: textColor3.withOpacity(0.7),
                ),
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                textAlign: TextAlign.start,
              ),
            ),
            LikeSharePost(),
          ],
        ),
      ),
    );
  }
}
