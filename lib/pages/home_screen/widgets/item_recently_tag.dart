import 'package:flutter/material.dart';
import 'package:mixvietnam/base/app_config.dart';
import 'package:mixvietnam/constants/colors.dart';
import 'package:mixvietnam/models/gerne_chips.dart';

class ItemRecentlyTag extends StatelessWidget {
  ItemRecentlyTag({this.chips});
  final GerneChips chips;
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(top: 7, bottom: 7, left: 12, right: 12),
      decoration: BoxDecoration(
        color: (chips.isChoose) ? PRIMARY_COLOR : Colors.transparent,
        borderRadius: BorderRadius.circular(20),
      ),
      child: Text(
        chips.text,
        style: AppConfig.style.contentText1.copyWith(
            color: (chips.isChoose) ? Colors.white : textColor.withOpacity(0.6),
            fontWeight: FontWeight.w500,
            fontSize: 14),
      ),
    );
  }
}
