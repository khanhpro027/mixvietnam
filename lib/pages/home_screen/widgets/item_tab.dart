import 'package:flutter/material.dart';
import 'package:mixvietnam/base/app_config.dart';
import 'package:mixvietnam/constants/colors.dart';

class ItemTab extends StatelessWidget {
  const ItemTab({
    Key key,
    @required this.path,
    @required this.label,
    @required this.index,
    this.selectedIndex,
  }) : super(key: key);

  final int selectedIndex;
  final String path;
  final String label;
  final int index;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 5, bottom: 3),
      child: Column(
        children: [
          Image.asset(
            path,
            color: (selectedIndex == index) ? PRIMARY_COLOR : textColor3,
          ),
          Text(
            label,
            style: AppConfig.style.contentText1.copyWith(
              fontWeight: FontWeight.w400,
              fontSize: 12,
              color: (selectedIndex == index) ? PRIMARY_COLOR : textColor3,
            ),
          )
        ],
      ),
    );
  }
}
