import 'package:flutter/material.dart';
import 'package:mixvietnam/base/app_config.dart';
import 'package:mixvietnam/constants/colors.dart';
import 'package:mixvietnam/constants/strings.dart';
import 'package:mixvietnam/models/single.dart';
import 'package:mixvietnam/widgets/simple_button_widget.dart';

class ItemSinger extends StatelessWidget {
  ItemSinger({this.singer, this.toUpCase = false, this.isJoin = false});
  final Singer singer;
  final bool toUpCase;
  final bool isJoin;
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          width: 100,
          height: 100,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(50),
              image: DecorationImage(
                  image: AssetImage(singer.avatar), fit: BoxFit.cover)),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 8, bottom: 2),
          child: Text(
            singer.name,
            style: AppConfig.style.contentText1.copyWith(
                fontWeight: FontWeight.w500, fontSize: 14, color: CL_BOLD),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(bottom: 8),
          child: Text(
            '${singer.follower}',
            style: AppConfig.style.contentText1.copyWith(
                fontWeight: FontWeight.w400, fontSize: 12, color: textColor3),
          ),
        ),
        _joinOrFollow(isJoin),
      ],
    );
  }

  Widget _joinOrFollow(bool isJoin) {
    return (isJoin == true)
        ? SimpleButtonWidget(
            width: 100,
            height: 40,
            text: singer.isFlow ? joined.toUpperCase() : join.toUpperCase(),
            textColor: singer.isFlow ? PRIMARY_COLOR : Colors.white,
            radiusBorder: 8,
            background: singer.isFlow ? Colors.white : PRIMARY_COLOR,
          )
        : SimpleButtonWidget(
            width: 100,
            height: 40,
            text: singer.isFlow ? followed.toUpperCase() : follow.toUpperCase(),
            textColor: singer.isFlow ? PRIMARY_COLOR : Colors.white,
            radiusBorder: 8,
            background: singer.isFlow ? Colors.white : PRIMARY_COLOR,
          );
  }
}
