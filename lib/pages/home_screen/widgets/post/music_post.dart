import 'package:flutter/material.dart';
import 'package:mixvietnam/base/app_config.dart';
import 'package:mixvietnam/constants/colors.dart';
import 'package:mixvietnam/constants/image.dart';
import 'package:mixvietnam/constants/strings.dart';
import 'package:mixvietnam/models/music_post_model.dart';
import 'package:mixvietnam/widgets/avatar_play.dart';

class PlayMusicPost extends StatelessWidget {
  const PlayMusicPost({this.post});
  final MusicPostModel post;

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;

    return Row(
      children: [
        AvatarPlay(),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              width: size.width - 172,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    post.author,
                    style: AppConfig.style.contentText1.copyWith(
                      color: textColor.withOpacity(0.6),
                      fontWeight: FontWeight.w400,
                      fontSize: 14,
                    ),
                  ),
                  Text(
                    post.title,
                    style: AppConfig.style.contentText1.copyWith(
                        fontSize: 18,
                        fontWeight: FontWeight.w500,
                        color: CL_BOLD),
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                  ),
                  // progress
                  const SizedBox(height: 3,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        '$countPlayList ${post.countPlayList} Bài hát',
                        style: AppConfig.style.contentText1.copyWith(
                          color: textColor,
                          fontSize: 10,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                      Text(
                        '${post.postion}/${post.durarion}',
                        style: AppConfig.style.contentText1.copyWith(
                          color: textColor,
                          fontSize: 10,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                    ],
                  ),
                  Container(
                    margin: const EdgeInsets.only(top: 8, bottom: 8),
                    width: size.width - 172,
                    child: const LinearProgressIndicator(
                      value: 20,
                      color: textColor,
                      valueColor: AlwaysStoppedAnimation<Color>(PRIMARY_COLOR),
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        post.like.toString() + ' lượt thích • 1 đăng lại ',
                        style: AppConfig.style.contentText1.copyWith(
                          color: textColor,
                          fontSize: 10,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                      Text(
                        '1,278 lượt nghe',
                        style: AppConfig.style.contentText1.copyWith(
                          color: textColor,
                          fontSize: 10,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            Image.asset(love_track),
          ],
        ),
      ],
    );
  }
}
