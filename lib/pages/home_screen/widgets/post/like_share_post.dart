import 'package:flutter/material.dart';
import 'package:mixvietnam/base/app_config.dart';
import 'package:mixvietnam/constants/colors.dart';
import 'package:mixvietnam/constants/image.dart';
import 'package:mixvietnam/pages/home_screen/widgets/post/button_with_icon.dart';

class LikeSharePost extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 16, right: 16),
      child: Column(
        children: [
          const Divider(
            height: 2,
            color: textColor,
          ),
          Padding(
            padding:
                const EdgeInsets.only(left: 13, right: 13, top: 10, bottom: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                ButtonWithIcon(
                  icon: likeIcon,
                  text: 'Thích',
                ),
                ButtonWithIcon(
                  icon: commentIcon,
                  text: 'Thảo luận',
                ),
                ButtonWithIcon(
                  icon: shareIcon,
                  text: 'Chia sẻ',
                ),
              ],
            ),
          ),
          const Divider(
            height: 2,
            color: textColor,
          ),
          _bottomShare(),
        ],
      ),
    );
  }

  Widget _bottomShare() {
    return Padding(
      padding: const EdgeInsets.only(top: 10, bottom: 12),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            children: [
              Stack(
                children: [
                  _fakeAvatar(path: fake_like),
                  _fakeAvatar(
                      path: fake_like2,
                      margin: const EdgeInsets.only(left: 14)),
                  _fakeAvatar(
                      path: fake_like3,
                      margin: const EdgeInsets.only(left: 28)),
                ],
              ),
              Text(
                '+ 1, 378 người khác đang thích',
                style: AppConfig.style.contentText1.copyWith(
                    fontSize: 10,
                    fontWeight: FontWeight.w400,
                    color: textColor3),
              ),
            ],
          ),
          Text(
            '1, 378 bình luận • 378 chia sẻ',
            style: AppConfig.style.contentText1.copyWith(
                fontSize: 12, fontWeight: FontWeight.w400, color: textColor3),
          ),
        ],
      ),
    );
  }

  Widget _fakeAvatar({String path, EdgeInsets margin}) {
    return Container(
      width: 18,
      margin: margin ?? EdgeInsets.zero,
      height: 18,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(9),
        border: Border.all(width: 2, color: Colors.white),
        image: DecorationImage(
          image: AssetImage(path),
          fit: BoxFit.cover,
        ),
      ),
    );
  }
}
