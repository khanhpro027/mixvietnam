import 'package:flutter/material.dart';
import 'package:mixvietnam/base/app_config.dart';
import 'package:mixvietnam/constants/colors.dart';

class ButtonWithIcon extends StatelessWidget {
  ButtonWithIcon({this.icon, this.style, this.text});
  final String icon;
  final String text;
  final TextStyle style;
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Image.asset(icon),
        Text(
          text,
          style: style ??
              AppConfig.style.contentText1.copyWith(
                  fontSize: 14, fontWeight: FontWeight.w500, color: textColor),
        ),
      ],
    );
  }
}
