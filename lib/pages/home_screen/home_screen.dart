import 'package:flutter/material.dart';
import 'package:mixvietnam/base/app_config.dart';
import 'package:mixvietnam/constants/colors.dart';
import 'package:mixvietnam/constants/image.dart';
import 'package:mixvietnam/pages/home_screen/widgets/item_tab.dart';
import 'package:mixvietnam/pages/tab_home/tab_home.dart';
import 'package:mixvietnam/pages/tab_user/tab_user.dart';

class HomeScreen extends StatefulWidget {
  @override
  _NaviPageState createState() => _NaviPageState();
}

class _NaviPageState extends State<HomeScreen>
    with SingleTickerProviderStateMixin {
  int _selectedIndex = 0;
  final PageController _pageController = PageController();
  TabController _controller;

  @override
  void initState() {
    super.initState();
    _controller = TabController(vsync: this, length: 5);
  }

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;

    return Scaffold(
      bottomNavigationBar: Container(
        height: 52,
        child: Column(
          children: [
            const Divider(
              color: textColor3,
              height: 2,
            ),
            PreferredSize(
              preferredSize: Size(size.width, 44),
              child: TabBar(
                padding: EdgeInsets.zero,
                tabs: [
                  ItemTab(
                      selectedIndex: _selectedIndex,
                      path: homeIcon,
                      label: 'Trang chủ',
                      index: 0),
                  ItemTab(
                      selectedIndex: _selectedIndex,
                      path: hashIcon,
                      label: 'Khám phá',
                      index: 1),
                  ItemTab(
                      selectedIndex: _selectedIndex,
                      path: libIcon,
                      label: 'Thư viện',
                      index: 2),
                  ItemTab(
                      selectedIndex: _selectedIndex,
                      path: notiIcon,
                      label: 'Thông báo',
                      index: 3),
                  ItemTab(
                      selectedIndex: _selectedIndex,
                      path: userIcon,
                      label: 'Cá nhân',
                      index: 4),
                ],
                labelStyle: AppConfig.style.contentText1
                    .copyWith(fontWeight: FontWeight.w400, fontSize: 12),
                indicatorColor: Colors.transparent,
                unselectedLabelColor: textColor3,
                labelColor: PRIMARY_COLOR,
                controller: _controller,
                labelPadding: EdgeInsets.zero,
                onTap: _onTappedBar,
              ),
            ),
          ],
        ),
      ),
      body: PageView(
        controller: _pageController,
        children: <Widget>[
          TabHome(),
          Container(),
          Container(),
          Container(),
          TabUser(),
        ],
      ),
    );
  }

  void _onTappedBar(int value) {
    setState(() {
      _selectedIndex = value;
    });
    _pageController.jumpToPage(value);
  }

/*
   Gọi api lấy list nghe gần đây : /social/v1/user/listen/list/
*/

/*
   Gọi api lấy danh sách nghe : /social/v1/user/listen/list/
*/

/*
   Gọi api lấy list gợi ý kết bạn : /social/v1/user/friend_suggestion/list/
*/

/*
   Gọi api lấy list gợi ý cộng đồng : /social/v1/user/community_suggestion/list/
*/

}
