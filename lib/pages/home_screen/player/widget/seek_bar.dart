import 'dart:math';

import 'package:flutter/material.dart';
import 'package:mixvietnam/base/app_config.dart';
import 'package:mixvietnam/constants/colors.dart';

class SeekBar extends StatefulWidget {
  final Duration duration;
  final Duration position;
  final Duration bufferedPosition;
  final ValueChanged<Duration> onChanged;
  final ValueChanged<Duration> onChangeEnd;

  const SeekBar({
    Key key,
    @required this.duration,
    @required this.position,
    @required this.bufferedPosition,
    this.onChanged,
    this.onChangeEnd,
  }) : super(key: key);

  @override
  SeekBarState createState() => SeekBarState();
}

class SeekBarState extends State<SeekBar> {
  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  // ignore: unused_element
  String _printDuration(Duration duration) {
    String twoDigits(int n) => n.toString().padLeft(2, '0');
    final twoDigitMinutes = twoDigits(duration.inMinutes.remainder(60));
    final twoDigitSeconds = twoDigits(duration.inSeconds.remainder(60));
    return '$twoDigitMinutes:$twoDigitSeconds';
  }

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;

    return Row(
      children: [
        Text(
          _printDuration(widget.position),
          style: AppConfig.style.contentText1
              .copyWith(fontSize: 14, fontWeight: FontWeight.w400),
        ),
        Container(
          child: SliderTheme(
            data: SliderThemeData(
                thumbShape: const RoundSliderThumbShape(enabledThumbRadius: 10),
                activeTrackColor: PRIMARY_COLOR,
                inactiveTrackColor: Colors.grey[300],
                disabledActiveTrackColor: Colors.grey),
            child: Slider(
              thumbColor: PRIMARY_COLOR,
              max: widget.duration.inMilliseconds.toDouble(),
              value: min(widget.bufferedPosition.inMilliseconds.toDouble(),
                  widget.duration.inMilliseconds.toDouble()),
              onChanged: (value) {
                setState(() {});
                if (widget.onChanged != null) {
                  widget.onChanged(Duration(milliseconds: value.round()));
                }
              },
              onChangeEnd: (value) {
                if (widget.onChangeEnd != null) {
                  widget.onChangeEnd(Duration(milliseconds: value.round()));
                }
              },
              activeColor: PRIMARY_COLOR,
            ),
          ),
          width: size.width * 0.8,
        ),

        // Container(
        //   width: size.width * 0.8,
        //   child: SliderTheme(
        //     data: _sliderThemeData.copyWith(
        //       thumbShape: HiddenThumbComponentShape(),
        //       activeTrackColor: Colors.blue.shade100,
        //       inactiveTrackColor: Colors.grey.shade300,
        //     ),
        //     child: ExcludeSemantics(
        //       child: Slider(
        //         min: 0.0,
        //         max: widget.duration.inMilliseconds.toDouble(),
        //         value: min(widget.bufferedPosition.inMilliseconds.toDouble(),
        //             widget.duration.inMilliseconds.toDouble()),
        //         onChanged: (value) {
        //           setState(() {
        //             _dragValue = value;
        //           });
        //           if (widget.onChanged != null) {
        //             widget.onChanged(Duration(milliseconds: value.round()));
        //           }
        //         },
        //         onChangeEnd: (value) {
        //           if (widget.onChangeEnd != null) {
        //             widget.onChangeEnd(Duration(milliseconds: value.round()));
        //           }
        //           _dragValue = null;
        //         },
        //       ),
        //     ),
        //   ),
        // ),
        Text(
          _printDuration(widget.duration),
          style: AppConfig.style.contentText1
              .copyWith(fontSize: 14, fontWeight: FontWeight.w400),
        ),
      ],
    );
  }

//  Duration get _remaining => widget.duration - widget.position;
}

class HiddenThumbComponentShape extends SliderComponentShape {
  @override
  Size getPreferredSize(bool isEnabled, bool isDiscrete) => Size.zero;

  @override
  void paint(
    PaintingContext context,
    Offset center, {
    @required Animation<double> activationAnimation,
    @required Animation<double> enableAnimation,
    @required bool isDiscrete,
    @required TextPainter labelPainter,
    @required RenderBox parentBox,
    @required SliderThemeData sliderTheme,
    @required TextDirection textDirection,
    @required double value,
    @required double textScaleFactor,
    @required Size sizeWithOverflow,
  }) {}
}

class PositionData {
  final Duration position;
  final Duration bufferedPosition;
  final Duration duration;

  PositionData(this.position, this.bufferedPosition, this.duration);
}

void showSliderDialog({
  @required BuildContext context,
  @required String title,
  @required int divisions,
  @required double min,
  @required double max,
  String valueSuffix = '',
  @required double value,
  @required Stream<double> stream,
  @required ValueChanged<double> onChanged,
}) {
  showDialog<void>(
    context: context,
    builder: (context) => AlertDialog(
      title: Text(title, textAlign: TextAlign.center),
      content: StreamBuilder<double>(
        stream: stream,
        builder: (context, snapshot) => SizedBox(
          height: 100.0,
          child: Column(
            children: [
              Text('${snapshot.data?.toStringAsFixed(1)}$valueSuffix',
                  style: const TextStyle(
                      fontFamily: 'Fixed',
                      fontWeight: FontWeight.bold,
                      fontSize: 24.0)),
              Slider(
                divisions: divisions,
                min: min,
                max: max,
                value: snapshot.data ?? value,
                onChanged: onChanged,
              ),
            ],
          ),
        ),
      ),
    ),
  );
}

T ambiguate<T>(T value) => value;
