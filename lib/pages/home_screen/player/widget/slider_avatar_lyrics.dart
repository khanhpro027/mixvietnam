import 'package:flutter/material.dart';
import 'package:mixvietnam/constants/colors.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:mixvietnam/models/music_post_model.dart';
import 'package:mixvietnam/pages/home_screen/player/widget/avatar_with_name.dart';
import 'package:mixvietnam/pages/home_screen/player/widget/lyrics_screen.dart';

class SliderAvatarLyrics extends StatefulWidget {
  SliderAvatarLyrics({this.musicPost, this.onChange});
  final MusicPostModel musicPost;
  final Function(int value) onChange;
  @override
  State<SliderAvatarLyrics> createState() => _SliderAvatarLyricsState();
}

class _SliderAvatarLyricsState extends State<SliderAvatarLyrics> {
  final CarouselController controller = CarouselController();

  int _currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;

    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        //  if (_currentIndex == 1) _customTabbar(),
        Divider(
          color: textColor3.withOpacity(0.4),
          height: 1,
        ),
        CarouselSlider(
          items: [
            //slider 1
            AvatarWithName(
              musicPost: widget.musicPost,
            ),

            //slider 2
            LyricsScreen(),
          ],
          options: CarouselOptions(
              autoPlay: false,
              height: size.height * 0.6,
              viewportFraction: 1.0,
              enlargeCenterPage: false,
              enableInfiniteScroll: false,
              onPageChanged: (current, index) {
                widget.onChange(current);
                setState(() {
                  _currentIndex = current;
                });
              }),
        ),
        const SizedBox(
          height: 20,
        ),
        Align(
          alignment: Alignment.center,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: _count.asMap().entries.map((entry) {
              return Padding(
                padding: const EdgeInsets.only(right: 8),
                child: (_currentIndex != entry.key)
                    ? _notSelectIndicator()
                    : _selectedIndicator(),
              );
            }).toList(),
          ),
        ),
      ],
    );
  }

  Widget _notSelectIndicator() {
    return Container(
      width: 6,
      height: 6,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(3),
        color: textColor3,
      ),
    );
  }

  Widget _selectedIndicator() {
    return Container(
      width: 32,
      height: 6,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(6),
        color: PRIMARY_COLOR,
      ),
    );
  }

  final List<int> _count = [0, 1];
}
