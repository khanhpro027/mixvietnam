import 'package:flutter/material.dart';
import 'package:mixvietnam/base/app_config.dart';
import 'package:mixvietnam/constants/colors.dart';
import 'package:mixvietnam/constants/image.dart';
import 'package:mixvietnam/models/music_post_model.dart';

class AvatarWithName extends StatelessWidget {
  AvatarWithName({this.musicPost});
  final MusicPostModel musicPost;
  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;

    return Stack(
      children: [
        Column(
          children: [
            Stack(
              children: [
                Container(
                  decoration: const BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage(fake_avatar_player),
                          fit: BoxFit.fill)),
                  width: size.width,
                  height: size.height * 0.5,
                ),
                Container(
                  width: size.width,
                  height: size.height * 0.5,
                  decoration: const BoxDecoration(
                      gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [
                      Colors.transparent,
                      Colors.white,
                    ],
                  )),
                ),
              ],
            ),
            Text(
              musicPost.title,
              style: AppConfig.style.contentText1.copyWith(
                  fontWeight: FontWeight.w500, fontSize: 24, color: CL_BOLD),
              maxLines: 1,
            ),
            const SizedBox(
              height: 8,
            ),
            Text(
              musicPost.author,
              style: AppConfig.style.contentText1.copyWith(
                  fontWeight: FontWeight.w400, fontSize: 16, color: textColor),
            ),
          ],
        ),
        SafeArea(
          child: Padding(
            padding: const EdgeInsets.only(left: 16, right: 16),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                InkWell(
                  child: const Icon(
                    Icons.keyboard_arrow_down_sharp,
                    color: Colors.white,
                  ),
                  onTap: () {},
                ),
                Container(
                  width: 34,
                  height: 34,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      border: Border.all(width: 1, color: Colors.white),
                      image: const DecorationImage(
                          image: AssetImage(fake_avatar4))),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
