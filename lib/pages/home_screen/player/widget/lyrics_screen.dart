import 'package:flutter/material.dart';
import 'package:mixvietnam/base/app_config.dart';
import 'package:mixvietnam/constants/colors.dart';
import 'package:mixvietnam/fake_data/fake_post.dart';
import 'package:scrollable_positioned_list/scrollable_positioned_list.dart';

class LyricsScreen extends StatefulWidget {
  @override
  State<LyricsScreen> createState() => _LyricsScreenState();
}

class _LyricsScreenState extends State<LyricsScreen> {
  @override
  void initState() {
    super.initState();
  }

  void _onChangePosition(int position) {
    _itemScrollController.scrollTo(
        index: position % FakePost.lyrics.length,
        duration: const Duration(seconds: 1),
        curve: Curves.easeIn,
        alignment: 0.5);

    setState(() {
      _position = position;
    });
  }

  int _position = 0;
  final ItemScrollController _itemScrollController = ItemScrollController();
  @override
  Widget build(BuildContext context) {
    return ScrollablePositionedList.separated(
        itemBuilder: (BuildContext context, int index) {
          final _value = FakePost.lyrics[index];
          return GestureDetector(
            child: _item(value: _value, key: index),
            onTap: () => _onChangePosition(index),
          );
        },
        itemCount: FakePost.lyrics.length,
        itemScrollController: _itemScrollController,
        separatorBuilder: (context, i) => const Padding(
              padding: EdgeInsets.symmetric(horizontal: 16),
            ));
  }

  Widget _item({String value, int key}) {
    return Padding(
        padding: const EdgeInsets.only(
          bottom: 16,
        ),
        child: Text(
          value,
          textAlign: TextAlign.center,
          style: AppConfig.style.contentText1.copyWith(
            fontWeight: FontWeight.w500,
            fontSize: (_position == key) ? 21 : 14,
            color: (_position == key) ? PRIMARY_BUTTON_COLOR : CL_BOLD,
          ),
        ));
  }
}
