import 'package:flutter/material.dart';
import 'package:mixvietnam/base/app_config.dart';
import 'package:mixvietnam/constants/colors.dart';
import 'package:mixvietnam/constants/strings.dart';
import 'package:mixvietnam/fake_data/fake_post.dart';
import 'package:mixvietnam/pages/personal_page/widget/personal_item_music.dart';

class SuggestPlayer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.only(top: 16, left: 16, bottom: 18),
          child: Text(
            suggest4U,
            style: AppConfig.style.contentText1.copyWith(
                fontWeight: FontWeight.w500, fontSize: 20, color: CL_BOLD),
          ),
        ),
        PersonalItemMusic(
          music: FakePost.music[0],
        ),
        PersonalItemMusic(
          music: FakePost.music[0],
        ),
        PersonalItemMusic(
          music: FakePost.music[0],
        ),
        PersonalItemMusic(
          music: FakePost.music[0],
        ),
        PersonalItemMusic(
          music: FakePost.music[0],
        ),
      ],
    );
  }
}
