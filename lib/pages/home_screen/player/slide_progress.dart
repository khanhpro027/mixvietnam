import 'package:audio_session/audio_session.dart';
import 'package:flutter/material.dart';
import 'package:just_audio/just_audio.dart';
import 'package:mixvietnam/base/app_helpers.dart';
import 'package:mixvietnam/constants/image.dart';
import 'package:mixvietnam/pages/home_screen/player/widget/seek_bar.dart';
import 'package:rxdart/rxdart.dart';

class SlideProgress extends StatefulWidget {
  SlideProgress({this.pageController, this.onChange});
  final PageController pageController;
  final Function(int value) onChange;
  @override
  State<SlideProgress> createState() => _SlideProgressState();
}

class _SlideProgressState extends State<SlideProgress>
    with WidgetsBindingObserver {
  //final String _position = '0:00';
  bool _isPlaying = false;
  final _player = AudioPlayer();

  @override
  void initState() {
    _init();
    super.initState();
  }

  Future<void> _init() async {
    // Inform the operating system of our app's audio attributes etc.
    // We pick a reasonable default for an app that plays speech.
    final session = await AudioSession.instance;
    await session.configure(const AudioSessionConfiguration.speech());
    // Listen to errors during playback.
    _player.playbackEventStream.listen((event) {},
        onError: (Object e, StackTrace stackTrace) {
      printDebug('A stream error occurred: $e');
    });
    // Try to load audio from a source and catch any errors.
    try {
      // AAC example: https://dl.espressif.com/dl/audio/ff-16b-2c-44100hz.aac
      await _player.setAudioSource(AudioSource.uri(Uri.parse(
          'https://s3.amazonaws.com/scifri-episodes/scifri20181123-episode.mp3')));
    } catch (e) {
      printDebug('Error loading audio source: $e');
    }
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    // Release decoders and buffers back to the operating system making them
    // available for other apps to use.
    _player.dispose();
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.paused) {
      // Release the player's resources when not in use. We use "stop" so that
      // if the app resumes later, it will still remember what position to
      // resume from.
      _player.stop();
    }
  }

  Stream<PositionData> get _positionDataStream =>
      Rx.combineLatest3<Duration, Duration, Duration, PositionData>(
          _player.positionStream,
          _player.bufferedPositionStream,
          _player.durationStream,
          (position, bufferedPosition, duration) => PositionData(
              position, bufferedPosition, duration ?? Duration.zero));

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Container(
      padding: const EdgeInsets.only(bottom: 10),
      width: size.width,
      height: 176,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              // Text(
              //   _position,
              //   style: AppConfig.style.contentText1
              //       .copyWith(fontSize: 14, fontWeight: FontWeight.w400),
              // ),
              StreamBuilder<PositionData>(
                stream: _positionDataStream,
                builder: (context, snapshot) {
                  final positionData = snapshot.data;
                  return SeekBar(
                    duration: positionData?.duration ?? Duration.zero,
                    position: positionData?.position ?? Duration.zero,
                    bufferedPosition:
                        positionData?.bufferedPosition ?? Duration.zero,
                    onChangeEnd: _player.seek,
                  );
                },
              ),
            ],
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              InkWell(
                child: Image.asset(iconBack10),
                onTap: () {},
              ),
              InkWell(
                child: Image.asset(iconPlayBack),
                onTap: () {
                  widget.pageController.jumpToPage(1);
                  widget.onChange(1);
                },
              ),
              GestureDetector(
                child:
                    Image.asset((_isPlaying == false) ? iconPlay : pauseIcon),
                onTap: () => _onPlay(),
              ),
              InkWell(
                child: Image.asset(iconPlayNext),
                onTap: () {},
              ),
              InkWell(
                child: Image.asset(iconNext10),
                onTap: () {},
              ),
            ],
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              InkWell(
                child: Image.asset(
                  love_track,
                  width: 24,
                  height: 24,
                ),
                onTap: () {},
              ),
              InkWell(
                child: Image.asset(
                  iconRandom,
                  width: 24,
                  height: 24,
                ),
                onTap: () {},
              ),
              GestureDetector(
                child: Image.asset(
                  iconLoop,
                  width: 24,
                  height: 24,
                ),
                onTap: () {},
              ),
              InkWell(
                child: Image.asset(
                  iconNextTo,
                  width: 24,
                  height: 24,
                ),
                onTap: () {
                  widget.pageController.jumpToPage(1);
                  widget.onChange(1);
                },
              ),
              InkWell(
                child: Image.asset(
                  iconThreeDot,
                  width: 24,
                  height: 24,
                ),
                onTap: () {
                  widget.pageController.jumpToPage(2);
                  widget.onChange(2);
                  //  // widget.pageController.jumpToPage(1);
                },
              ),
            ],
          ),
        ],
      ),
    );
  }

  void _onPlay() {
    if (_isPlaying == true) {
      _player.play();
    } else {
      _player.pause();
    }
    setState(() {
      _isPlaying = !_isPlaying;
    });
  }
}
