import 'package:flutter/material.dart';
import 'package:mixvietnam/base/app_config.dart';
import 'package:mixvietnam/constants/colors.dart';
import 'package:mixvietnam/constants/strings.dart';
import 'package:mixvietnam/fake_data/fake_post.dart';
import 'package:mixvietnam/pages/tab_home/widgets/home_playlist.dart';
import 'package:mixvietnam/pages/personal_page/widget/personal_item_music.dart';

class PlayListMusic extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 16, right: 16),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Playlist',
                  style: AppConfig.style.contentText1.copyWith(
                      fontWeight: FontWeight.w500,
                      fontSize: 20,
                      color: CL_BOLD),
                ),
                Text(
                  '12 bài hát • 53 phút',
                  style: AppConfig.style.contentText1.copyWith(
                      fontWeight: FontWeight.w400,
                      fontSize: 12,
                      color: textColor3),
                ),
              ],
            ),
          ),
          HomePlaylist(
            title: '',
          ),
          Padding(
            padding: const EdgeInsets.only(left: 16, bottom: 16),
            child: Text(
              suggest4U,
              style: AppConfig.style.contentText1.copyWith(
                  fontWeight: FontWeight.w500, fontSize: 20, color: CL_BOLD),
            ),
          ),
          PersonalItemMusic(
            music: FakePost.music[0],
          ),
          PersonalItemMusic(
            music: FakePost.music[0],
          ),
          PersonalItemMusic(
            music: FakePost.music[0],
          ),
          PersonalItemMusic(
            music: FakePost.music[0],
          ),
          PersonalItemMusic(
            music: FakePost.music[0],
          ),
          PlayListMusic(),
        ],
      ),
    );
  }
}
