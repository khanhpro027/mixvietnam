import 'package:flutter/material.dart';
import 'package:mixvietnam/base/app_config.dart';
import 'package:mixvietnam/constants/colors.dart';
import 'package:mixvietnam/constants/image.dart';
import 'package:mixvietnam/models/music_post_model.dart';
import 'package:mixvietnam/pages/home_screen/player/play_list_music.dart';
import 'package:mixvietnam/pages/home_screen/player/slide_progress.dart';
import 'package:mixvietnam/pages/home_screen/player/suggest_player.dart';
import 'package:mixvietnam/pages/home_screen/player/widget/slider_avatar_lyrics.dart';

class PlayerMusic extends StatefulWidget {
  PlayerMusic({this.musicPost});
  final MusicPostModel musicPost;

  @override
  State<PlayerMusic> createState() => _PlayerMusicState();
}

class _PlayerMusicState extends State<PlayerMusic> {
  final PageController controller = PageController();
  int index = 0;
  @override
  Widget build(BuildContext context) {
    // final Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: Column(
        children: [
          if (index != 0) _customTabbar(),
          Expanded(
            child: PageView(
              physics: const NeverScrollableScrollPhysics(),
              controller: controller,
              children: [
                SliderAvatarLyrics(
                  musicPost: widget.musicPost,
                  onChange: (value) => onChange(value),
                ),
                SuggestPlayer(),
                PlayListMusic(),
              ],
            ),
          ),
        ],
      ),
      bottomNavigationBar: SlideProgress(
        pageController: controller,
        onChange: (value) => onChange(value),
      ),
    );
  }

  void onChange(int value) {
    setState(() {
      index = value;
    });
  }

  Widget _customTabbar() {
    return SafeArea(
      child: Container(
        padding: const EdgeInsets.only(left: 16, right: 16),
        height: 44,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            InkWell(
              child: const Icon(
                Icons.keyboard_arrow_down_sharp,
                size: 22,
                color: Colors.black,
              ),
              onTap: () {},
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  widget.musicPost.title,
                  style: AppConfig.style.contentText1.copyWith(
                      fontWeight: FontWeight.w500,
                      fontSize: 14,
                      color: CL_BOLD),
                ),
                Text(
                  widget.musicPost.author,
                  style: AppConfig.style.contentText1.copyWith(
                      fontWeight: FontWeight.w500,
                      fontSize: 12,
                      color: textColor3),
                ),
              ],
            ),
            Container(
              width: 34,
              height: 34,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  border: Border.all(width: 1, color: Colors.white),
                  image:
                      const DecorationImage(image: AssetImage(fake_avatar4))),
            ),
          ],
        ),
      ),
    );
  }
}
