import 'package:flutter/cupertino.dart';
import 'package:mixvietnam/base/app_config.dart';
import 'package:mixvietnam/constants/colors.dart';
import 'package:mixvietnam/models/play_list.dart';

class ListPlayer extends StatelessWidget {
  ListPlayer({this.playList});
  final PlayList playList;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(right: 16),
      width: 132,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            margin: const EdgeInsets.only(bottom: 10),
            width: 132,
            height: 132,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(4),
              image: DecorationImage(
                fit: BoxFit.cover,
                image: AssetImage(playList.avatar),
              ),
            ),
          ),
          Text(
            playList.songName,
            style: AppConfig.style.contentText1.copyWith(
              fontWeight: FontWeight.w500,
              fontSize: 14,
              color: CL_BOLD,
            ),
            maxLines: 2,
            overflow: TextOverflow.ellipsis,
          ),
          Text(
            playList.authorName,
            textAlign: TextAlign.justify,
            style: AppConfig.style.contentText1.copyWith(
              fontWeight: FontWeight.w400,
              fontSize: 12,
              color: textColor3,
            ),
            maxLines: 2,
            overflow: TextOverflow.ellipsis,
          ),
        ],
      ),
    );
  }
}
