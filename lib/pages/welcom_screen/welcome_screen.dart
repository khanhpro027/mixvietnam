import 'package:flutter/material.dart';
import 'package:introduction_screen/introduction_screen.dart';
import 'package:mixvietnam/base/app_cache.dart';
import 'package:mixvietnam/base/app_helpers.dart';
import 'package:mixvietnam/constants/image.dart';
import 'package:mixvietnam/constants/strings.dart';
import 'package:mixvietnam/pages/gerne_screen/gerne_screen.dart';
import 'package:mixvietnam/pages/welcom_screen/widgets/body_widget.dart';
import 'package:mixvietnam/widgets/image_asset_widget.dart';
import 'package:mixvietnam/widgets/simple_button_widget.dart';

class WelcomeScreen extends StatefulWidget {
  @override
  _WelcomScreenState createState() => _WelcomScreenState();
}

class _WelcomScreenState extends State<WelcomeScreen> {
  final introKey = GlobalKey<IntroductionScreenState>();

  @override
  Widget build(BuildContext context) {
    printDebug('build : WelcomScreen');
    const paddingTitleImage = EdgeInsets.only(top: 50);

    const pageDecoration = PageDecoration(
        titleTextStyle: TextStyle(fontSize: 28.0, fontWeight: FontWeight.w700),
        bodyTextStyle: TextStyle(fontSize: 19.0),
        descriptionPadding: EdgeInsets.fromLTRB(16.0, 0.0, 16.0, 16.0),
        //  pageColor: Colors.white,
        imagePadding: EdgeInsets.zero,
        boxDecoration: BoxDecoration(
            image: DecorationImage(
          image: AssetImage(backgroundWelcome),
        )));

    return IntroductionScreen(
      key: introKey,
      showSkipButton: true,
      pages: [
        // Page view 1
        PageViewModel(
          titleWidget: const ImageAssetWidget(
            assetPath: welcome_1,
            padding: paddingTitleImage,
          ),
          bodyWidget: const BodyWidget(
              title: strTitleIntroduction1, content: strContentIntroduction1),
          decoration: pageDecoration,
        ),

        // Page view 2
        PageViewModel(
          titleWidget: const ImageAssetWidget(
            assetPath: welcome_2,
            padding: paddingTitleImage,
          ),
          bodyWidget: const BodyWidget(
              title: strTitleIntroduction2, content: strContentIntroduction2),
          decoration: pageDecoration,
        ),

        // Page view 3
        PageViewModel(
          titleWidget: const ImageAssetWidget(
            assetPath: welcome_3,
            padding: paddingTitleImage,
          ),
          bodyWidget: const BodyWidget(
              title: strTitleIntroduction3, content: strContentIntroduction3),
          decoration: pageDecoration,
        ),
      ],
      // onDone:
      next: Container(
        padding: const EdgeInsets.all(10.0),
        child: const Icon(Icons.arrow_forward, size: 30.0),
        decoration: BoxDecoration(
          color: Colors.white,
          shape: BoxShape.circle,
          boxShadow: [
            BoxShadow(
              offset: const Offset(0.0, 2.0),
              color: Colors.grey[350],
              blurRadius: 5.0,
              spreadRadius: 1,
            ),
          ],
        ),
      ),
      onDone: () {},
      // Button start
      done: SimpleButtonWidget(
        background: Colors.red,
        width: 163,
        radiusBorder: 20,
        text: STR_INTRODUCTION_START,
        onPressed: () => _onIntroEnd(context),
      ),

      skip: const Text(STR_IGNORE),
      dotsDecorator: DotsDecorator(
        size: const Size(8.0, 8.0),
        color: const Color(0xFFBDBDBD),
        activeColor: Theme.of(context).colorScheme.secondary,
        activeSize: const Size(42.0, 8.0),
        activeShape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(25.0)),
        ),
      ),
    );
  }

  void _onIntroEnd(context) {
    AppCache().isShowWelcomeScreen = FLAG_OFF;
    Navigator.of(context).pushReplacement(
      MaterialPageRoute(
        builder: (context) => GenreScreen(),
        settings: const RouteSettings(name: '/welcom_screen/genre_screen'),
      ),
    );
  }
}
