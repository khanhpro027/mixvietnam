import 'package:flutter/material.dart';

class BodyWidget extends StatelessWidget {
  const BodyWidget({
    Key key,
    @required this.title,
    @required this.content,
  }) : super(key: key);

  final String title;
  final String content;

  @override
  Widget build(BuildContext context) {
    return RichText(
      text: TextSpan(
          text: title,
          style: const TextStyle(
              fontSize: 24.0, fontWeight: FontWeight.w700, color: Colors.black),
          children: [
            TextSpan(
              text: content,
              style: const TextStyle(
                fontSize: 17.0,
                color: Colors.black,
                fontWeight: FontWeight.normal,
              ),
            ),
          ]),
      textAlign: TextAlign.center,
    );
  }
}
