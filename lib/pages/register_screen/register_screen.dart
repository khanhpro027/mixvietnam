import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:mixvietnam/base/app_config.dart';
import 'package:mixvietnam/base/app_helpers.dart';
import 'package:mixvietnam/constants/colors.dart';
import 'package:mixvietnam/constants/image.dart';
import 'package:mixvietnam/constants/strings.dart';
import 'package:mixvietnam/models/area_model.dart';
import 'package:mixvietnam/models/personal.dart';
import 'package:mixvietnam/pages/register_screen/widgets/register_password.dart';
import 'package:mixvietnam/repository/auth_repo.dart';
import 'package:mixvietnam/widgets/come_back_group.dart';
import 'package:mixvietnam/widgets/input_area.dart';

class RegisterScreen extends StatefulWidget {
  @override
  State<RegisterScreen> createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  @override
  Widget build(BuildContext context) {
    printDebug('build : RegisterScreen');

    return Scaffold(
        body: Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        // Logo
        Align(
          child: Padding(
            padding: const EdgeInsets.only(top: 50, left: 16),
            child: SvgPicture.asset(iconHome),
          ),
          alignment: Alignment.topLeft,
        ),

        // Input email , name
        Padding(
          padding: const EdgeInsets.only(left: 16, right: 16),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.only(bottom: 3),
                child: Text(
                  registerText,
                  style: AppConfig.style.contentText1.copyWith(
                      fontWeight: FontWeight.w500,
                      fontSize: 20,
                      color: CL_BOLD),
                ),
              ),
              Text(
                registerMiniText,
                style: AppConfig.style.contentText1.copyWith(
                    fontWeight: FontWeight.w500,
                    fontSize: 12,
                    color: textColor.withOpacity(0.6)),
              ),
              const SizedBox(height: 10),

              // Input
              InputArea(
                title1: labelEmail,
                title2: registerName,
                titleButton: nextText,
                onClickNextButton: _onClickConfigRegister,
              ),
            ],
          ),
        ),

        // Come back
        ComeBackGroup(
          isShowLogin: true,
          text1: suggestLogin1,
        ),
      ],
    ));
  }

  Future<void> _onClickConfigRegister(AreaModel area) async {
    // Turn on loading
    AppHelpers.onLoading(context, flag: true);

    // Call api check email and username
    final isAuthCheck = await AuthRepo.authCheck(
      context: context,
      email: area.textField1,
      username: area.textField2,
    );

    // Move to input info user screen
    if (isAuthCheck) {
      _onMoveToInputPassUserPage(
        context,
        Personal(email: area.textField1, name: area.textField2),
      );
    }
  }

  void _onMoveToInputPassUserPage(BuildContext context, Personal user) {
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) => RegisterPassword(user: user),
        settings: const RouteSettings(
            name: '/login_screen/register_screen/input_pass'),
      ),
    );
  }
}

/*

1. Goi check email : https://test.mixvietnam.com/user/v1/auth/check/

*/
