import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:mixvietnam/base/app_config.dart';
import 'package:mixvietnam/constants/colors.dart';
import 'package:mixvietnam/constants/image.dart';
import 'package:mixvietnam/constants/strings.dart';
import 'package:mixvietnam/models/personal.dart';
import 'package:mixvietnam/models/register_prepare_model.dart';
import 'package:mixvietnam/widgets/come_back_group.dart';
import 'package:mixvietnam/widgets/input_area.dart';

class RegisterSuccess extends StatelessWidget {
  const RegisterSuccess({this.user, this.registerPrepare});
  final Personal user;
  final RegisterPrepareModel registerPrepare;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Align(
            child: Padding(
              padding: const EdgeInsets.only(top: 50, left: 16),
              child: SvgPicture.asset(iconHome),
            ),
            alignment: Alignment.topLeft,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 16, right: 16),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  comfirmRegister,
                  style: AppConfig.style.contentText1.copyWith(
                      fontWeight: FontWeight.w500,
                      fontSize: 20,
                      color: CL_BOLD),
                ),
                Text(
                  textSendConfirmRequest,
                  style: AppConfig.style.contentText1.copyWith(
                      fontWeight: FontWeight.w500,
                      fontSize: 12,
                      color: textColor.withOpacity(0.6)),
                ),
                Text(
                  user.email,
                  style: AppConfig.style.contentText1.copyWith(
                      fontWeight: FontWeight.w500,
                      fontSize: 12,
                      color: CL_BOLD),
                ),
                const SizedBox(
                  height: 10,
                ),
                InputArea(
                  child: Image.asset(confirmRegisterImage),
                  titleButton: login,
                  isShowPrivay: false,
                ),
              ],
            ),
          ),

          // Come back
          ComeBackGroup(isShowLogin: true),
        ],
      ),
    );
  }
}
