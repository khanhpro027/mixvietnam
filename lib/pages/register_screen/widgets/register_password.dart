import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:mixvietnam/base/app_config.dart';
import 'package:mixvietnam/base/app_helpers.dart';
import 'package:mixvietnam/constants/colors.dart';
import 'package:mixvietnam/constants/common.dart';
import 'package:mixvietnam/constants/image.dart';
import 'package:mixvietnam/constants/strings.dart';
import 'package:mixvietnam/models/area_model.dart';
import 'package:mixvietnam/models/personal.dart';
import 'package:mixvietnam/models/register_prepare_model.dart';
import 'package:mixvietnam/pages/register_screen/widgets/pin_code_register.dart';
import 'package:mixvietnam/repository/auth_repo.dart';
import 'package:mixvietnam/widgets/come_back_group.dart';
import 'package:mixvietnam/widgets/input_area.dart';

class RegisterPassword extends StatelessWidget {
  const RegisterPassword({Key key, this.user}) : super(key: key);
  final Personal user;

  @override
  Widget build(BuildContext context) {
    printDebug('build : RegisterScreen');

    return Scaffold(
        body: Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Align(
          child: Padding(
            padding: const EdgeInsets.only(top: 50, left: 16),
            child: SvgPicture.asset(iconHome),
          ),
          alignment: Alignment.topLeft,
        ),
        Padding(
          padding: const EdgeInsets.only(left: 16, right: 16),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.only(bottom: 3),
                child: Text(
                  createPass,
                  style: AppConfig.style.contentText1.copyWith(
                      fontWeight: FontWeight.w500,
                      fontSize: 20,
                      color: CL_BOLD),
                ),
              ),
              FittedBox(
                child: Text(
                  notePassword,
                  style: AppConfig.style.contentText1.copyWith(
                      fontWeight: FontWeight.w500,
                      fontSize: 12,
                      color: textColor.withOpacity(0.6)),
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              InputArea(
                title1: password,
                title2: rePassword,
                titleButton: nextText,
                onClickNextButton: (AreaModel area) => registerPrepare(
                    context, user, area.textField1, area.textField2),
              ),
            ],
          ),
        ),

        // Come back
        ComeBackGroup(
          text1: suggestLogin1,
          text2: login.toLowerCase(),
        ),
      ],
    ));
  }

  Future<void> registerPrepare(
      BuildContext context, Personal user, String pass, String rePass) async {
    if (pass == rePass) {
      // Call api register
      final registerPrepare =
          await AuthRepo.registerPrepare(email: user.email, pass: pass);

      // If register prepare is null
      if (registerPrepare == IS_NULL) return;

      // If register prepare is success
      _onMoveToPinCodeRegisterPage(context, user, registerPrepare);
    } else {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: const Text('Thông báo'),
            content:
                const Text('Mật khẩu và nhập lại mật khẩu không giống nhau'),
            actions: [
              TextButton(
                child: const Text('Đóng'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              )
            ],
          );
        },
      );
    }
  }

  void _onMoveToPinCodeRegisterPage(
    BuildContext context,
    Personal user,
    RegisterPrepareModel registerPrepare,
  ) {
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) => PinCodeRegister(
          user: user,
          registerPrepare: registerPrepare,
        ),
        settings: const RouteSettings(
          name: '/login_screen/register_screen/input_pass/pin_code_register',
        ),
      ),
    );
  }
}

/*
2. Nhập mật khẩu gửi xác thực otp : https://test.mixvietnam.com/user/v1/auth/register-prepare/
*/
