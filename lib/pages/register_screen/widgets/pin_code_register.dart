import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:mixvietnam/base/app_config.dart';
import 'package:mixvietnam/constants/colors.dart';
import 'package:mixvietnam/constants/common.dart';
import 'package:mixvietnam/constants/image.dart';
import 'package:mixvietnam/constants/strings.dart';
import 'package:mixvietnam/models/area_model.dart';
import 'package:mixvietnam/models/personal.dart';
import 'package:mixvietnam/models/register_prepare_model.dart';
import 'package:mixvietnam/pages/register_screen/widgets/register_username.dart';
import 'package:mixvietnam/repository/auth_repo.dart';
import 'package:mixvietnam/widgets/come_back_group.dart';
import 'package:mixvietnam/widgets/input_area.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

class PinCodeRegister extends StatefulWidget {
  const PinCodeRegister({this.user, this.registerPrepare});
  final Personal user;
  final RegisterPrepareModel registerPrepare;

  @override
  State<PinCodeRegister> createState() => _PinCodeRegisterState();
}

class _PinCodeRegisterState extends State<PinCodeRegister> {
  bool _isInput = false;
  String _pinCode = '';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Align(
            child: Padding(
              padding: const EdgeInsets.only(top: 50, left: 16),
              child: SvgPicture.asset(iconHome),
            ),
            alignment: Alignment.topLeft,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 16, right: 16),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(bottom: 3),
                  child: Text(
                    registerCompleted,
                    style: AppConfig.style.contentText1.copyWith(
                        fontWeight: FontWeight.w500,
                        fontSize: 20,
                        color: CL_BOLD),
                  ),
                ),
                Text(
                  sendPinCodeText,
                  style: AppConfig.style.contentText1.copyWith(
                      fontWeight: FontWeight.w500,
                      fontSize: 12,
                      color: textColor.withOpacity(0.6)),
                ),
                Text(
                  widget.user.email,
                  style: AppConfig.style.contentText1.copyWith(
                      fontWeight: FontWeight.w500,
                      fontSize: 12,
                      color: CL_BOLD),
                ),
                const SizedBox(height: 10),

                // Input pin code
                InputArea(
                  values: _isInput,
                  child: PinCodeTextField(
                    appContext: context,
                    length: 6,
                    onChanged: _onChange,
                    cursorColor: Colors.black,
                    pinTheme: PinTheme.defaults(
                      borderRadius: BorderRadius.circular(10),
                      shape: PinCodeFieldShape.box,
                      errorBorderColor: colorBorder.withOpacity(0.1),
                      disabledColor: colorBorder,
                      inactiveColor: textColor,
                      activeColor: textColor,
                      selectedColor: textColor,
                    ),
                  ),
                  titleButton: confirmPinCode,
                  isShowPrivay: false,
                  onClickNextButton: (AreaModel area) =>
                      _configPinCode(widget.user, widget.registerPrepare),
                ),
                const SizedBox(height: 30),
                Center(
                  child: RichText(
                    text: const TextSpan(
                        text: waitingText,
                        style: TextStyle(color: textColor),
                        children: [
                          TextSpan(
                              text: '10:00 ',
                              style: TextStyle(fontWeight: FontWeight.w700)),
                          TextSpan(text: minutes),
                        ]),
                  ),
                ),
              ],
            ),
          ),

          // Come back
          ComeBackGroup(
            isShowLogin: true,
          ),
        ],
      ),
    );
  }

  void _onChange(String value) {
    _pinCode = value;

    setState(() {
      if (value.length == 6) {
        _isInput = true;
      } else {
        _isInput = false;
      }
    });
  }

  Future<void> _configPinCode(
    Personal user,
    RegisterPrepareModel registerPrepare,
  ) async {
    // Call api config register
    final token = await AuthRepo.registerConfirm(
      user: user,
      registerPrepare: registerPrepare,
      otp: _pinCode,
    );

    // If register prepare is null
    if (token == IS_NULL) return;

    // If register prepare is success
    _onMoveToInputUsernamePage(context, user.copyWith(token: token));
  }

  void _onMoveToInputUsernamePage(BuildContext context, Personal user) {
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) => RegisterUsername(user: user),
        settings: const RouteSettings(
          name:
              '/login_screen/register_screen/input_pass/pin_code_register/input_username',
        ),
      ),
    );
  }
}

/*
3. Gọi api xác thực otp : https://test.mixvietnam.com/user/v1/auth/register-confirm/
*/
