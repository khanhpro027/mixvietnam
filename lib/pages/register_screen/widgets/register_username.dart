import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:mixvietnam/base/app_config.dart';
import 'package:mixvietnam/base/app_helpers.dart';
import 'package:mixvietnam/constants/colors.dart';
import 'package:mixvietnam/constants/common.dart';
import 'package:mixvietnam/constants/image.dart';
import 'package:mixvietnam/constants/strings.dart';
import 'package:mixvietnam/models/area_model.dart';
import 'package:mixvietnam/models/personal.dart';
import 'package:mixvietnam/pages/gerne_screen/gerne_screen.dart';
import 'package:mixvietnam/repository/user_repo.dart';
import 'package:mixvietnam/widgets/come_back_group.dart';
import 'package:mixvietnam/widgets/input_area.dart';

class RegisterUsername extends StatelessWidget {
  const RegisterUsername({Key key, this.user, this.pass}) : super(key: key);
  final Personal user;
  final String pass;

  @override
  Widget build(BuildContext context) {
    printDebug('build : RegisterScreen');

    return Scaffold(
        body: Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Align(
          child: Padding(
            padding: const EdgeInsets.only(top: 50, left: 16),
            child: SvgPicture.asset(iconHome),
          ),
          alignment: Alignment.topLeft,
        ),
        Padding(
          padding: const EdgeInsets.only(left: 16, right: 16),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                username,
                style: AppConfig.style.contentText1.copyWith(
                    fontWeight: FontWeight.w500, fontSize: 20, color: CL_BOLD),
              ),
              Text(
                hintUsername,
                style: AppConfig.style.contentText1.copyWith(
                    fontWeight: FontWeight.w500,
                    fontSize: 12,
                    color: textColor.withOpacity(0.6)),
              ),
              const SizedBox(
                height: 10,
              ),
              InputArea(
                title1: username,
                titleButton: doneText,
                onClickNextButton: (AreaModel area) => _updateProfileUser(
                  context,
                  user.copyWith(userName: area.textField1),
                ),
              ),
            ],
          ),
        ),

        // Come back
        ComeBackGroup(),
      ],
    ));
  }

  Future<void> _updateProfileUser(BuildContext context, Personal user) async {
    // Call api register
    final resultUpdate = await UserRepo.updateMyProfile(user: user);

    // If register is success
    if (resultUpdate == IS_NULL) return;

    // Move to GenreScreen
    _onMoveToGenreScreen(context);
  }

  void _onMoveToGenreScreen(BuildContext context) {
    Navigator.of(context).pushReplacement(
      MaterialPageRoute(
        builder: (context) => GenreScreen(),
        settings: const RouteSettings(name: '/option_screen/genre_screen'),
      ),
    );
  }
}

/*
4. Gọi api update thông tin : https://test.mixvietnam.com/user/v1/profile/update/
*/
