import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:mixvietnam/base/app_config.dart';
import 'package:mixvietnam/base/app_helpers.dart';
import 'package:mixvietnam/constants/colors.dart';
import 'package:mixvietnam/constants/image.dart';
import 'package:mixvietnam/constants/strings.dart';
import 'package:mixvietnam/models/gerne_chips.dart';
import 'package:mixvietnam/pages/gerne_screen/bloc/gerne_bloc.dart';
import 'package:mixvietnam/pages/gerne_screen/widgets/gerne_area.dart';
import 'package:mixvietnam/pages/home_screen/home_screen.dart';
import 'package:mixvietnam/repository/geme_repo.dart';
import 'package:mixvietnam/widgets/simple_button_widget.dart';

class GenreScreen extends StatelessWidget {
  final List<String> _genres = [];
  final _blocGerne = GerneBloc();

  @override
  Widget build(BuildContext context) {
    return BlocProvider<GerneBloc>(
      create: (context) => _blocGerne,
      child: Scaffold(
        backgroundColor: background,
        body: SingleChildScrollView(
          padding: const EdgeInsets.only(left: 16),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Align(
                child: Padding(
                  padding: const EdgeInsets.only(top: 50, bottom: 50),
                  child: SvgPicture.asset(iconHome),
                ),
                alignment: Alignment.topLeft,
              ),
              RichText(
                text: TextSpan(
                    text: chooseTypeMusic,
                    style: AppConfig.style.contentText1.copyWith(
                      fontWeight: FontWeight.w500,
                      fontSize: 20,
                      color: CL_BOLD,
                    ),
                    children: [
                      TextSpan(
                          text: musicText,
                          style: const TextStyle(color: priceColor)),
                    ]),
              ),
              Text(
                yourMusic,
                style: AppConfig.style.contentText1.copyWith(
                  fontWeight: FontWeight.w500,
                  fontSize: 20,
                  color: CL_BOLD,
                ),
              ),

              // List germe
              GerneArea(onChoose: _onChooseType),

              // Number type has choose
              BlocBuilder<GerneBloc, GerneState>(
                buildWhen: (previousState, state) {
                  return state is! InitialGerne;
                },
                builder: (BuildContext context, state) {
                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      // Bạn đã chọn n thể loại âm nhạc
                      RichText(
                        text: TextSpan(
                          text: '\nBạn đã chọn ',
                          style: AppConfig.style.contentText1.copyWith(
                            color: CL_BOLD,
                            fontWeight: FontWeight.w500,
                          ),
                          children: [
                            TextSpan(
                              text: numberTypeMusic.toString(),
                              style: AppConfig.style.contentText1
                                  .copyWith(color: PRIMARY_COLOR),
                            ),
                            TextSpan(text: typeMusic),
                          ],
                        ),
                      ),

                      // Giới hạn thể loại
                      Padding(
                        padding: const EdgeInsets.only(
                            right: 16, bottom: 16, top: 50),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            // Describe
                            RichText(
                              text: TextSpan(
                                text: 'Chọn ',
                                style: AppConfig.style.contentText1.copyWith(
                                  color: CL_BOLD,
                                  fontWeight: FontWeight.w500,
                                ),
                                children: [
                                  TextSpan(
                                    text: '3',
                                    style: AppConfig.style.contentText1
                                        .copyWith(color: PRIMARY_COLOR),
                                  ),
                                  TextSpan(text: typeMusicLimit),
                                ],
                              ),
                            ),

                            // Button save geme
                            SimpleButtonWidget(
                              isDisable: state.isDisable,
                              text: 'MIX',
                              width: 129,
                              height: 32,
                              background: PRIMARY_BUTTON_COLOR,
                              radiusBorder: 8,
                              onPressed: () => _onSaveGenre(context, _genres),
                            ),
                          ],
                        ),
                      ),
                    ],
                  );
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<void> _onSaveGenre(BuildContext context, List<String> genres) async {
    printDebug('Call : save genre');

    // Call save genre
    await GemeRepo.saveListGenre(
      genres: genres,
      onSuccess: () {
        Navigator.of(context).pushReplacement(
          MaterialPageRoute(builder: (_) => HomeScreen()),
        );
      },
    );
  }

  void _onChooseType(GerneChips gerneChips) {
    if (gerneChips.isChoose) {
      _genres.add(gerneChips.text);
    } else {
      _genres.remove(gerneChips.text);
    }

    // Update gerne and button
    if (isShowButtonSave) {
      _blocGerne.add(const ShowButtonEvent());
    } else {
      _blocGerne.add(const DisableButtonEvent());
    }
  }

  // Get set
  bool get isShowButtonSave => _genres.length >= 3;
  int get numberTypeMusic => _genres.length;
}

/*
  Gọi api update thể loại : /user/v1/profile/music/set/
*/

/*
  Gọi api lấy danh sách thể loại : /user/v1/setting/music/types/
*/
