part of 'gerne_bloc.dart';

abstract class GerneEvent extends Equatable {
  const GerneEvent();

  @override
  List<Object> get props => [];
}

class DisableButtonEvent extends GerneEvent {
  const DisableButtonEvent();
}

class ShowButtonEvent extends GerneEvent {
  const ShowButtonEvent();
}
