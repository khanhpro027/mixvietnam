import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'gerne_event.dart';
part 'gerne_state.dart';

class GerneBloc extends Bloc<GerneEvent, GerneState> {
  GerneBloc() : super(DisableButton());

  @override
  Stream<GerneState> mapEventToState(GerneEvent event) async* {
    if (event is DisableButtonEvent) {
      yield DisableButton();
      return;
    }

    if (event is ShowButtonEvent) {
      yield ShowButton();
      return;
    }
  }
}
