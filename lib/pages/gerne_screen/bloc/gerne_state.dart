part of 'gerne_bloc.dart';

abstract class GerneState extends Equatable {
  const GerneState({this.isDisable});

  final bool isDisable;

  @override
  List<Object> get props => [DateTime.now(), isDisable];
}

class InitialGerne extends GerneState {}

class DisableButton extends GerneState {
  @override
  bool get isDisable => true;
}

class ShowButton extends GerneState {
  @override
  bool get isDisable => false;
}
