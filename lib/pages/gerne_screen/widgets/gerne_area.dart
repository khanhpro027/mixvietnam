import 'package:flutter/material.dart';
import 'package:mixvietnam/base/app_helpers.dart';
import 'package:mixvietnam/models/gerne_chips.dart';
import 'package:mixvietnam/pages/gerne_screen/widgets/chips_widget.dart';
import 'package:mixvietnam/repository/geme_repo.dart';
import 'package:mixvietnam/widgets/app_loading_indicator.dart';

class GerneArea extends StatelessWidget {
  const GerneArea({Key key, this.onChoose}) : super(key: key);
  final ValueChanged<GerneChips> onChoose;

  @override
  Widget build(BuildContext context) {
    printDebug('Call : GerneArea ');

    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(16.0),
      ),
      child: FutureBuilder(
        future: GemeRepo.getListGenre(),
        builder:
            (BuildContext context, AsyncSnapshot<List<GerneChips>> snapshot) {
          if (snapshot?.hasData ?? false) {
            final _listGerne = snapshot?.data;

            return Padding(
              padding: const EdgeInsets.only(right: 12),
              child: Wrap(
                children: _listGerne
                    .map(
                      (GerneChips gerneChips) => ChipsWidget(
                        chips: gerneChips,
                        onChoose: _onChoose,
                      ),
                    )
                    .toList(),
              ),
            );
          }

          return const AppLoadingIndicator();
        },
      ),
    );
  }

  void _onChoose(GerneChips gerneChips) {
    onChoose.call(gerneChips);
  }
}
