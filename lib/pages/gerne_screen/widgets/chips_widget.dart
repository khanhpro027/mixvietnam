import 'package:flutter/material.dart';
import 'package:mixvietnam/base/app_config.dart';
import 'package:mixvietnam/constants/colors.dart';
import 'package:mixvietnam/models/gerne_chips.dart';

class ChipsWidget extends StatefulWidget {
  const ChipsWidget({this.chips, this.onChoose});
  final GerneChips chips;
  final ValueChanged<GerneChips> onChoose;

  @override
  State<ChipsWidget> createState() => _ChipsWidgetState();
}

class _ChipsWidgetState extends State<ChipsWidget> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 12, bottom: 12, left: 12),
      child: GestureDetector(
        child: Chip(
          label: Padding(
            padding:
                const EdgeInsets.only(top: 15, bottom: 15, left: 17, right: 17),
            child: Text(
              widget.chips.text,
              style: AppConfig.style.contentText1.copyWith(
                  color: (widget.chips.isChoose)
                      ? Colors.white
                      : PRIMARY_BUTTON_COLOR,
                  fontSize: 14,
                  fontWeight: FontWeight.w500),
            ),
          ),
          backgroundColor:
              (widget.chips.isChoose) ? PRIMARY_BUTTON_COLOR : Colors.white,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(40),
            side: const BorderSide(color: PRIMARY_BUTTON_COLOR),
          ),
        ),
        onTap: () {
          setState(() {
            widget.chips.isChoose = !widget.chips.isChoose;
            widget.onChoose.call(widget.chips);
          });
        },
      ),
    );
  }
}
