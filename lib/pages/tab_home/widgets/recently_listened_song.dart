import 'package:flutter/material.dart';
import 'package:mixvietnam/base/app_config.dart';
import 'package:mixvietnam/constants/colors.dart';
import 'package:mixvietnam/constants/strings.dart';
import 'package:mixvietnam/fake_data/fake_recently_listened_song.dart';
import 'package:mixvietnam/models/play_list.dart';
import 'package:mixvietnam/pages/home_screen/widgets/item_playlist.dart';
import 'package:mixvietnam/pages/home_screen/widgets/item_recently_tag.dart';

class RecentlyListenedSong extends StatelessWidget {
  const RecentlyListenedSong({Key key, this.listRecently}) : super(key: key);
  final List<PlayList> listRecently;

  @override
  Widget build(BuildContext context) {
    // final Size size = MediaQuery.of(context).size;

    return DefaultTabController(
      length: listRecently.length,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 16, bottom: 12, left: 16),
            child: Text(
              recentlyText,
              style: AppConfig.style.contentText1.copyWith(
                  fontWeight: FontWeight.w500, fontSize: 20, color: CL_BOLD),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 16),
            child: TabBar(
                indicatorPadding: EdgeInsets.zero,
                //  padding: EdgeInsets.zero,
                isScrollable: true,
                labelPadding: EdgeInsets.zero,
                indicatorColor: Colors.transparent,
                indicatorSize: TabBarIndicatorSize.label,
                tabs: listRecently
                    .map((data) => ItemRecentlyTag(
                          chips: data.tab,
                        ))
                    .toList()),
          ),
          Container(
            margin: const EdgeInsets.only(top: 10),
            height: 212,
            child: TabBarView(
              children: FakeRecentlyListenedSong.tabbar
                  .map((data) => SingleChildScrollView(
                        padding: const EdgeInsets.only(left: 16),
                        scrollDirection: Axis.horizontal,
                        child: Row(
                            children: data
                                .map(
                                  (child) => ItemPlayList(
                                    playList: child,
                                  ),
                                )
                                .toList()),
                      ))
                  .toList(),
            ),
          ),
        ],
      ),
    );
  }
}
