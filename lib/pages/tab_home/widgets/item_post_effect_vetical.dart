import 'package:flutter/material.dart';
import 'package:mixvietnam/pages/tab_home/widgets/custom_loading_effect_widget.dart';

class ItemPostEffectVetical extends StatelessWidget {
  const ItemPostEffectVetical({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: const [
        CustomLoadingEffectWidget(
          width: 132,
          height: 132,
          borderRadius: 4,
          margin: EdgeInsets.only(bottom: 8),
        ),
        CustomLoadingEffectWidget(width: 132, height: 17, borderRadius: 4),
        SizedBox(height: 2),
        CustomLoadingEffectWidget(width: 132, height: 42, borderRadius: 4),
      ],
    );
  }
}
