import 'package:flutter/material.dart';
import 'package:mixvietnam/constants/colors.dart';
import 'package:shimmer/shimmer.dart';

class CustomLoadingEffectWidget extends StatelessWidget {
  const CustomLoadingEffectWidget({
    Key key,
    this.width,
    this.height,
    this.borderRadius,
    this.margin,
  }) : super(key: key);

  final double width;
  final double height;
  final double borderRadius;
  final EdgeInsets margin;

  @override
  Widget build(BuildContext context) {
    return Shimmer.fromColors(
      baseColor: borderColor,
      highlightColor: Colors.white,
      child: Container(
        margin: margin ?? EdgeInsets.zero,
        width: width ?? 0,
        height: height ?? 0,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(borderRadius ?? 0),
          color: borderColor,
        ),
      ),
    );
  }
}
