import 'package:flutter/material.dart';
import 'package:mixvietnam/pages/tab_home/widgets/custom_loading_effect_widget.dart';
import 'package:mixvietnam/pages/tab_home/widgets/item_post_effect_vetical.dart';
import 'package:mixvietnam/pages/tab_home/widgets/post_playlist_loading_effect_widget.dart';

class LoadingEffect extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      physics: const BouncingScrollPhysics(),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const CustomLoadingEffectWidget(
            height: 19,
            width: 96,
            borderRadius: 2,
            margin: EdgeInsets.only(top: 12, bottom: 10, left: 16),
          ),

          // Suggest posts loading effect
          SingleChildScrollView(
            physics: const NeverScrollableScrollPhysics(),
            child: Padding(
              padding: const EdgeInsets.only(left: 16),
              child: Row(
                children: const [
                  ItemPostEffectVetical(),
                  Padding(
                    padding: EdgeInsets.only(left: 10, right: 10),
                    child: ItemPostEffectVetical(),
                  ),
                  ItemPostEffectVetical(),
                ],
              ),
            ),
            scrollDirection: Axis.horizontal,
          ),

          //
          const CustomLoadingEffectWidget(
            height: 19,
            width: 132,
            borderRadius: 4,
            margin: EdgeInsets.only(left: 16, top: 19, bottom: 10),
          ),

          // Play list loading effect
          const PostPlayListLoadingEffectWidget(),
        ],
      ),
    );
  }
}
