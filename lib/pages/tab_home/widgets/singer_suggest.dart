import 'package:flutter/material.dart';
import 'package:mixvietnam/base/app_config.dart';
import 'package:mixvietnam/constants/colors.dart';
import 'package:mixvietnam/constants/strings.dart';
import 'package:mixvietnam/fake_data/fake_suggest_singler.dart';
import 'package:mixvietnam/pages/home_screen/widgets/item_singer.dart';

class SingleSuggest extends StatelessWidget {
  SingleSuggest({this.title, this.isJoin = false});
  final String title;
  final bool isJoin;
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.only(top: 16, left: 16, bottom: 10),
          child: Text(
            title ?? singerSuggest,
            style: AppConfig.style.contentText1.copyWith(
              fontWeight: FontWeight.w500,
              fontSize: 20,
              color: CL_BOLD,
            ),
          ),
        ),
        SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          padding: const EdgeInsets.only(left: 16),
          child: Row(
            children: FakeSuggestSinger.data
                .map((data) => Padding(
                      padding: const EdgeInsets.only(right: 46),
                      child: ItemSinger(
                        toUpCase: true,
                        singer: data,
                        isJoin: isJoin,
                      ),
                    ))
                .toList(),
          ),
        ),
      ],
    );
  }
}
