import 'package:flutter/material.dart';
import 'package:mixvietnam/pages/tab_home/widgets/custom_loading_effect_widget.dart';

class PostPlayListLoadingEffectWidget extends StatelessWidget {
  const PostPlayListLoadingEffectWidget({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;

    return Container(
      color: Colors.white,
      child: Column(
        children: [
          Row(
            children: [
              const CustomLoadingEffectWidget(
                width: 36,
                height: 36,
                borderRadius: 16,
                margin:
                    EdgeInsets.only(top: 12, left: 16, right: 8, bottom: 10),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CustomLoadingEffectWidget(
                      width: size.width - 106, height: 17, borderRadius: 4),
                  const SizedBox(
                    height: 2,
                  ),
                  const CustomLoadingEffectWidget(
                      width: 97, height: 15, borderRadius: 4),
                ],
              ),
              const CustomLoadingEffectWidget(
                width: 26,
                height: 26,
                borderRadius: 4,
                margin: EdgeInsets.only(right: 16, left: 4),
              ),
            ],
          ),
          Row(
            children: [
              const CustomLoadingEffectWidget(
                width: 84,
                height: 84,
                borderRadius: 2,
                margin: EdgeInsets.only(left: 16, right: 8, bottom: 10),
              ),
              Column(
                children: [
                  Container(
                    width: size.width - 124,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: const [
                        CustomLoadingEffectWidget(
                            width: 36, height: 36, borderRadius: 16),
                        CustomLoadingEffectWidget(
                            width: 26, height: 26, borderRadius: 2),
                      ],
                    ),
                    margin: const EdgeInsets.only(bottom: 10),
                  ),
                  CustomLoadingEffectWidget(
                    width: size.width - 124,
                    height: 22,
                    borderRadius: 2,
                  ),
                ],
              )
            ],
          ),
        ],
      ),
    );
  }
}
