import 'package:flutter/material.dart';
import 'package:mixvietnam/base/app_config.dart';
import 'package:mixvietnam/constants/colors.dart';
import 'package:mixvietnam/constants/strings.dart';
import 'package:mixvietnam/fake_data/fake_recently_listened_song.dart';
import 'package:mixvietnam/pages/home_screen/widgets/item_playlist.dart';

class HomePlaylist extends StatelessWidget {
  HomePlaylist({this.title});
  final String title;
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.only(left: 16, bottom: 10),
          child: Text(
            title ?? topListened,
            style: AppConfig.style.contentText1.copyWith(
              fontWeight: FontWeight.w500,
              fontSize: 20,
              color: CL_BOLD,
            ),
          ),
        ),
        SingleChildScrollView(
          padding: const EdgeInsets.only(left: 16),
          scrollDirection: Axis.horizontal,
          child: Row(
              children: FakeRecentlyListenedSong.detail
                  .map(
                    (child) => ItemPlayList(
                      playList: child,
                    ),
                  )
                  .toList()),
        )
      ],
    );
  }
}
