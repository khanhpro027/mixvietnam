import 'package:after_layout/after_layout.dart';
import 'package:flutter/material.dart';
import 'package:mixvietnam/base/app_config.dart';
import 'package:mixvietnam/base/app_helpers.dart';
import 'package:mixvietnam/constants/colors.dart';
import 'package:mixvietnam/constants/strings.dart';
import 'package:mixvietnam/fake_data/fake_post.dart';
import 'package:mixvietnam/fake_data/fake_recently_listened_song.dart';
import 'package:mixvietnam/models/music_post_model.dart';
import 'package:mixvietnam/pages/tab_home/widgets/home_playlist.dart';
import 'package:mixvietnam/pages/tab_home/widgets/loading_effect.dart';
import 'package:mixvietnam/pages/tab_home/widgets/recently_listened_song.dart';
import 'package:mixvietnam/pages/tab_home/widgets/singer_suggest.dart';
import 'package:mixvietnam/pages/home_screen/widgets/item_post.dart';
import 'package:mixvietnam/repository/home_repo.dart';
import 'package:mixvietnam/widgets/primary_appbar.dart';

class TabHome extends StatefulWidget {
  @override
  State<TabHome> createState() => _TabHomeState();
}

class _TabHomeState extends State<TabHome> with AfterLayoutMixin<TabHome> {
  bool _isLoading = true;
  // ignore: unused_field
  List<MusicPostModel> _recentlyList = [];

  @override
  void initState() {
    printDebug('build : initState');
    _getHomeData().then((value) {
      if (mounted) {
        setState(() {
          _isLoading = false;
        });
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    printDebug('build : HomeScreen');

    final size = MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: background.withOpacity(0.2),
      appBar: PreferredSize(
        preferredSize: Size(size.width, 150),
        child: PrimaryAppBar(title: home_text, isLoading: _isLoading),
      ),
      body: (_isLoading == true)
          ? LoadingEffect()
          : RefreshIndicator(
              onRefresh: _onRefresh,
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    //
                    RecentlyListenedSong(
                      listRecently: FakeRecentlyListenedSong.data,
                    ),

                    //
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 16, left: 16, bottom: 10),
                      child: Text(
                        newPost,
                        style: AppConfig.style.contentText1.copyWith(
                          fontWeight: FontWeight.w500,
                          fontSize: 20,
                          color: CL_BOLD,
                        ),
                      ),
                    ),
                    ItemPost(
                      post: FakePost.post[0],
                    ),
                    ItemPost(
                      post: FakePost.post[0],
                    ),
                    ItemPost(
                      post: FakePost.post[0],
                    ),

                    // Suggest singer
                    Padding(
                      padding: const EdgeInsets.only(bottom: 16),
                      child: SingleSuggest(),
                    ),

                    ItemPost(post: FakePost.post[0]),

                    Padding(
                      padding: const EdgeInsets.only(bottom: 16),
                      child: SingleSuggest(
                        isJoin: true,
                        title: communitiesText,
                      ),
                    ),
                    ItemPost(post: FakePost.post[0]),

                    HomePlaylist(),

                    ItemPost(post: FakePost.post[0]),
                    ItemPost(post: FakePost.post[0]),
                    ItemPost(post: FakePost.post[0]),

                    HomePlaylist(
                      title: newPlayListText,
                    ),

                    ItemPost(post: FakePost.post[0]),
                  ],
                ),
              ),
            ),
    );
  }

  Future<void> _getHomeData() async {
    _recentlyList = await HomeRepo.getRecentlyPostList(page: 1);
  }

  Future<void> _onRefresh() async {
    setState(() {});
  }

  // void _onLoadingTabHome() async {}

  @override
  void afterFirstLayout(BuildContext context) {}
}

/*
  Gọi api lấy list home : /social/v1/post/recently/
*/
