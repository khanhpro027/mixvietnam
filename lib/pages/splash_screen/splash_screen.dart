import 'package:after_layout/after_layout.dart';
import 'package:flutter/material.dart';
import 'package:mixvietnam/base/app_cache.dart';
import 'package:mixvietnam/base/app_config.dart';
import 'package:mixvietnam/base/app_helpers.dart';
import 'package:mixvietnam/constants/colors.dart';
import 'package:mixvietnam/constants/strings.dart';
import 'package:mixvietnam/pages/home_screen/home_screen.dart';
import 'package:mixvietnam/pages/option_screen/option_screen.dart';

class SplashScreen extends StatefulWidget {
  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen>
    with AfterLayoutMixin<SplashScreen> {
  @override
  Widget build(BuildContext context) {
    printDebug('build : SplashScreen');

    return Scaffold(
      body: Stack(
        children: [
          // Image background
          Container(
            constraints: const BoxConstraints.expand(),
            child: Image.asset(
              'assets/images/splash_screen.png',
              fit: BoxFit.fill,
            ),
          ),

          // Icon background
          Center(
            child: Image.asset('assets/images/icon_flash_screen.png'),
          ),

          // Name group and name App
          Positioned(
            bottom: MediaQuery.of(context).size.height / 10,
            left: 0.0,
            right: 0.0,
            child: Column(
              children: [
                Text(
                  strAppName,
                  style: AppConfig.style.headline3?.copyWith(color: CL_WHITE),
                ),
                Text(
                  strSuzuGroup,
                  style: AppConfig.style.bodyText1?.copyWith(
                    color: CL_BODYTEXT,
                    fontWeight: FontWeight.normal,
                    fontSize: 16,
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  @override
  void afterFirstLayout(BuildContext context) => _handleMoveToPage(context);

  void _handleMoveToPage(BuildContext context) {
    // Check has login
    if (AppCache().logged) {
      Navigator.of(context).pushReplacement(
        MaterialPageRoute(
          builder: (context) => HomeScreen(),
          settings: const RouteSettings(name: '/home_screen'),
        ),
      );
    } else {
      Navigator.of(context).pushReplacement(
        MaterialPageRoute(
          builder: (context) => OptionScreen(),
          settings: const RouteSettings(name: '/option_screen'),
        ),
      );
    }
  }
}
