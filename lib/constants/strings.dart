const String strAppName = 'MixVietNam';
const String strCheckVersion = 'Kiểm tra phiên bản';

// Flash screen
const String strSuzuGroup = '© SuZu Group';

// Login creen
const String login = 'ĐĂNG NHẬP';
const String copyRight = '© SuZu Group';
const String start = 'TRẢI NGHIỆM NGAY';
const String loginWithAccount = 'Đăng nhập bằng tài khoản';
const String labelEmail = 'Email hoặc Tên đăng nhập';
const String labelPassword = 'Nhập mật khẩu';
const String STR_REMEMBER = 'Ghi nhớ đăng nhập';
const String forgetPass = 'Quên mật khẩu?';
const String note = 'Bằng cách tiếp tục bạn đã Đồng ý với ';
const String privacy = 'Điều khoản dịch vụ,';
const String term = 'Chính sách bảo mật MixVietnam';
const String backText = ' TRỞ LẠI';
const String suggestRegister1 = 'Bạn chưa có tài khoản? Hãy ';
const String suggestLogin1 = 'Bạn đã có tài khoản? Hãy ';
const String suggestRegister2 = 'tạo tài khoản';
const String suggestRegister3 = ' để tham gia ngay';
const String or = 'HOẶC';

// Register screen
const String register = 'ĐĂNG KÝ';
const String registerText = 'Tạo tài khoản đăng nhập';
const String registerName = 'Tên hiển thị';
const String nextText = 'TIẾP TỤC';
const String confirmPinCode = 'XÁC THỰC TÀI KHOẢN';
const String registerMiniText =
    'Nhập địa chỉ Email hoặc Tên đăng nhập để đăng ký tài khoản';
const String registerCompleted = 'Tạo tài khoản thành công';
const String sendPinCodeText =
    'Chúng tôi đã gửi mã xác thực tài khoản của bạn vào email ';
const String waitingText = 'Thời gian còn lại ';
const String sendEmailTo =
    'Chúng tôi đã gửi email xác thực tài khoản của bạn vào địa chỉ ';

// Forget pass
const String forgetPassScreen = 'Quên mật khẩu';
const String forgetPassMiniText =
    'Nhập địa chỉ Email của bạn vào khung phía dưới';
const String labelEmailOnly = 'Email';
const String resetPassButton = 'ĐẶT LẠI MẬT KHẨU';
const String createPass = 'Tạo mật khẩu';
const String notePassword =
    'Nhập tối thiểu 6 kí tự  có ít nhất 1 kí tự viết hoa hoặc 1 kí tự đặc biệt';
const String password = 'Nhập mật khẩu';
const String rePassword = 'Nhập lại mật khẩu';
const String username = 'Tên người dùng';
const String hintUsername = 'Nhập tối đa 20 kí tự (0/20)';
const String doneText = 'HOÀN THÀNH';
const String comfirmRegister = 'Tài khoản đã xác thực';
const String textSendConfirmRequest =
    'Chúng tôi đã gửi email xác thực tài khoản của bạn vào địa chỉ';

// Update app
const APP_STORE_URL =
    'https://apps.apple.com/us/app/tu%E1%BB%95i-tr%E1%BA%BB-online/id974433568?l';
const PLAY_STORE_URL =
    'https://play.google.com/store/apps/details?id=vn.tuoitre.app';

// Search page
const String searchHomeTitleNavigation = 'TÌM KIẾM';
const String searchBarTextBoxHome = 'Nhập từ khoá tìm kiếm ...';
const String searchHomeBoxRecommendTitle = 'Mọi người cũng tìm kiếm';
const String categoryListTitleNavigation = 'DANH MỤC';

//
const String msgNoData = 'Không có dữ liệu';
const String msgFunctionNotFinish =
    'Tính năng đang phát triển.Vui lòng chờ bản cập nhật sau!';
const String msgSignInFaild =
    'Tên đăng nhập hoặc mật khẩu không đúng. Vui lòng thử lại.';
const String MSG_REGISTER_EXIST_ACCOUNT = 'Tài khoản đã tồn tại.';
const String msgRegisterSuccess = 'Đăng ký thành công.';
const String msgEmailFaild = 'Tên hoặc email không hợp lệ.';
const String msgSent = 'Đã gửi';
const String msgLiked = 'Bạn đã yêu thích bài viết';

// Welcom page
const String STR_IGNORE = 'Bỏ qua';
const String strTitleIntroduction1 = 'Âm nhạc kết nối cộng đồng\n';
const String strContentIntroduction1 =
    'Cập nhật tình hình âm nhạc trên thế giới liên tục và nhanh chóng nhất';
const String strTitleIntroduction2 = 'Nghe nhạc mọi lúc mọi nơi\n';
const String strContentIntroduction2 =
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam,'
    'purus sit amet luctus venenatis, lectus';
const String strTitleIntroduction3 = 'Chém gió tẹt ga cùng đồng âm\n';
const String STR_INTRODUCTION_START = 'BẮT ĐẦU';
const String strContentIntroduction3 =
    'Cùng đồng âm chia sẻ và thảo luận âm nhạc hot đang có mặt trên thế giới';

// Home page
const bool HOME_STOP_LOAD_MORE = false;
const bool HOME_CAN_LOAD_MORE = true;
const bool FLAG_ON = true;
const bool FLAG_OFF = false;
const String home_text = 'Trang chủ';
const String yourThiking = 'Viết cảm nghĩ của bạn...';
const String music_text = ' Nhạc';
const String image_text = ' Hình ảnh';
const String recentlyText = 'Nghe gần đây';
const String countPlayList = 'Playlist • ';
const String seeAllAlbum = 'XEM TOÀN BỘ DANH SÁCH';
const String singerSuggest = 'Nghệ sĩ xịn sò';
const String follower = ' người theo dõi';
const String follow = 'Theo dõi';
const String followed = 'Đã theo dõi';
const String join = 'Tham gia';
const String joined = 'Đã tham gia';
const String newPost = 'Bài viết mới';
const String communitiesText = 'Cộng đồng chơi nỗi';
const String topListened = 'Nghe cực kỳ nhiều';
const String newPlayListText = 'Nghe cực kỳ nhiều';
const String homeText = 'Trang chủ';
const String khamPha = 'Khám phá';
const String libary = 'Thư viện';
const String notiText = 'Thông báo';
const String userText = 'Cá nhân';
const String strLogout = 'Đăng Xuất';

// Gerne page
String chooseTypeMusic = 'Chọn thể loại';
String musicText = ' âm nhạc';
String yourMusic = 'bạn đang quan tâm';
String typeMusicLimit = ' thể loại để \ncó sự trải nghiệm tốt nhất';
String typeMusic = ' thể loại âm nhạc';

// Search page
const bool SEARCH_LOADING = true;
const bool SEARCH_NOT_LOADING = false;

// notification screen
const String newestNoti = 'Thông báo mới';
const String oldNoti = 'Thông báo cũ';

// User Setting page
const String historyLogin = 'Lịch sử hoạt động';
const String infomationText = 'Thông tin tài khoản';
const String reportBug = 'Báo cáo sự cố';
const String privacyUser = 'Điều khoản';
const String serviceText = 'Dịch vụ';
const String copyRightText = 'Chính sách bản quyền';
const String aboutUs = 'Về chúng tôi';
const String aboutYourSelf = 'Mô tả bản thân';
const String yourNameText = 'Tên hiển thị';
const String emailAndUserNameText = 'Email & Tên đăng nhập';
const String passWordText = 'Mật khẩu';
const String lastPassword = 'Nhập mật khẩu cũ';
const String newPassword = 'Nhập mật khẩu mới';
const String inputPassword = 'Nhập lại mật khẩu';

// Network
const String STR_NETWORK_ERROR = 'Lỗi hệ thống';

// Cache key
const String THEME_SETTING = 'THEME_SETTING';
const String TEXT_ZOOM = 'TEXT_ZOOM';
const String DART_THEME = 'DART_THEME';
const String LIGHT_THEME = 'LIGHT_THEME';
const SYSTEM_THEME = 'SYSTEM_THEME';
const String FLAG_WELCOME_SCREEN = 'FLAG_WELCOME_SCREEN';
const String FLAG_LOGIN = 'FLAG_LOGIN';
const String CACHE_USER = 'CACHE_USER';
const String FLAG_REMEMBER_LOGIN = 'FLAG_REMEMBER_LOGIN';
const String CACHE_USERNAME = 'CACHE_USERNAME';
const String CACHE_PASS = 'CACHE_PASS';

// Personal Page
const String postText = 'Bài viết';
const String userFollowingText = 'Người đang theo dõi';
const String yourSignText = 'YOUR INSIGHT';
const String pinPostUserText = 'Bài viết ghim';
const String musicPosted = 'Bài hát đã đăng';
const String newest = 'Mới nhất';

/// Formaters
const UI_NUMBER_FM = '#,###';
const UI_DATE_FM = 'yyyy/MM/dd';
const UI_DATE_MMDD = 'MM/dd';
const UI_DATE_MMDD_FM = 'MM/dd HH:mm';
const UI_DATETIME_FM = 'yyyy/MM/dd HH:mm:ss';
const UI_DATETIME_HHMM_FM = 'yyyy/MM/dd HH:mm';
const UI_DATETIME = 'yyyy/MM/dd/';
const UI_DATE_MMMD = 'MMM d';
const UI_DATE_MMMDYYYY = 'MMM d yyyy';
const String minutes = 'phút';

// Player Screen
const String suggest4U = 'Đề xuất dành cho bạn';

// Insights
const String todayInsightsText = 'Bài hát nỗi bật';
