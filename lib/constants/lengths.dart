const double appBarHeight = 50;
const double bottomBarHeight = 54;
const int designWidth = 360;
const double searchTxtFieldHeight = 34;
const int maxItemsPaging = 20;
const double bigNewsHeight = 250;
const double smallNewsHeight = 110;
const double LENGTH_DEFAULT = 8.0;

/// Text size
const double TEXT_SIZE_DEFAULT = 0;
const double TEXT_SIZE_MIN = -2;
const double TEXT_SIZE_MAX = 6;
const double SIZE_SMALL = 32;
const double SIZE_MEDIUM = 48;
const double SIZE_LARGE = 56;

/// State
const int STATUS_SUCCESS = 0;
const int STATUS_ERROR = 1;
