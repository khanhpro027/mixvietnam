import 'package:flutter/foundation.dart' as foundation;
import 'package:mixvietnam/constants/strings.dart';
//----------------------------------------------------------------------------
// API endpoints
//----------------------------------------------------------------------------

const IS_NULL = null;

const API_TIMEOUT = Duration(seconds: 10);

/// Debug mode flag
const IS_DEBUG_MODE = foundation.kDebugMode;

/// Enable debug layer, only when IS_DEBUG_MODE is true
const ENABLE_DEBUG_OUTPUT = true;
const ENABLE_DEBUG_LAYER = true;

/// Log API response (debug only)
const API_LOG_RESPONSE = FLAG_ON;

/// Post content type
const HTTP_CONTENT_TYPE_JSON = 'application/json';
const HTTP_CONTENT_TYPE_FORM_DATA = 'multipart/form-data';
