import 'package:flutter/material.dart';

class Style {
  static TextTheme _textTheme;

  void init(BuildContext context) {
    _textTheme = Theme.of(context).textTheme;
  }

  TextStyle get bodyText1 =>
      _textTheme?.bodyText1?.copyWith(fontFamily: 'SFProDisplayRegular');

  TextStyle get headline3 =>
      _textTheme?.headline3?.copyWith(fontFamily: 'SFProDisplayBold');

  TextStyle get headline6 =>
      _textTheme?.headline6?.copyWith(fontFamily: 'SFProDisplayBold');

  TextStyle get contentText1 =>
      _textTheme?.caption?.copyWith(fontFamily: 'SFProDisplay');
}
