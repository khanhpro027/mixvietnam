part of 'home_route_bloc.dart';

abstract class HomeRouteState {
  const HomeRouteState();
}

class PaymentHistoryRouteInitial extends HomeRouteState {}

class UpdatePaymentSuccess extends HomeRouteState {
  const UpdatePaymentSuccess();
}

class UpdateReviewSuccess extends HomeRouteState {
  const UpdateReviewSuccess();
}
