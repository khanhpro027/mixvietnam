import 'package:flutter/material.dart';
import 'package:mixvietnam/pages/home_screen/home_screen.dart';
import 'package:mixvietnam/pages/tab_home/tab_home.dart';
import 'package:mixvietnam/routes/base_route.dart';

class HomeRoute extends BaseRoute {
  const HomeRoute(GlobalKey<NavigatorState> navigatorState)
      : super(navigatorState);

  static const String root = '/';
  static const String detail = '/detail';

  @override
  String get rootName => 'Home';

  @override
  bool get shouldRefreshContext => false;

  @override
  Map<String, WidgetBuilder> get routeBuilders {
    return {
      root: (context) => HomeScreen(),
      detail: (context) => TabHome(),
    };
  }
}
