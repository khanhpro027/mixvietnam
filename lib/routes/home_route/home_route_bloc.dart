import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mixvietnam/base/app_helpers.dart';
import 'package:mixvietnam/routes/home_route/home_route.dart';
import 'package:mixvietnam/routes/route_arguments.dart';

part 'home_route_event.dart';
part 'home_route_state.dart';

class HomeRouteBloc extends Bloc<HomeRouteEvent, HomeRouteState> {
  HomeRouteBloc({@required this.navigatorKey})
      : super(PaymentHistoryRouteInitial());

  final GlobalKey<NavigatorState> navigatorKey;

  NavigatorState get _navigator => navigatorKey.currentState;

  @override
  Stream<HomeRouteState> mapEventToState(
    HomeRouteEvent event,
  ) async* {
    if (event is ToPaymentHistoryRootEvent) {
      printDebug('event = ${event is ToPaymentHistoryRootEvent}');

      _navigator.popUntil(ModalRoute.withName(HomeRoute.root));
      return;
    }

    if (event is ToReviewDetailPhotoFromeDetailEvent) {
      _handleReviewDetailPhoto(event);
      return;
    }

    if (event is BackToPaymentHistoryDetailEvent) {
      _navigator.popUntil(ModalRoute.withName(HomeRoute.detail));
      if (event != null) {
        yield const UpdateReviewSuccess();
      }

      return;
    }
  }

  Future<void> _handleReviewDetailPhoto(
      ToReviewDetailPhotoFromeDetailEvent event) async {
    _navigator.pushNamed(
      HomeRoute.detail,
      arguments: RouteArguments(
        fullscreenDialog: false,
        opaque: true,
        data: <String, dynamic>{
          'review': event,
          'dealer': event,
          'isEditReview': event.isEditReview,
        },
      ),
    );
  }
}
