part of 'home_route_bloc.dart';

abstract class HomeRouteEvent extends Equatable {
  const HomeRouteEvent();

  @override
  List<Object> get props => [];
}

class ToPaymentHistoryRootEvent extends HomeRouteEvent {}

class ToPaymentHistoryDetailEvent extends HomeRouteEvent {
  const ToPaymentHistoryDetailEvent();
}

class ToEditServiceEvent extends HomeRouteEvent {
  const ToEditServiceEvent();
}

class ToEditMemoEvent extends HomeRouteEvent {
  const ToEditMemoEvent();
}

class ToReviewSelectBikeFromeDetailEvent extends HomeRouteEvent {
  const ToReviewSelectBikeFromeDetailEvent({
    this.isEditReview,
  });

  final bool isEditReview;
}

class ToReviewDetailPagingFromeDetailEvent extends HomeRouteEvent {
  const ToReviewDetailPagingFromeDetailEvent({this.isEditReview});

  final bool isEditReview;
}

class ToReviewDetailPhotoFromeDetailEvent extends HomeRouteEvent {
  const ToReviewDetailPhotoFromeDetailEvent({this.isEditReview});

  final bool isEditReview;
}

class BackToPaymentHistoryDetailEvent extends HomeRouteEvent {
  const BackToPaymentHistoryDetailEvent();
}
