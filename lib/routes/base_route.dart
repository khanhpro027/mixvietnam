import 'package:firebase_analytics/observer.dart';
import 'package:flutter/material.dart';
import 'package:mixvietnam/base/app_helpers.dart';
import 'package:mixvietnam/routes/route_arguments.dart';
import 'package:recase/recase.dart';

abstract class BaseRoute {
  const BaseRoute(this.navigatorState);

  static const String root = '/';

  /// NavigatorState
  final GlobalKey<NavigatorState> navigatorState;

  /// Root view name
  String get rootName;

  /// BuildContext should be refresh
  bool get shouldRefreshContext;

  /// Route ⇔ View builder
  Map<String, WidgetBuilder> get routeBuilders;

  /// Generate widget from route settings
  Route<dynamic> onGenerateRoute(RouteSettings settings) {
    final RouteArguments args =
        settings.arguments as RouteArguments ?? RouteArguments();

    // Select page builder
    final pageBuilder = (context) {
      if (routeBuilders.containsKey(settings.name)) {
        return routeBuilders[settings.name](context);
      }

      return const Offstage();
    };

    if (args.opaque == false) {
      return PageRouteBuilder<dynamic>(
        pageBuilder: (context, animation, secondaryAnimation) =>
            pageBuilder(context),
        settings: settings,
        fullscreenDialog: args.fullscreenDialog,
        maintainState: args.maintainState,
        opaque: false,
      );
    }

    return MaterialPageRoute<dynamic>(
      builder: (context) => pageBuilder(context),
      settings: settings,
      fullscreenDialog: args.fullscreenDialog,
      maintainState: args.maintainState,
    );
  }

  /// Extract route name for analytics
  String nameExtractor(RouteSettings settings) {
    return settings.name == root
        ? rootName
        : ReCase(settings.name ?? '').pascalCase;
  }

  /// Handle back button on Android
  WillPopCallback get onWillPop =>
      () async => !await navigatorState.currentState.maybePop();

  /// Get Navigator widget associated with the route
  Widget navigator({
    @required BuildContext context,
  }) {
    final navigator = Navigator(
      key: navigatorState,
      onGenerateRoute: onGenerateRoute,
      initialRoute: root,
      observers: <NavigatorObserver>[
        BaseRouteObserver(
          context,
          nameExtractor: nameExtractor,
        ),
      ],
    );

    return onWillPop != null
        ? WillPopScope(onWillPop: onWillPop, child: navigator)
        : navigator;
  }
}

class BaseRouteObserver extends RouteObserver<PageRoute<dynamic>> {
  BaseRouteObserver(
    this.context, {
    @required this.nameExtractor,
  }) : focusNode = FocusScope.of(context);

  final BuildContext context;
  final FocusScopeNode focusNode;
  final ScreenNameExtractor nameExtractor;

  /// Log screen view
  void _sendScreenView(PageRoute<dynamic> route) {
    final String screenName = nameExtractor(route.settings);
    if (screenName != null) {
      // AppAnalytics.sendPageview(screen: screenName, saveScreen: true);
    }
  }

  @override
  void didPush(Route<dynamic> route, Route<dynamic> previousRoute) {
    super.didPush(route, previousRoute);
    if (route is PageRoute) {
      _sendScreenView(route);
    }
  }

  @override
  void didReplace({Route<dynamic> newRoute, Route<dynamic> oldRoute}) {
    super.didReplace(newRoute: newRoute, oldRoute: oldRoute);
    if (newRoute is PageRoute) {
      _sendScreenView(newRoute);
    }
  }

  @override
  void didPop(Route<dynamic> route, Route<dynamic> previousRoute) {
    super.didPop(route, previousRoute);
    if (nameExtractor(previousRoute.settings) !=
        nameExtractor(route.settings)) {
      AppHelpers.hideKeyboard(focusNode);
    }
    if (previousRoute is PageRoute && route is PageRoute) {
      _sendScreenView(previousRoute);
    }
  }
}
