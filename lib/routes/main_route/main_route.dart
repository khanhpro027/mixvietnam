import 'package:flutter/material.dart';
import 'package:mixvietnam/pages/login_screen/login_with_social.dart';
import 'package:mixvietnam/pages/option_screen/option_screen.dart';
import 'package:mixvietnam/pages/register_screen/register_screen.dart';
import 'package:mixvietnam/routes/base_route.dart';

class MainRoute extends BaseRoute {
  const MainRoute(GlobalKey<NavigatorState> navigatorState)
      : super(navigatorState);

  static const String root = '/';
  static const String loginWithSocial = '/login_with_social';
  static const String registerScreen = '/register_screen';

  @override
  String get rootName => 'Main';

  @override
  bool get shouldRefreshContext => false;

  @override
  Map<String, WidgetBuilder> get routeBuilders {
    return {
      root: (context) => OptionScreen(),
      loginWithSocial: (context) => LoginWithSocial(),
      registerScreen: (context) => RegisterScreen(),
    };
  }
}
