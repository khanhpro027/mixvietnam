part of 'main_route_bloc.dart';

abstract class MainRouteEvent extends Equatable {
  const MainRouteEvent();

  @override
  List<Object> get props => [];
}

class ToPaymentHistoryRootEvent extends MainRouteEvent {}

class ToPaymentHistoryDetailEvent extends MainRouteEvent {
  const ToPaymentHistoryDetailEvent();
}

class ToEditServiceEvent extends MainRouteEvent {
  const ToEditServiceEvent();
}

class ToEditMemoEvent extends MainRouteEvent {
  const ToEditMemoEvent();
}

class ToReviewSelectBikeFromeDetailEvent extends MainRouteEvent {
  const ToReviewSelectBikeFromeDetailEvent({
    this.isEditReview,
  });

  final bool isEditReview;
}

class ToReviewDetailPagingFromeDetailEvent extends MainRouteEvent {
  const ToReviewDetailPagingFromeDetailEvent({this.isEditReview});

  final bool isEditReview;
}

class ToReviewDetailPhotoFromeDetailEvent extends MainRouteEvent {
  const ToReviewDetailPhotoFromeDetailEvent({this.isEditReview});

  final bool isEditReview;
}

class BackToPaymentHistoryDetailEvent extends MainRouteEvent {
  const BackToPaymentHistoryDetailEvent();
}
