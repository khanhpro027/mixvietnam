part of 'main_route_bloc.dart';

abstract class MainRouteState {
  const MainRouteState();
}

class PaymentHistoryRouteInitial extends MainRouteState {}

class UpdatePaymentSuccess extends MainRouteState {
  const UpdatePaymentSuccess();
}

class UpdateReviewSuccess extends MainRouteState {
  const UpdateReviewSuccess();
}
