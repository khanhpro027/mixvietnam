import 'package:mixvietnam/base/app_helpers.dart';

class RouteArguments {
  RouteArguments({
    this.maintainState = true,
    this.fullscreenDialog = false,
    this.data,
    this.opaque = true,
  });

  factory RouteArguments.fromJson(dynamic json) {
    return RouteArguments(
      maintainState: lookup<bool>(json, ['maintain_state']) ?? true,
      fullscreenDialog: lookup<bool>(json, ['fullscreen_dialog']) ?? false,
      opaque: lookup<bool>(json, ['opaque']) ?? true,
      data: (lookup<Map>(json, ['data']) as Map<String, dynamic>) ??
          <String, dynamic>{},
    );
  }

  final bool maintainState;
  final bool fullscreenDialog;
  final bool opaque;
  final Map<String, dynamic> data;

  Map<String, dynamic> toMap() {
    return {
      'maintain_state': maintainState,
      'fullscreen_dialog': fullscreenDialog,
      'opaque': opaque,
      'data': data,
    };
  }

  @override
  String toString() => <String, dynamic>{
        'maintainState': maintainState,
        'fullscreenDialog': fullscreenDialog,
        'opaque': opaque,
        'data': data,
      }.toString();
}
