import 'package:mixvietnam/constants/image.dart';
import 'package:mixvietnam/models/gerne_chips.dart';
import 'package:mixvietnam/models/play_list.dart';

class FakeRecentlyListenedSong {
  static List<PlayList> data = [
    PlayList(tab: GerneChips(id: '01', isChoose: true, text: 'Bài hát')),
    PlayList(tab: GerneChips(id: '01', isChoose: false, text: 'Playlists')),
    PlayList(tab: GerneChips(id: '01', isChoose: false, text: 'Albums')),
    PlayList(tab: GerneChips(id: '01', isChoose: false, text: 'Nghệ sĩ')),
  ];
  static List<List<PlayList>> tabbar = [detail, detail, detail, detail];
  static List<PlayList> detail = [
    PlayList(
        songName: 'Love the way you lie',
        authorName: 'Bài hát • Eminem ft Rihana',
        avatar: fake_avatar1),
    PlayList(
        songName: '6H CHILL',
        authorName:
            'Bài hát • BẠN CÓ TÀI MÀ \n(Official Music Track) (prod. HPro)',
        avatar: fake_avatar2),
    PlayList(
        songName: 'Những Bản Lofi Không Lời Cực Chill...',
        authorName: 'Playlist • Chang và Những người bạn',
        avatar: fake_avatar3),
    PlayList(
        songName: 'Love the way you lie',
        authorName: 'Bài hát • Eminem ft Rihana',
        avatar: fake_avatar1),
  ];
}
