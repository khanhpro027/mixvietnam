import 'package:mixvietnam/constants/image.dart';
import 'package:mixvietnam/models/personal.dart';

class FakePersonal {
  static Personal personalFake = const Personal(
    name: 'Hettie McGee',
    bio:
        'Music Artist from Spaceakers.\nBusiness Contact: Mrs. ABCCC - +84383550000',
    postCount: '32',
    following: '128',
    follower: '1',
    email: '@hettie.mcgee',
    avatar: fake_personal,
    cover: fake_banner,
  );
}
