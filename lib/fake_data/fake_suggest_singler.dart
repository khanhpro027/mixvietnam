import 'package:mixvietnam/constants/image.dart';
import 'package:mixvietnam/models/single.dart';

class FakeSuggestSinger {
  static List<Singer> data = [
    Singer(
        name: 'Juan Perkins',
        follower: '128 người theo dõi',
        isFlow: false,
        avatar: fake_suggest),
    Singer(
        name: 'Herbert Pearson',
        follower: '1 bạn chung',
        isFlow: true,
        avatar: fake_suggest1),
    Singer(
        name: 'Marshall Hawkins',
        follower: '12 người theo dõi',
        isFlow: false,
        avatar: fake_suggest2),
    Singer(
        name: 'Marshall Hawkins',
        follower: '12 người theo dõi',
        isFlow: false,
        avatar: fake_suggest2),
    Singer(
        name: 'Marshall Hawkins',
        follower: '12 người theo dõi',
        isFlow: false,
        avatar: fake_suggest2),
    Singer(
        name: 'Marshall Hawkins',
        follower: '12 người theo dõi',
        isFlow: false,
        avatar: fake_suggest2),
    Singer(
        name: 'Marshall Hawkins',
        follower: '12 người theo dõi',
        isFlow: false,
        avatar: fake_suggest2),
  ];
}
