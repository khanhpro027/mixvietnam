import 'package:mixvietnam/constants/image.dart';
import 'package:mixvietnam/models/music_post_model.dart';

class FakePost {
  static List<MusicPostModel> post = [
    const MusicPostModel(
        musicId: 0,
        title: 'Nhạc Remix Vinahouse 2020 - Nhạc Trẻ Remix...',
        author: 'Eminem ft Rihana',
        countPlayList: 12,
        postion: '0:00',
        postUserName: 'Scott Powell',
        time: '2 giờ trước',
        avatar: fake_avatar_music,
        durarion: '53:00',
        like: 12,
        tag: '#hiphipneverdie #masew2020 #chungthanh #trang...',
        content:
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam, purus sit amet luctus venenatis, lectus magna ',
        listented: 10),
  ];

  static List<MusicPostModel> music = [
    const MusicPostModel(
        author: 'Binz ft Chi Pu',
        avatar: fake_avatar_music,
        title: 'Kẻ cắp gặp bà già',
        durarion: '4:53',
        countPlayList: 1728,
        listented: 1728),
  ];

  static List<String> lyrics = [
    'Just gonna stand there and watch me burn',
    "Well that's alright, because I like the way it hurts",
    'Just gonna stand there and hear me cry',
    "Well that's alright, because I love the\n way you lie",
    'I love the way you lie',
    "I can't tell you what it really is",
    'I can only tell you what it feels like',
    "And right now there's a steel knife, in my windpipe",
    "I can't breathe, but I still fight, while I can fight",
    "As long as the wrong feels right, it's like I'm in flight",
    'High off of love drunk from my hate',
    'Just gonna stand there and watch me burn',
    "Well that's alright, because I like the way it hurts",
    'Just gonna stand there and hear me cry',
    "Well that's alright, because I love the\n way you lie",
    'I love the way you lie',
    "I can't tell you what it really is",
    'I can only tell you what it feels like',
    "And right now there's a steel knife, in my windpipe",
    "I can't breathe, but I still fight, while I can fight",
    "As long as the wrong feels right, it's like I'm in flight",
    'High off of love drunk from my hate',
    'Just gonna stand there and watch me burn',
    "Well that's alright, because I like the way it hurts",
    'Just gonna stand there and hear me cry',
    "Well that's alright, because I love the\n way you lie",
    'I love the way you lie',
    "I can't tell you what it really is",
    'I can only tell you what it feels like',
    "And right now there's a steel knife, in my windpipe",
    "I can't breathe, but I still fight, while I can fight",
    "As long as the wrong feels right, it's like I'm in flight",
    'High off of love drunk from my hate',
    'Just gonna stand there and watch me burn',
    "Well that's alright, because I like the way it hurts",
    'Just gonna stand there and hear me cry',
    "Well that's alright, because I love the\n way you lie",
    'I love the way you lie',
    "I can't tell you what it really is",
    'I can only tell you what it feels like',
    "And right now there's a steel knife, in my windpipe",
    "I can't breathe, but I still fight, while I can fight",
    "As long as the wrong feels right, it's like I'm in flight",
    'High off of love drunk from my hate',
  ];
}
