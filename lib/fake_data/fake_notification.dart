import 'package:mixvietnam/constants/image.dart';
import 'package:mixvietnam/models/notification.dart';

class FakeNoti {
  static List<NotificationModel> newestNoti = [
    NotificationModel(
      avatar: fake_avatar,
      title: 'Nguyen Thanh ',
      content:
          'dolor sit amet, consectetur adipiscing elit ut aliquam, purus sit amet ',
      time: '2 giờ trước',
      isRead: true,
    ),
    NotificationModel(
      avatar: fake_avatar,
      title: 'Nguyen Thanh ',
      content:
          'dolor sit amet, consectetur adipiscing elit ut aliquam, purus sit amet ',
      time: '2 giờ trước',
      isRead: false,
    ),
    NotificationModel(
      avatar: fake_avatar,
      title: 'Nguyen Thanh ',
      content:
          'dolor sit amet, consectetur adipiscing elit ut aliquam, purus sit amet ',
      time: '2 giờ trước',
      isRead: false,
    ),
    NotificationModel(
      avatar: fake_avatar,
      title: 'Nguyen Thanh ',
      content:
          'dolor sit amet, consectetur adipiscing elit ut aliquam, purus sit amet ',
      time: '2 giờ trước',
      isRead: true,
    ),
  ];

  static List<NotificationModel> oldNoti = [
    NotificationModel(
      avatar: fake_avatar,
      title: 'Nguyen Thanh ',
      content:
          'dolor sit amet, consectetur adipiscing elit ut aliquam, purus sit amet ',
      time: '2 giờ trước',
      isRead: true,
    ),
    NotificationModel(
      avatar: fake_avatar,
      title: 'Nguyen Thanh ',
      content:
          'dolor sit amet, consectetur adipiscing elit ut aliquam, purus sit amet ',
      time: '2 giờ trước',
      isRead: false,
    ),
    NotificationModel(
      avatar: fake_avatar,
      title: 'Nguyen Thanh ',
      content:
          'dolor sit amet, consectetur adipiscing elit ut aliquam, purus sit amet ',
      time: '2 giờ trước',
      isRead: false,
    ),
    NotificationModel(
      avatar: fake_avatar,
      title: 'Nguyen Thanh ',
      content:
          'dolor sit amet, consectetur adipiscing elit ut aliquam, purus sit amet ',
      time: '2 giờ trước',
      isRead: true,
    ),
  ];
}
